<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\ChatController;
use App\Http\Controllers\Api\CitiesController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\MeController;
use App\Http\Controllers\Api\MediaController;
use App\Http\Controllers\Api\OrganizationController;
use App\Http\Controllers\Api\Panel\Auth\LoginController as PanelLoginController;
use App\Http\Controllers\Api\Auth\OneTimePasswordController;
use App\Http\Controllers\Api\RatingController;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum', 'abilities:user-actions'])->group(function () {
    Route::group(['prefix' => 'me'], function () {
        Route::get('/', [MeController::class, 'show'])->name('meShow');
        Route::patch('/', [MeController::class, 'update'])->name('meUpdate');
        Route::get('/organizations', [MeController::class, 'organizations'])->name('meOrganizations');
        Route::get('/events', [MeController::class, 'events'])->name('meEvents');
        Route::get('/subscribes', [MeController::class, 'subscribes'])->name('meSubscribes');
        Route::get('/ctoken', [MeController::class, 'broadcastToken'])->name('meBroadcastToken');
        Route::get('/notifications', [MeController::class, 'notifications'])->name('meNotifications');
        Route::get('/applications', [MeController::class, 'applications'])->name('meApplications');
        Route::get('/possible-friends', [MeController::class, 'possibleFriends'])->name('mePossibleFriends');
    });

    Route::group(['prefix' => 'media'], function () {
        Route::post('/', [MediaController::class, 'store'])->name('mediaStore');
        Route::delete('/{media}', [MediaController::class, 'destroy'])->name('destroyMedia');
    });

    Route::group(['prefix' => 'events'], function () {
        Route::get('/', [EventController::class, 'index'])->name('eventIndex');
        Route::get('/{event}', [EventController::class, 'show'])->name('eventShow');
        Route::get('/{event}/participants', [EventController::class, 'showParticipants'])->name('eventsShowParticipants');
        Route::post('/{event}/participate', [EventController::class, 'participate'])->name('eventParticipate');
    });

    Route::group(['prefix' => 'organization'], function () {
        Route::get('/', [OrganizationController::class, 'index'])->name('organizationIndex');
        Route::get('/search', [OrganizationController::class, 'search'])->name('organizationSearch');
        Route::get('/{organization}', [OrganizationController::class, 'show'])->name('organizationShow');
        Route::get('/{organization}/events', [OrganizationController::class, 'events'])->name('organizationEvents');
        Route::get('/{organization}/members', [OrganizationController::class, 'members'])->name('organizationMembers');
        Route::post('/{organization}/subscribe', [OrganizationController::class, 'subscribe'])->name('organizationSubscribe');
        Route::post('/{organization}/unsubscribe', [OrganizationController::class, 'unsubscribe'])->name('organizationUnubscribe');
        Route::post('/{organization}/join', [OrganizationController::class, 'join'])->name('organizationJoin');
        Route::post('/{organization}/leave', [OrganizationController::class, 'leave'])->name('organizationLeave');
    });

    Route::group(['prefix' => 'chats'], function () {
        Route::get('/', [ChatController::class, 'index'])->name('panelChatIndex');
        Route::get('/events', [ChatController::class, 'indexEvents'])->name('panelChatIndexEvent');
        Route::get('/organizations', [ChatController::class, 'indexOrganizations'])->name('panelChatIndexOrganization');
        Route::get('/{chat}', [ChatController::class, 'show'])->name('panelChatShow');
        Route::post('/{chat}/message', [ChatController::class, 'message'])->name('panelChatMessage');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index'])->name('userIndex');
        Route::get('/search', [UserController::class, 'search'])->name('userSearch');
        Route::get('/{user}', [UserController::class, 'show'])->name('userShow');
        Route::get('/{user}/events', [UserController::class, 'events'])->name('userEvents');
        Route::post('/{user}/friends/add', [UserController::class, 'addToFriend'])->name('userAddToFriend');
        Route::post('/{user}/friends/accept', [UserController::class, 'acceptFriendRequest'])->name('userAcceptFriendRequest');
    });

    Route::group(['prefix' => 'ratings'], function () {
        Route::get('/users', [RatingController::class, 'users'])->name('ratingUsers');
        Route::get('/organizations', [RatingController::class, 'organizations'])->name('ratingOrganizations');
    });
});

Route::group(['prefix' => 'auth'], function() {
    Route::post('otp', [OneTimePasswordController::class, 'send'])->name('authOtpSend');
    Route::post('login', [LoginController::class, 'login'])->name('authLogin');
});

Route::group(['prefix' => 'city'], function() {
    Route::get('/', [CitiesController::class, 'index'])->name('citiesIndex');
    Route::get('/{city}', [CitiesController::class, 'show'])->name('citiesShow');
});

Route::group(['prefix' => 'tags'], function() {
    Route::get('/', [TagController::class, 'index'])->name('tagIndex');
});
