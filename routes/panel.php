<?php

use App\Http\Controllers\Api\Panel\ApplicationTemplateController;
use App\Http\Controllers\Api\Panel\Auth\LoginController;
use App\Http\Controllers\Api\Panel\ChatController;
use App\Http\Controllers\Api\Panel\EventController;
use App\Http\Controllers\Api\Panel\NotificationController;
use App\Http\Controllers\Api\Panel\PanelController;
use App\Http\Controllers\Api\Panel\UserController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Panel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API Panel routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum', 'ability:panel-actions'])->group(function () {
    Route::group(['prefix' => 'me'], function () {
        Route::get('/', [PanelController::class, 'show'])->name('panelMeShow');
        Route::patch('/', [PanelController::class, 'update'])->name('panelMeUpdate');
        Route::post('/media', [PanelController::class, 'storeMedia'])->name('panelMeStoreMedia');
        Route::get('/ctoken', [PanelController::class, 'broadcastToken'])->name('panelMeBroadcastToken');
    });

    Route::group(['prefix' => 'events'], function () {
        Route::get('/', [EventController::class, 'index'])->name('panelEventsIndex');
        Route::get('/{event}', [EventController::class, 'show'])->name('panelEventShow');
        Route::get('/{event}/participants', [EventController::class, 'showParticipants'])->name('panelEventShowParticipants');
        Route::post('/', [EventController::class, 'store'])->name('panelEventStore');
        Route::post('/{event}/media', [EventController::class, 'storeMedia'])->name('panelEventStoreMedia');
        Route::post('/{event}/activate', [EventController::class, 'activate'])->name('panelEventActivate');
        Route::post('/{event}/deactivate', [EventController::class, 'deactivate'])->name('panelEventDeactivate');
        Route::patch('/{event}', [EventController::class, 'update'])->name('panelEventUpdate');
        Route::delete('/{event}', [EventController::class, 'destroy'])->name('panelEventDelete');
        Route::post('/{event}/approve', [EventController::class, 'approveUser'])->name('panelEventApproveUser');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/members', [UserController::class, 'members'])->name('panelUsersMembers');
        Route::get('/subscribers', [UserController::class, 'subscribers'])->name('panelUsersSubscribers');
        Route::get('/participants', [UserController::class, 'participants'])->name('panelUsersParticipants');
        Route::get('/search', [UserController::class, 'search'])->name('panelUsersSearch');
        Route::get('/{user}', [UserController::class, 'show'])->name('panelUsersShow');
        Route::post('/{user}/invite', [UserController::class, 'invite'])->name('panelUsersInvite');
    });

    Route::group(['prefix' => 'chats'], function () {
        Route::get('/', [ChatController::class, 'index'])->name('panelChatIndex');
        Route::get('/{chat}', [ChatController::class, 'show'])->name('panelChatShow');
        Route::post('/{chat}/message', [ChatController::class, 'message'])->name('panelChatMessage');
    });

    Route::group(['prefix' => 'templates'], function () {
        Route::get('/', [ApplicationTemplateController::class, 'show'])->name('panelTemplateShow');
        Route::post('/', [ApplicationTemplateController::class, 'update'])->name('panelTemplateStore');
        Route::post('/seal', [ApplicationTemplateController::class, 'storeSeal'])->name('panelTemplateStoreSeal');
        Route::post('/sign', [ApplicationTemplateController::class, 'storeSign'])->name('panelTemplateStoreSign');
    });

    Route::post('/notifications', [NotificationController::class, 'send'])->name('panelNotificationsSend');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [LoginController::class, 'login'])->name('panelAuthLogin');
});
