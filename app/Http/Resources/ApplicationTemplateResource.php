<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\ApplicationTemplate */
class ApplicationTemplateResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $sign = $this->getMedia('signs')->first();
        $seal = $this->getMedia('seals')->first();

        return [
            'id' => $this->id,
            'general_title' => $this->general_title,
            'organization_title' => $this->organization_title,
            'sign' => $sign ? MediaResource::make($sign) : null,
            'seal' => $seal ? MediaResource::make($seal) : null,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
