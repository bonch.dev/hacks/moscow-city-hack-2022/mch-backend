<?php

namespace App\Http\Resources;

use App\Enums\MessageType;
use App\Models\Message;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Message
 */
class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'iid' => $this->iid,
            'member_id' => $this->chat_member_id,
            'message' => $this->message,
            'code' => $this->code,
            'type' => $this->type,
            'silent' => $this->silent,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
