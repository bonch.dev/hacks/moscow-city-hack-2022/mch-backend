<?php

namespace App\Http\Resources;

use App\Models\TagGroup;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin TagGroup
 */
class TagGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => __("tags.{$this->name}"),
            'tags' => TagResource::collection($this->tags),
        ];
    }
}
