<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @mixin Media
 * @responseField code 200
 */
class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return Cache::remember("media#$this->id", now()->addHour(), fn() => [
            'id' => $this->id,
            'url' => [
                'full_size' => $this->getTemporaryUrl(now()->addHour()->addMinutes(30)),
                $this->mergeWhen($this->hasGeneratedConversion('thumb'), fn() => [
                    'thumb' => $this->getTemporaryUrl(now()->addHour()->addMinutes(30),'thumb'),
                ])
            ]
        ]);
    }
}
