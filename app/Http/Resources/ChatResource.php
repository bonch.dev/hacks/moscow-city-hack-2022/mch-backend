<?php

namespace App\Http\Resources;

use App\Models\Chat;
use App\Models\Event;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\Pure;

/**
 * @mixin Chat
 */
class ChatResource extends JsonResource
{
    /**
     * User for documentation purpose
     *
     * @var User|null
     */
    static public ?User $docUser = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = $request->user();

        if ($this::$docUser) {
            $user = $this::$docUser;
            $this->members->add($this::$docUser);
        }

        $chatable_type = match ($this->chatable_type) {
            Organization::class => 'organization',
            Event::class => 'event',
        };

        $chatable = [
            'id' => $this->chatable->id,
            'name' => $this->chatable->name ?? $this->chatable->title,
            'type' => $chatable_type,
            'avatar' => $this->chatable->avatar ? MediaResource::make($this->chatable->avatar) : null,
        ];

        return [
            'id' => $this->id,
            'messages' => MessageResource::collection($this->messages),
            'members' => MemberResource::collection($this->members),
            'chatable' => $chatable,
            'chatable_type' => $chatable_type,
            'chatable_id' => $this->chatable_id,
            'is_closed' => $this->is_closed,
            'closed_at' => $this->closed_at,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
