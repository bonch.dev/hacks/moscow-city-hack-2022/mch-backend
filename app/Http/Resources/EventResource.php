<?php

namespace App\Http\Resources;

use App\Models\Event;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Event
 */
class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var Event $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'title' => $resource->title,
            'description' => $resource->description,
            'place' => $resource->place,
            'start_at' => $resource->start_at,
            'min_age' => $resource->min_age,
            'is_offline' => $resource->is_offline,
            'tags' => TagResource::collection($resource->tags),
            'avatar' => MediaResource::make($resource->avatar),
            'city' => CityResource::make($resource->city),
            'organization' => $this->whenLoaded(
                'organization',
                fn () => OrganizationResource::make($resource->organization)
            ),
            'roles' => $this->whenLoaded(
                'roles',
                fn () => RoleResource::collection($resource->roles),
            ),
            'participants_count' => $resource->participants_count ?? 0,
            $this->mergeWhen(
                $resource->is_participant_count !== null,
                ['is_participant' => $resource->is_participant_count > 0]
            ),
            'chat_id' => $this->whenLoaded(
                'chat',
                fn () => $resource->chat->id,
            )
        ];
    }
}
