<?php

namespace App\Http\Resources;

use App\Models\Organization;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Organization
 */
class OrganizationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var Organization $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,
            'contacts' => $resource->contacts,
            'avatar' => $this->whenLoaded('media', fn() => MediaResource::make($resource->avatar)),
            'users_count' => $resource->users_count ?? 0,
            'events_count' => $resource->events_count ?? 0,
            $this->mergeWhen(
                $resource->is_member_count !== null,
                [ 'is_member' => $resource->is_member_count > 0 ],
            ),
            $this->mergeWhen(
                $resource->is_subscriber_count !== null,
                ['is_subscriber' => $resource->is_subscriber_count > 0 ],
            ),
            'chat_id' => $this->whenLoaded(
                'chat',
                fn () => $resource->chat->id,
            )
        ];
    }
}
