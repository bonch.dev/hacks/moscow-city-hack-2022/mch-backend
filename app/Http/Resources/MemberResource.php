<?php

namespace App\Http\Resources;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\ChatMember */
class MemberResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        // dd($this->memberable::class);
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'member' => [
                $this->mergeWhen($this->memberable::class === Organization::class, [
                    'organization' => OrganizationResource::make($this->memberable)
                ]),
                $this->mergeWhen($this->memberable::class === User::class, [
                    'user' => UserResource::make($this->memberable)
                ]),
            ],

            'chat_id' => $this->chat_id,
        ];
    }
}
