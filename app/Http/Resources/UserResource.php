<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * @mixin User
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        $requestUser = $request->user();

        $checkFriendship =
            $requestUser &&
            ($requestUser::class === User::class) &&
            ($requestUser->id != $this->id);

        return [
            'id' => $this->id,
            'name' => $this->name,
            $this->mergeWhen($checkFriendship, fn () => [
                'is_friend' => $requestUser->isFriendWith($this->resource)
            ]),
            'avatar' => MediaResource::make($this->avatar),
            'description' => $this->description,
            'age' => $this->age,
            'score' => $this->score,
            'level' => $this->level,
            'need_points_for_new_level' => $this->need_to_new_level,
            'city' => CityResource::make($this->city),
            $this->mergeWhen($this->getAttribute('recently_created'), [
                'recently_created' => $this->getAttribute('recently_created'),
            ])
        ];
    }
}
