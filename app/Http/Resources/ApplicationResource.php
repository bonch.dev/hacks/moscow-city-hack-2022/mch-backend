<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\EventApplication */
class ApplicationResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'event' => EventResource::make($this->event),
            'organization' => OrganizationResource::make($this->organization),
            'template' => ApplicationTemplateResource::make($this->organization->template),
        ];
    }
}
