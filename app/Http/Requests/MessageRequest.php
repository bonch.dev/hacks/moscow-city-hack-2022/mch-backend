<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @bodyParam message string Message of chat message. Example: Test chat message! Listen me!
 * @bodyParam iid string IID of chat message. Example: EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E
 *
 * @property-read string $message
 * @property-read string $iid
 */
class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string|max:4096',
            'iid' => 'string|nullable',
        ];
    }
}
