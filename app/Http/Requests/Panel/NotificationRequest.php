<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read string $message
 *
 * @bodyParam message string required Message of notification. Example: Test notification message! Listen me!
 */
class NotificationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'message' => 'required|string|max:255',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
