<?php

namespace App\Http\Requests\Panel\ApplicationTemplate;

use Illuminate\Foundation\Http\FormRequest;

class UpdateApplicationTemplateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'organization_title' => 'string|max:255',
            'general_title' => 'string|max:255',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
