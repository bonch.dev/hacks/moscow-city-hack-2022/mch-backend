<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read string $name
 * @property-read string $description
 * @property-read string $contacts
 *
 * Scribe
 * @bodyParam name string Username. Example: Pallam
 * @bodyParam description string User story (organization self-description). Example: Beautiful text about myself
 * @bodyParam contacts string Contact information. Example: Phone: +79110297197; Email: kargin.go@bonch.dev
 */
class OrganizationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'string',
                'max:255',
            ],
            'description' => [
                'string',
                'nullable',
                'max:4096',
            ],
            'contacts' => [
                'string',
                'nullable',
                'max:1024',
            ]
        ];
    }
}
