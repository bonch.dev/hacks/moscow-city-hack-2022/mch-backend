<?php

namespace App\Http\Requests\Panel\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 * Scribe
 * @bodyParam title string required Event title. Example: my best event.
 * @bodyParam description string Event description. Example: best events for best peoples in best world.
 * @bodyParam place string Events place. Example: MoscowCityHack
 * @bodyParam start_at date required DateTime, when events starts. Example:  2022-12-03 15:42:05
 * @bodyParam min_age int required Min age of participants of events. Example: 18
 * @bodyParam is_offline bool Is events offline or online. Example: true
 * @bodyParam city_id int required City of event. Example: 1
 * @bodyParam tags int[] Id of tags, used for this event. Example: [1, 2, 3]
 * @bodyParam roles.*.name string Role name. Example: Best role
 * @bodyParam roles.*.description string Role description. Example: best best role for best best organization
 */
class StoreEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:255'
            ],

            'description' => [
                'string',
                'nullable',
                'max:4096'
            ],

            'place' => [
                'string',
                'nullable',
                'max:255'
            ],

            'start_at' => [
                'required',
                'date',
                'after:now',
            ],

            'min_age' => [
                'required',
                'int',
            ],

            'is_offline' => [
                'required',
                'bool',
            ],

            'city_id' => [
                'required',
                'exists:cities,id',
            ],

            'tags' => 'array|max:3',
            'tags.*' => 'exists:tags,id',

            'roles' => [
                'array'
            ],
            'roles.*.name' => [
                'string',
                'max:255',
            ],
            'roles.*.description' => [
                'string',
                'nullable',
                'max:4096',
            ]
        ];
    }
}
