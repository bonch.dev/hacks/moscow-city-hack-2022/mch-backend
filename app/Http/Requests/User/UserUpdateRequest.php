<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read string $name
 * @property-read integer $birthday
 * @property-read string $description
 *
 * Scribe
 * @bodyParam name string Username. Example: Pallam
 * @bodyParam birthday string User birthday. Must be earlier than total 18 age. Example: 1997-04-22
 * @bodyParam description string User story (user self-description). Example: Beautiful text about myself
 * @bodyParam language string User language setting. Available languages: ru_RU, en_US. Example: ru_RU
 * @bodyParam city_id int User city. Example: 1
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'string',
                'max:255',
            ],
            'birthday' => ['date'],
            'description' => [
                'string',
                'nullable',
                'max:4096',
            ],
            'city_id' => [
                'exists:cities,id',
            ]
        ];
    }
}
