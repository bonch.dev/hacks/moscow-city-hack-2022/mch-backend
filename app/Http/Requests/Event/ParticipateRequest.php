<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read array<int> $roles
 *
 * @bodyParam roles int[] Nullable Id of roles, used for participate. Example: [1, 2, 3]
 */
class ParticipateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'roles' => [
                'array',
            ],
            'roles.*' => [
                'exists:roles,id'
            ]
        ];
    }
}
