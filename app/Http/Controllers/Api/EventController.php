<?php

namespace App\Http\Controllers\Api;

use App\Events\FriendParticipateEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Event\ParticipateRequest;
use App\Http\Resources\EventResource;
use App\Http\Resources\UserResource;
use App\Models\ChatMember;
use App\Models\Event;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @group User Events
 * @authenticated
 */
class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:participate,event')->only('participate');
    }

    /**
     * Show available events
     *
     * @apiResourceCollection App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,participants,organization,organization.media,tags.tagGroup paginate=10
     */
    public function index()
    {
        /** @var User $user */
        $user = request()->user();

        return EventResource::collection(
            Event::aged($user)
                ->with([
                    'media',
                    'tags',
                    'roles',
                    'organization.media'
                ])
                ->withCount([
                    'participants',
                    'participants as is_participant_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    }
                ])
                ->paginate()
        );
    }

    /**
     * Show information about event
     *
     * @apiResource App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,participants,organization,organization.media,tags.tagGroup,roles
     *
     * @param Event $event
     * @return EventResource
     */
    public function show(Event $event)
    {
        /** @var User $user */
        $user = request()->user();

        return EventResource::make(
            $event->loadMissing([
                'media',
                'organization.media',
                'tags',
                'roles',
            ])->loadCount([
                'participants',
                'participants as is_participant_count' => function (Builder $query) use ($user) {
                    $query->where('user_id', $user->id);
                },
            ])
        );
    }

    /**
     * Show participants of selected event
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city
     *
     * @param Event $event
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function showParticipants(Event $event)
    {
        return UserResource::collection(
            $event
                ->participants()
                ->withPivot('role_ids')
                ->with(['media', 'city'])
                ->paginate()
        );
    }

    /**
     * Send participate request for join to event.
     *
     * @param Event $event
     * @param ParticipateRequest $request
     * @return JsonResponse
     */
    public function participate(Event $event, ParticipateRequest $request)
    {
        /** @var User $user */
        $user = $request->user();

        $roles = $request->roles;
        if (!$roles) {
            $roles = [];
        }

        $event->participants()->attach($user, ['role_ids' => $roles]);

        $chatMember = ChatMember::make();
        $chatMember->chat()->associate($event->chat);
        $chatMember->memberable()->associate($user);
        $chatMember->save();

        FriendParticipateEvent::dispatch($user, $event);

        return response()->json([
            'message' => 'participated'
        ]);
    }
}
