<?php

namespace App\Http\Controllers\Api\Panel\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\Auth\LoginRequest;
use App\Http\Resources\OrganizationResource;
use App\Models\Organization;
use denis660\Centrifugo\Centrifugo;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Str;

/**
 * @group Panel Auth
 */
class LoginController extends Controller
{
    /**
     * Login function
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request, Centrifugo $centrifugo)
    {
        $login = $request->login;

        $organization = Organization::firstOrNew(['login' => $login]);

        if ($organization->exists) {
            if ( ! Hash::check($request->password, $organization->password)) {
                return response()->json([
                    'message' => 'not authorized',
                    'error' => 'password mismatched',
                ], 401);
            }

            $organization->loadMissing(['media', 'chat']);
        } else {
            $organization->password = Hash::make($request->password);
            $organization->save();
        }

        $token = $organization->createToken(Str::random(), ['panel-actions']);

        $centrifugoToken = $centrifugo->generateConnectionToken("organization::$organization->id");

        $organization->setAttribute('recentlyAuthenticated', true);

        return response()->json([
            'message' => 'authenticated',
            'data' => [
                'access_token' => $token->plainTextToken,
                'broadcast_token' => $centrifugoToken,
                'organization' => OrganizationResource::make($organization)
            ]
        ]);
    }
}
