<?php

namespace App\Http\Controllers\Api\Panel;

use App\Events\OrganizationInviteEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\UserResource;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

/**
 * @group Panel Users
 */
class UserController extends Controller
{
    /**
     * Show information about user
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city
     *
     * @param User $user
     * @return void
     */
    public function show(User $user)
    {
        return UserResource::make(
            $user->loadMissing([
                'media',
                'city',
            ])
        );
    }


    /**
     * Show organization members
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function members()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        return UserResource::collection(
            $organization->users()
                ->with([
                    'media',
                    'city'
                ])
                ->paginate()
        );
    }

    /**
     * Show organization subscribers
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function subscribers()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        return UserResource::collection(
            $organization->subscribers()
                ->with([
                    'media',
                    'city'
                ])
                ->paginate()
        );
    }

    /**
     * Show participants of organizations events
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function participants()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        return UserResource::collection(
            $organization->participants()
                ->whereDoesntHave('organizations', function (Builder $query) use ($organization) {
                    $query->where('id', $organization->id);
                })
                ->with([
                    'media',
                    'city'
                ])
                ->paginate()
        );
    }

    /**
     * Return users from search.
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     */
    public function search(SearchRequest $request)
    {
        $users = User::search($request->search)->paginate();

        $users->load(['media', 'city']);

        return UserResource::collection($users);
    }

    /**
     * Invite user to organization
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     *
     * @response {"message":"invite sent"}
     */
    public function invite(User $user)
    {
        /** @var Organization $organization */
        $organization = request()->user();

        OrganizationInviteEvent::dispatch($organization, $user);

        return response()->json([
            'message'=> 'invite sent'
        ]);
    }
}
