<?php

namespace App\Http\Controllers\Api\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Media\StoreMediaRequest;
use App\Http\Requests\Panel\ApplicationTemplate\UpdateApplicationTemplateRequest;
use App\Http\Resources\ApplicationTemplateResource;
use App\Models\Organization;

/**
 * @group Panel Application Template
 */
class ApplicationTemplateController extends Controller
{
    /**
     * Show current application template
     *
     * @apiResource App\Http\Resources\ApplicationTemplateResource
     * @apiResourceModel App\Models\ApplicationTemplate
     *
     * @return ApplicationTemplateResource|\Illuminate\Http\JsonResponse
     */
    public function show()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        $template = $organization->template()->firstOrCreate();

        return ApplicationTemplateResource::make($template);
    }

    /**
     * Update current application template
     *
     * @apiResource App\Http\Resources\ApplicationTemplateResource
     * @apiResourceModel App\Models\ApplicationTemplate
     *
     * @param UpdateApplicationTemplateRequest $request
     * @return ApplicationTemplateResource
     */
    public function update(UpdateApplicationTemplateRequest $request)
    {
        /** @var Organization $organization */
        $organization = request()->user();

        $template = $organization->template()->firstOrCreate();

        $template->update($request->validated());

        return ApplicationTemplateResource::make($template);
    }

    /**
     * Store seal for application template
     *
     * @param StoreMediaRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     *
     * @response {"message":"stored application template seal"}
     */
    public function storeSeal(StoreMediaRequest $request)
    {
        /** @var Organization $organization */
        $organization = request()->user();

        $file = $request->file('file');

        $template = $organization->template()->first();

        $template->clearMediaCollection('seals');

        $template->addMedia($file)
            ->setFileName(sprintf('media.%s', $file->extension()))
            ->setName('media')
            ->toMediaCollection('seals', 's3');

        return response()->json([
            'message' => 'stored application template seal',
        ]);
    }

    /**
     * Store sign to application template
     *
     * @param StoreMediaRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     *
     * @response {"message":"stored application template sign"}
     */
    public function storeSign(StoreMediaRequest $request)
    {
        /** @var Organization $organization */
        $organization = request()->user();

        $file = $request->file('file');

        $template = $organization->template()->first();

        $template->clearMediaCollection('signs');

        $template->addMedia($file)
            ->setFileName(sprintf('media.%s', $file->extension()))
            ->setName('media')
            ->toMediaCollection('signs', 's3');

        return response()->json([
            'message' => 'stored application template sign',
        ]);
    }
}
