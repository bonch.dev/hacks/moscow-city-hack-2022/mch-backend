<?php

namespace App\Http\Controllers\Api\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Media\StoreMediaRequest;
use App\Http\Requests\Panel\Event\ApproveUserRequest;
use App\Http\Requests\Panel\Event\StoreEventRequest;
use App\Http\Requests\Panel\Event\UpdateEventRequest;
use App\Http\Resources\EventResource;
use App\Http\Resources\MediaResource;
use App\Http\Resources\UserResource;
use App\Models\Event;
use App\Models\EventApplication;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Laravel\Sanctum\PersonalAccessToken;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Panel Events
 */
class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:update,event')->only('update');
        $this->middleware('can:activate,event')->only('activate');
        $this->middleware('can:deactivate,event')->only('deactivate');
        $this->middleware('can:destroy,event')->only('destroy');
        $this->middleware('can:approveUser,event')->only('approveUser');
    }

    /**
     * Shows events of current organization with pagination
     *
     * @apiResourceCollection App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=organization,organization.media,tags.tagGroup paginate=10
     */
    public function index()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        return EventResource::collection(
            $organization
                ->events()
                ->with([
                    'media',
                    'organization.media',
                    'tags',
                    'roles',
                    'chat',
                ])
                ->withCount('participants')
                ->paginate(),
        );
    }

    /**
     * Shows information of selected event
     *
     * @apiResource App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,roles,organization,organization.media,tags.tagGroup,roles
     *
     * @param Event $event
     * @return EventResource
     */
    public function show(Event $event)
    {
        return EventResource::make(
            $event->loadMissing([
                'media',
                'organization.media',
                'tags',
                'chat',
                'roles',
            ])->loadCount('participants')
        );
    }

    /**
     * Show participants of selected event
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city
     *
     * @param Event $event
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function showParticipants(Event $event)
    {
        return UserResource::collection(
            $event
                ->participants()
                ->withPivotValue('role_ids')
                ->with(['media', 'city'])
                ->paginate()
        );
    }

    /**
     * Store a newly created event.
     *
     * @apiResource App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,roles,organization,organization.media,tags.tagGroup
     * @apiResourceAdditional title="Event created" message="Event successful created"
     */
    public function store(StoreEventRequest $request)
    {
        /** @var Organization $organization */
        $organization = $request->user();

        $validated = collect($request->validated());
        $tags = $validated->get('tags');
        $roles = $validated->get('roles');

        $event = Event::make($validated->except(['tags', 'roles'])->toArray());

        $event->organization()->associate($organization);

        $event->save();

        if ($tags && count($tags) > 0) {
            $event->tags()->sync($tags);
        }

        if ($roles && count($roles) > 0) {
            $event->setRelation('roles', $event->roles()->createMany($roles));
        }

        return EventResource::make($event)->additional([
            'title' => __('Event created'),
            'message' => __('Event successful created'),
        ]);
    }

    /**
     * Update event.
     *
     * @apiResource App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,roles,organization,organization.media,tags.tagGroup
     * @apiResourceAdditional title="Event created" message="Event successful created"
     */
    public function update(Event $event, UpdateEventRequest $request)
    {
        $validated = collect($request->validated());
        $tags = $validated->get('tags');
        $roles = $validated->get('roles');

        $event->update($validated->except(['tags'])->toArray());

        if ($tags && count($tags)) {
            $event->tags()->sync($tags);
        }

        if ($roles && count($roles)) {
            $event->roles()->delete();
            $event->setRelation('roles', $event->roles()->createMany($roles));
        }

        return EventResource::make($event)->additional([
            'title' => __('Event updated'),
            'message' => __('Event successful updated'),
        ]);
    }

    /**
     * Store (or replace it) media for recently created event
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function storeMedia(Event $event, StoreMediaRequest $request)
    {
        $file = $request->file('file');

        $mediables = $event->addSingleMedia($file);

        return response()->json([
            'data' => MediaResource::collection($mediables),
            'title' => __('Images stored'),
            'message' => __('Image stored successfully')
        ], Response::HTTP_CREATED);
    }

    /**
     * Activate (Publish) stored event
     *
     * @apiResource App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,organization,organization.media,tags.tagGroup
     *
     * @param Event $event
     * @return EventResource
     */
    public function activate(Event $event)
    {
        $event->is_published = true;
        $event->save();

        return EventResource::make($event)
            ->additional([
                'message' => 'event activated successfully',
            ]);
    }

    /**
     * Deactivate (unpublish) stored event
     *
     * @apiResource App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,organization,organization.media,tags.tagGroup
     *
     * @param Event $event
     * @return EventResource
     */
    public function deactivate(Event $event)
    {
        $event->is_published = false;
        $event->save();

        return EventResource::make($event)
            ->additional([
                'message' => 'event deactivated successfully',
            ]);
    }

    /**
     * Delete event.
     *
     * @param Event $event
     * @return JsonResponse
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return response()->json([
            'message' => 'event deleted successfully',
        ]);
    }

    /**
     * Approve, that user had been on event
     *
     * @param Event $event
     * @param ApproveUserRequest $request
     * @return JsonResponse
     *
     * @response {"message": "succesfully approved user for event"}
     */
    public function approveUser(Event $event, ApproveUserRequest $request)
    {
        /** @var Organization $code */
        $organization = $request->user();

        $code = $request->code;
        $token = PersonalAccessToken::findToken($code);

        if ($token === null) {
            return response()->json([
                'message' => 'code invalid',
            ], 429);
        }

        $user = $token->tokenable;
        if ($user::class != User::class) {
            return response()->json([
                'message' => 'code does not match any user',
            ], 404);
        }

        if (EventApplication::where('event_id', $event->id)
            ->where('user_id', $user->id)
            ->where('organization_id', $organization->id)
            ->exists()
        ) {
            return response()->json([
                'message' => 'user already approved',
            ], 409);
        }

         EventApplication::create([
            'event_id' => $event->id,
            'user_id' => $user->id,
            'organization_id' => $organization->id,
        ]);

        return response()->json([
            'message' => 'successfully approved user for event',
        ]);
    }
}
