<?php

namespace App\Http\Controllers\Api\Panel;

use App\Events\OrganizationNotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\NotificationRequest;
use App\Models\Organization;

/**
 * @group Panel Notifications
 */
class NotificationController extends Controller
{
    /**
     * Send notification for users
     *
     * @param NotificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @response {"message":"sending notification"}
     */
    public function send(NotificationRequest $request)
    {
        /** @var Organization $organization */
        $organization = $request->user();

        OrganizationNotificationEvent::dispatch($organization, $request->message);

        return response()->json([
            'message' => 'sending notification',
        ]);
    }
}
