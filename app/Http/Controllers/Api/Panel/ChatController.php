<?php

namespace App\Http\Controllers\Api\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use App\Http\Resources\ChatResource;
use App\Http\Resources\MessageResource;
use App\Models\Chat;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @group Panel Chats
 */
class ChatController extends Controller
{
    /**
     * Show current organizations chats
     *
     * @apiResourceCollection App\Http\Resources\ChatResource
     * @apiResourceModel App\Models\Chat with=members,members.memberable,messages paginate=10
     */
    public function index()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        return ChatResource::collection(
            $organization
                ->chats()
                ->with([
                    'chatable',
                    'members',
                    'members.memberable.media',
                    'messages' => fn($query) => $query
                        ->orderByDesc('updated_at')
                        ->orderByDesc('id')
                        ->limit(10),
                ])
                ->paginate()
        );
    }

    /**
     * Show chat resource
     *
     * @urlParam chat integer required The ID of chat.
     *
     * @apiResource App\Http\Resources\ChatResource
     * @apiResourceModel App\Models\Chat with=members,members.memberable,messages
     */
    public function show(Chat $chat): ChatResource
    {
        return new ChatResource(
            $chat->loadMissing([
                'chatable',
                'members',
                'members.memberable.media',
                'messages' => fn($query) => $query
                    ->orderByDesc('updated_at')
                    ->orderByDesc('id')
                    ->limit(10),
            ])
        );
    }

    /**
     * Show messages in chat resource
     *
     * @urlParam chat integer required The ID of chat. Example: 1
     *
     * @apiResourceCollection App\Http\Resources\MessageResource
     * @apiResourceModel App\Models\Message
     */
    public function messages(Chat $chat): AnonymousResourceCollection
    {
        return MessageResource::collection(
            $chat->messages()
                ->orderByDesc('updated_at')
                ->orderByDesc('id')
                ->paginate(30)
        );
    }

    /**
     * Send message to chat
     *
     * @urlParam chat integer required The ID of chat, used for sending message. Example: 48
     * @apiResource App\Http\Resources\MessageResource
     * @apiResourceModel App\Models\Message
     */
    public function message(MessageRequest $request, Chat $chat): MessageResource
    {
        /** @var Organization $organization */
        $organization = $request->user();

        $member = $chat->members()->whereHasMorph(
            'memberable',
            Organization::class,
            function (Builder $query) use ($organization) {
                $query->where('id', $organization->id);
            }
        )->first();

        $message = $chat->sendMessage(
            $member->id,
            $request->message,
            $request->iid,
        );

        return MessageResource::make($message)->additional([
            'title' => 'message sent',
            'message' => 'message sent'
        ]);
    }
}
