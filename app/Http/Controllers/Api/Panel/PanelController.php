<?php

namespace App\Http\Controllers\Api\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Media\StoreMediaRequest;
use App\Http\Requests\Panel\OrganizationUpdateRequest;
use App\Http\Resources\MediaResource;
use App\Http\Resources\OrganizationResource;
use App\Models\Organization;
use denis660\Centrifugo\Centrifugo;
use Illuminate\Http\JsonResponse;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Panel Organization
 */
class PanelController extends Controller
{
    /**
     * Display information about current organization.
     *
     * @apiResource App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media
     */
    public function show()
    {
        /** @var Organization $organization */
        $organization = request()->user();

        return OrganizationResource::make(
            $organization->load([
                'media',
                'chat'
            ])->loadCount('users')
        );
    }

    /**
     * Update information about current organization.
     *
     * @apiResource App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization
     */
    public function update(OrganizationUpdateRequest $request)
    {
        /** @var Organization $organization */
        $organization = $request->user();

        $organization->update($request->validated());

        return OrganizationResource::make(
            $organization->loadMissing([
                'media',
            ])
        )->additional([
            'message' => 'organization updated successfully',
        ]);
    }

    /**
     * Update media for current organization.
     *
     * @apiResource App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function storeMedia(StoreMediaRequest $request)
    {
        /** @var Organization $organization */
        $organization = $request->user();
        $file = $request->file('file');

        $mediables = $organization->addSingleMedia($file);

        return response()->json([
            'data' => MediaResource::collection($mediables),
            'title' => __('Images stored'),
            'message' => __('Image stored successfully')
        ], Response::HTTP_CREATED);
    }

    /**
     * Generate Centrifugo Broadcast authentication token for current user
     *
     * @param Centrifugo $centrifugo
     * @return JsonResponse
     */
    public function broadcastToken(Centrifugo $centrifugo): JsonResponse
    {
        /** @var Organization $organization */
        $organization = request()->user();

        $centrifugoToken = $centrifugo->generateConnectionToken("organization::$organization->id");

        return response()->json([
            'data' => [
                'broadcast_token' => $centrifugoToken,
            ]
        ]);
    }
}
