<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @group City
 */
class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @apiResourceCollection App\Http\Resources\CityResource
     * @apiResourceModel App\Models\City
     */
    public function index(): AnonymousResourceCollection
    {
        return CityResource::collection(City::paginate());
    }

    /**
     * Show information about specified City.
     *
     * @apiResource App\Http\Resources\CityResource
     * @apiResourceModel App\Models\City
     */
    public function show(City $city): CityResource
    {
        return CityResource::make($city);
    }
}
