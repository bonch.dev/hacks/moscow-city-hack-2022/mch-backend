<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\EventResource;
use App\Http\Resources\OrganizationResource;
use App\Models\ChatMember;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * @group User Organizations
 */
class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:subscribe,organization')->only('subscribe');
        $this->middleware('can:unsubscribe,organization')->only('unsubscribe');
        $this->middleware('can:join,organization')->only('join');
        $this->middleware('can:leave,organization')->only('leave');
    }

    /**
     * Show available organizations
     *
     * @apiResourceCollection App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        /** @var User $user */
        $user = request()->user();

        return OrganizationResource::collection(
            Organization::with([
                'media',
            ])
                ->orderBy('events_count', 'desc')
                ->withCount([
                    'users',
                    'events',
                    'users as is_member_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    },
                    'subscribers as is_subscriber_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    }
                ])
                ->paginate()
        );
    }

    /**
     * Show information about organization
     *
     * @apiResource App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media
     *
     * @param Organization $organization
     * @return OrganizationResource
     */
    public function show(Organization $organization)
    {
        /** @var User $user */
        $user = request()->user();

        return OrganizationResource::make(
            $organization
                ->loadMissing(['media'])
                ->loadCount([
                    'users',
                    'events',
                    'users as is_member_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    },
                    'subscribers as is_subscriber_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    }
                ])
        );
    }


    /**
     * Show members in organization
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     *
     * @param Organization $organization
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function members(Organization $organization)
    {
        return OrganizationResource::collection(
            $organization
                ->users()
                ->with(['media', 'city'])
                ->paginate(),
        );
    }

    /**
     * Show events created by organization
     *
     * @apiResourceCollection App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,organization.media paginate=10
     *
     * @param Organization $organization
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function events(Organization $organization)
    {
        return EventResource::collection(
            $organization
                ->events()
                ->with([
                    'media',
                    'organization.media',
                ])
                ->paginate()
        );
    }

    /**
     * Return organization from search...
     *
     * @apiResourceCollection App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media paginate=10
     *
     */
    public function search(SearchRequest $request)
    {
        $organizations = Organization::search($request->search)->paginate();

        $organizations->load(['media']);

        return OrganizationResource::collection($organizations);
    }

    /**
     * Subscribe for notifications from organizations
     *
     * @param Organization $organization
     * @return \Illuminate\Http\JsonResponse
     *
     * @response scenario=success {
     *   "message": "subscribed successfully"
     * }
     */
    public function subscribe(Organization $organization)
    {
        /** @var User $user */
        $user = request()->user();

        $user->subscribes()->attach($organization, touch: false);

        return response()->json([
            'message' => 'subscribed successfully'
        ]);
    }

    /**
     * Unsubscribe for notifications from organizations
     *
     * @param Organization $organization
     * @return \Illuminate\Http\JsonResponse
     *
     * @response scenario=success {
     *   "message": "unsubscribed successfully"
     * }
     */
    public function unsubscribe(Organization $organization)
    {
        /** @var User $user */
        $user = request()->user();

        $user->subscribes()->detach($organization, touch: false);

        return response()->json([
            'message' => 'unsubscribed successfully'
        ]);
    }

    /**
     * Join onto organizations
     *
     * @param Organization $organization
     * @return \Illuminate\Http\JsonResponse
     *
     * @response scenario=success {
     *   "message": "joined successfully"
     * }
     */
    public function join(Organization $organization)
    {
        /** @var User $user */
        $user = request()->user();

        $user->organizations()->attach($organization, touch: false);

        $chatMember = ChatMember::make();
        $chatMember->chat()->associate($organization->chat);
        $chatMember->memberable()->associate($user);
        $chatMember->save();

        return response()->json([
            'message' => 'joined successfully'
        ]);
    }

    /**
     * Leave from organizations
     *
     * @param Organization $organization
     * @return \Illuminate\Http\JsonResponse
     *
     * @response scenario=success {
     *   "message": "leaved successfully"
     * }
     */
    public function leave(Organization $organization)
    {
        /** @var User $user */
        $user = request()->user();

        $user->organizations()->detach($organization, touch: false);

        return response()->json([
            'message' => 'leaved successfully'
        ]);
    }
}
