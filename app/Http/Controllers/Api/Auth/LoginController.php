<?php

namespace App\Http\Controllers\Api\Auth;

use App\Exceptions\LoginFailedThrowable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\Auth\OneTimePassword;
use App\Models\User;
use denis660\Centrifugo\Centrifugo;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Propaganistas\LaravelPhone\PhoneNumber;

/**
 * @group User Auth
 */
class LoginController extends Controller
{
    /**
     * Login function
     *
     * @param LoginRequest $request
     * @param Centrifugo $centrifugo
     * @return JsonResponse
     */
    public function login(LoginRequest $request, Centrifugo $centrifugo)
    {
        $phoneNumber = PhoneNumber::make($request->phone)->ofCountry('RU')->formatE164();

        $otp = OneTimePassword::alive()
            ->wherePhone($phoneNumber)
            ->whereCode($request->code)
            ->firstOr(['*'], function () {
                throw new LoginFailedThrowable();
            });

        $otp->delete();

        $user = User::firstOrCreate(['phone' => $phoneNumber]);

        if (! $user->wasRecentlyCreated) {
            $user->loadMissing(['media']);
        } else {
            $user->setAttribute('recently_created', true);
        }

        $token = $user->createToken(Str::random(), ['user-actions']);

        $centrifugoToken = $centrifugo->generateConnectionToken("user::$user->id");

        $user->setAttribute('recentlyAuthenticated', true);

        return response()->json([
            'message' => 'authenticated',
            'data' => [
                'access_token' => $token->plainTextToken,
                'broadcast_token' => $centrifugoToken,
                'user' => UserResource::make($user)
            ]
        ]);
    }
}
