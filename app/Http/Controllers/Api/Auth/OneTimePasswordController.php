<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\OneTimePasswordSendRequest;
use App\Models\Auth\OneTimePassword;
use App\Services\UCaller\UCaller;
use Illuminate\Http\JsonResponse;
use Propaganistas\LaravelPhone\PhoneNumber;

/**
 * @group User Auth
 */
class OneTimePasswordController extends Controller
{
    /**
     * Creates and send OTP to specified PhoneNumber.
     * Type of OTP generates might be changed with backend Feature Flags
     * <br> In Dev Mode it always: <b>1234</b>
     *
     * @param OneTimePasswordSendRequest $request
     * @return JsonResponse
     */
    public function send(OneTimePasswordSendRequest $request): JsonResponse
    {
        $phoneNumber = PhoneNumber::make($request->phone)->ofCountry('RU')->formatE164();

        $otp = OneTimePassword::active()->wherePhone($phoneNumber)->firstOrNew();
        if ($otp->retries > 1) {
            return response()->json([
                'title' => __('Call limit reached.'),
                'message' => __('That\'s all for now. Try again in ~5 minute')
            ], 429);
        }

        $otp = UCaller::perform($phoneNumber, $otp);

        if (! $otp->additional_data['status']) {
            return response()->json([
                'title' => __('We can\'t check it now. Try again in ~5 minutes.'),
                'message' => __('Our OTP service is full and asleep. We\'ll wake him up as soon as possible.'),
                'error' => $otp->additional_data['error'],
            ], 409);
        }

        $otp->save();

        return response()->json([
            'message' => 'called'
        ]);
    }
}
