<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\EventResource;
use App\Http\Resources\UserResource;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

/**
 * @group User Users
 */
class UserController extends Controller
{
    /**
     * Return informations about users in system
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection(
            User::with([
                'media',
                'city',
            ])
                ->orderBy('score', 'desc')
                ->paginate()
        );
    }

    /**
     * Show information about user
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return UserResource::make(
            $user->loadMissing([
                'media',
                'city',
            ])
        );
    }

    /**
     * Show events participated by user
     *
     * @apiResourceCollection App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,organization.media paginate=10
     *
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function events(User $user)
    {
        return EventResource::collection(
            $user->events()
                ->with([
                    'media',
                    'organization.media',
                ])
                ->paginate()
        );
    }

    /**
     * Return users from search.
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     */
    public function search(SearchRequest $request)
    {
        $users = User::search($request->search)->paginate();

        $users->load(['media', 'city']);

        return UserResource::collection($users);
    }

    /**
     * Add user to friendlist
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     *
     * @response {"message":"successfully sent friendship request"}
     */
    public function addToFriend(User $user)
    {
        /** @var User $requestUser */
        $requestUser = request()->user();

        if ($requestUser->hasSentFriendRequestTo($user)) {
            return response()->json([
                'message' => 'you already sent a friend request to this user',
            ], 409);
        }

        $requestUser->befriend($user);

        return response()->json([
            'message' => 'successfully sent friendship request',
        ]);
    }

    /**
     * Accept friend request
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     *
     * @response {"message":"successfully accepted friendship request"}
     */
    public function acceptFriendRequest(User $user)
    {
        /** @var User $requestUser */
        $requestUser = request()->user();

        if (! $requestUser->hasFriendRequestFrom($user)) {
            return response()->json([
                'message' => 'this user doesnt sent you a friend request'
            ], 404);
        }

        $requestUser->acceptFriendRequest($user);

        return response()->json([
            'message' => 'successfully accepted friendship request',
        ]);
    }
}
