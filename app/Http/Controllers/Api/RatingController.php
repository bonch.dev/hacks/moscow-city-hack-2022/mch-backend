<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrganizationResource;
use App\Http\Resources\UserResource;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

/**
 * @group User Ratings
 */
class RatingController extends Controller
{
    /**
     * Show users ratings in city
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function users()
    {
        return UserResource::collection(
            User::orderBy('score', 'desc')
                ->where('city_id', request()->user()->city_id)
                ->with(['media', 'city'])
                ->paginate()
        );
    }

    /**
     * Show organizations ratings
     *
     * @apiResourceCollection App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media,users,events paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function organizations()
    {
        /** @var User $user */
        $user = request()->user();

        return OrganizationResource::collection(
            Organization::with([
                'media',
            ])
                ->orderBy('events_count', 'desc')
                ->withCount([
                    'users',
                    'events',
                    'users as is_member_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    },
                    'subscribers as is_subscriber_count' => function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    }
                ])
                ->paginate()
        );
    }
}
