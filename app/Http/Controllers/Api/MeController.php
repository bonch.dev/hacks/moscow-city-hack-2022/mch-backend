<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\ApplicationResource;
use App\Http\Resources\EventResource;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\OrganizationResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use denis660\Centrifugo\Centrifugo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @group User Me
 * @authenticated
 */
class MeController extends Controller
{
    /**
     * Display information about current user.
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media
     */
    public function show(): UserResource
    {
        /** @var User $user */
        $user = request()->user();

        return UserResource::make(
            $user->loadMissing([
                'media',
                'city',
            ])
        );
    }

    /**
     * Display organizations of current user (when user is member)
     *
     * @apiResourceCollection App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media paginate=10
     */
    public function organizations()
    {
        /** @var User $user */
        $user = request()->user();

        return OrganizationResource::collection(
            $user
                ->organizations()
                ->with(['media', 'chat'])
                ->paginate()
        );
    }

    /**
     * Display events of current user (when user is participant)
     *
     * @apiResourceCollection App\Http\Resources\EventResource
     * @apiResourceModel App\Models\Event with=media,organization,organization.media,participants paginate=10
     */
    public function events()
    {
        /** @var User $user */
        $user = request()->user();

        return EventResource::collection(
            $user
                ->events()
                ->with([
                    'media',
                    'tags',
                    'chat',
                    'organization.media'
                ])
                ->withCount('participants')
                ->paginate()
        );
    }

    /**
     * Display subscribes of current user
     *
     * @apiResourceCollection App\Http\Resources\OrganizationResource
     * @apiResourceModel App\Models\Organization with=media paginate=10
     */
    public function subscribes()
    {
        /** @var User $user */
        $user = request()->user();

        return OrganizationResource::collection(
            $user
                ->subscribes()
                ->with(['media'])
                ->paginate()
        );
    }

    /**
     * Update information about current user.
     * After <b>FIRST</b> update, user WILL BE registered.
     *
     * @param UserUpdateRequest $request
     * @return JsonResponse
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media
     */
    public function update(UserUpdateRequest $request)
    {
        $user = $request->user();

        if ($user->registered_at == null) {
            $user->registered_at = now();
        }

        $user->update($request->validated());

        return UserResource::make($user)
            ->additional([
                'message' => 'user updated successfully',
            ]);
    }

    /**
     * Generate Centrifugo Broadcast authentication token for current user
     *
     * @param Centrifugo $centrifugo
     * @return JsonResponse
     */
    public function broadcastToken(Centrifugo $centrifugo): JsonResponse
    {
        /** @var User $user */
        $user = request()->user();

        $centrifugoToken = $centrifugo->generateConnectionToken("user::$user->id");

        return response()->json([
            'data' => [
                'broadcast_token' => $centrifugoToken,
            ]
        ]);
    }

    /**
     * Show friends of current user
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city paginate=10
     */
    public function friends()
    {
        return UserResource::collection(
            request()->user()
                ->friends()
                ->with(['media', 'city'])
                ->paginate()
        );
    }

    /**
     * Show users notifications
     *
     * @apiResourceCollection App\Http\Resources\NotificationResource
     * @apiResourceModel App\Models\Notification paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function notifications()
    {
        return NotificationResource::collection(
            request()->user()
                ->notifications()
        );
    }

    /**
     * Show information about user event applications
     *
     * @apiResourceCollection App\Http\Resources\ApplicationResource
     * @apiResourceModel App\Models\EventApplication with=event,organization,organization.template paginate=10
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function applications()
    {
        return ApplicationResource::collection(
            request()->user()
                ->applications()
                ->with([
                    'event',
                    'organization.template'
                ])
                ->paginate()
        );
    }

    /**
     * Show possible friends (max 10, cause it slow)
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User with=media,city
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function possibleFriends()
    {
        $user = request()->user();

        $friendsID = $user->friends()->pluck('id');

        $organizationsUserIDs = $user
            ->organizations()
            ->with(['users'])
            ->whereNotIn('organization_user.user_id', $friendsID)
            ->pluck('organization_user.user_id');

        $eventsUserIDs = $user
            ->events()
            ->with(['users'])
            ->whereNotIn('participants.user_id', $friendsID)
            ->pluck('participants.user_id');

        $possibleFriends = $organizationsUserIDs->merge($eventsUserIDs)->take(10);

        return UserResource::collection(
            User::whereIn('id', $possibleFriends)->with(['media', 'city'])->get()
        );
    }
}
