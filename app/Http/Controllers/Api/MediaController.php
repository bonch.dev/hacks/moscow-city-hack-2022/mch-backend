<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Media\StoreMediaRequest;
use App\Http\Resources\MediaResource;
use App\Models\Media;
use Illuminate\Http\JsonResponse;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group User Media
 * @authenticated
 */
class MediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:destroy,media')->only('destroy');
    }

    /**
     * Store media file
     *
     * <br> Media resources enabled for download ~5 minutes after request
     * <br> All Media resources, by default, has -1 (primary) ordering.
     * <br> Max uploaded media of user = 5. If more, will return error.
     *
     * @param StoreMediaRequest $request
     * @return JsonResponse
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function store(StoreMediaRequest $request)
    {
        $user = $request->user();
        $file = $request->file('file');

        $mediables = $user->addSingleMedia($file);

        return response()->json([
            'data' => MediaResource::collection($mediables),
            'title' => __('Images stored'),
            'message' => __('Image stored successfully')
        ], Response::HTTP_CREATED);
    }

    /**
     * Deleting media, uploaded by user
     *
     * @urlParam media integer required The ID of media, used for deletion. Example: 1
     *
     * @param Media $media
     * @return JsonResponse
     *
     * @response {"title":"Изображение удалено","message":"Изображение успешно удалено"}
     */
    public function destroy(Media $media): JsonResponse
    {
        if ($media->delete()) {
            return response()->json([
                'title' => __('Image deleted'),
                'message' => __('Image deleted succesfully'),
            ], Response::HTTP_OK);
        }

        return response()->json([
            'title' => __('Image not deleted'),
            'message' => __('Conflict on message deleting'),
        ], Response::HTTP_CONFLICT);
    }
}
