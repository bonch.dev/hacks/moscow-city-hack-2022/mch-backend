<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagGroupResource;
use App\Models\TagGroup;

/**
 * @group Tag
 */
class TagController extends Controller
{
    /**
     * Return list of present tags
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $lang = request()->getPreferredLanguage();

        return \Cache::rememberForever("tag:$lang#index", function () {
            return TagGroupResource::collection(TagGroup::all());
        });
    }
}
