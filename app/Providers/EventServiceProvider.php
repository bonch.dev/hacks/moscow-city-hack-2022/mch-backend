<?php

namespace App\Providers;

use App\Enums\NotificationType;
use App\Events\FriendParticipateEvent;
use App\Events\OrganizationCreateEvent;
use App\Events\OrganizationInviteEvent;
use App\Events\OrganizationNotificationEvent;
use App\Listeners\FriendshipListener;
use App\Models\Event;
use App\Models\EventApplication;
use App\Models\Message;
use App\Models\Notification;
use App\Models\Organization;
use App\Observers\EventApplicationObserver;
use App\Observers\EventObserver;
use App\Observers\MessageObserver;
use App\Observers\OrganizationObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event as EventFacade;
use function Illuminate\Events\queueable;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'acq.friendships.sent' => [
            FriendshipListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     * @noinspection PhpParamsInspection
     */
    public function boot()
    {
        EventFacade::listen(queueable(function (FriendParticipateEvent $event) {
            foreach ($event->user->friends()->get() as $friend) {
                Notification::FriendParticipateEvent($event->user, $friend, $event->event);
            }
        })->onConnection('redis'));

        EventFacade::listen(queueable(function (OrganizationCreateEvent $event) {
            foreach ($event->organization->subscribers as $target) {
                Notification::OrganizationCreateEvent($event->organization, $target, $event->event);
            }
            foreach ($event->organization->users as $target) {
                Notification::OrganizationCreateEvent($event->organization, $target, $event->event);
            }
        })->onConnection('redis'));

        EventFacade::listen(queueable(function (OrganizationNotificationEvent $event) {
            foreach ($event->organization->subscribers as $target) {
                Notification::OrganizationNotification($event->organization, $target, $event->message);
            }
            foreach ($event->organization->users as $target) {
                Notification::OrganizationNotification($event->organization, $target, $event->message);
            }
        })->onConnection('redis'));

        EventFacade::listen(queueable(function (OrganizationInviteEvent $event) {
            Notification::OrganizationInvite($event->organization, $event->user);
        }));

        Event::observe(EventObserver::class);
        Message::observe(MessageObserver::class);
        Organization::observe(OrganizationObserver::class);
        EventApplication::observe(EventApplicationObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
