<?php

namespace App\Enums;

class MessageType
{
    public const MESSAGE = "message";
    public const SYSTEM = "system";

    /**
     * @return string[]
     */
    public static function toArray(): array
    {
        return [self::MESSAGE, self::SYSTEM];
    }
}
