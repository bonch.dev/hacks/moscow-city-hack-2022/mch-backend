<?php

namespace App\Enums;

class NotificationType
{
    public const FRIENDSHIP_REQUEST = "friendship_request";
    public const FRIEND_PARTICIPATE_EVENT = "friend_participate_event";
    public const ORGANIZATION_NOTIFICATION = "organization_notification";
    public const ORGANIZATION_EVENT = "organization_event";
    public const ORGANIZATION_INVITE = "organization_invite";

    /**
     * @return string[]
     */
    public static function toArray(): array
    {
        return [
            self::FRIENDSHIP_REQUEST,
            self::FRIEND_PARTICIPATE_EVENT,
            self::ORGANIZATION_NOTIFICATION,
            self::ORGANIZATION_EVENT,
            self::ORGANIZATION_INVITE,
        ];
    }
}
