<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response as AccessResponse;
use Symfony\Component\HttpFoundation\Response;

class EventPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function update(Organization $organization, Event $event): bool
    {
        return $event->organization_id == $organization->id;
    }

    public function activate(Organization $organization, Event $event): bool
    {
        return $event->organization_id == $organization->id;
    }

    public function deactivate(Organization $organization, Event $event): bool
    {
        return $event->organization_id == $organization->id;
    }

    public function destroy(Organization $organization, Event $event): bool
    {
        return $event->organization_id == $organization->id;
    }

    public function approveUser(Organization $organization, Event $event): bool
    {
        return $event->organization_id == $organization->id;
    }

    public function participate(User $user, Event $event): AccessResponse
    {
        if ($user->participations()->where('event_id', $event->id)->exists()) {
            return AccessResponse::deny('Вы уже участвуете в этом событии', Response::HTTP_GONE);
        }

        return AccessResponse::allow();
    }
}
