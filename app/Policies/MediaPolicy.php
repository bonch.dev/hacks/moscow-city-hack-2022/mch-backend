<?php

namespace App\Policies;

use App\Models\Media;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MediaPolicy
{
    use HandlesAuthorization;

    public function destroy(User $user, Media $media): bool
    {
        return $media->model_id == $user->id && $media->model_type == $user::class;
    }
}
