<?php

namespace App\Policies;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response as AccessResponse;
use Symfony\Component\HttpFoundation\Response;

class OrganizationPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function subscribe(User $user, Organization $organization): AccessResponse
    {
        if ($user->subscribes()->wherePivot('organization_id', $organization->id)->exists()) {
            return AccessResponse::deny('Вы уже подписаны на эту организацию', Response::HTTP_GONE);
        }

        return AccessResponse::allow();
    }

    public function unsubscribe(User $user, Organization $organization): AccessResponse
    {
        if (! $user->subscribes()->wherePivot('organization_id', $organization->id)->exists()) {
            return AccessResponse::deny('Вы не подписаны на эту организацию', Response::HTTP_GONE);
        }

        return AccessResponse::allow();
    }

    public function join(User $user, Organization $organization): AccessResponse
    {
        if ($user->organizations()->wherePivot('organization_id', $organization->id)->exists()) {
            return AccessResponse::deny('Вы уже вступили в эту организацию', Response::HTTP_GONE);
        }

        return AccessResponse::allow();
    }

    public function leave(User $user, Organization $organization): AccessResponse
    {
        if (! $user->organizations()->wherePivot('organization_id', $organization->id)->exists()) {
            return AccessResponse::deny('Вы не находитесь в этой организации', Response::HTTP_GONE);
        }

        return AccessResponse::allow();
    }
}
