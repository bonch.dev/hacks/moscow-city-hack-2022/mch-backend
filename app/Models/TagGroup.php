<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class TagGroup extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name'];

    protected $with = ['tags'];

    public function tags(): HasMany
    {
        return $this->hasMany(Tag::class);
    }
}
