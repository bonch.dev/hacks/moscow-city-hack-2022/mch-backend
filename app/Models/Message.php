<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

class Message extends Model implements HasMedia
{
    use HasFactory, HasEagerLimit, InteractsWithMedia;

    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected static function booted()
    {
        static::addGlobalScope('ordering', function (Builder $builder) {
            $builder->orderByDesc('created_at');
        });
    }

    protected $fillable = [
        'iid',
        'message',
        'code',
        'type',
        'silent',
        'chat_member_id',
        'chat_id',
    ];

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }

    public function member(): BelongsTo
    {
        return $this->belongsTo(ChatMember::class);
    }
}
