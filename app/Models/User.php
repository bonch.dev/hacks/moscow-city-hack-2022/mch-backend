<?php

namespace App\Models;

use App\Services\Scorer;
use App\Traits\AddSingleMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Attributes\SearchUsingFullText;
use Laravel\Scout\Attributes\SearchUsingPrefix;
use Laravel\Scout\Searchable;
use Multicaret\Acquaintances\Traits\Friendable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Friendable, SoftDeletes,
        InteractsWithMedia, AddSingleMedia, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'city_id',
        'phone',
        'birthday',
        'description',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'birthday' => 'date',
    ];

    #[SearchUsingPrefix(['name'])]
    #[SearchUsingFullText(['phone', 'description'])]
    public function toSearchableArray()
    {
        return [
            'phone' => $this->phone,
            'name' => $this->name,
            'description' => $this->description,
        ];
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function participations(): HasMany
    {
        return $this->hasMany(Participant::class);
    }

    public function events(): BelongsToMany
    {
        return $this
            ->belongsToMany(Event::class, 'participants')
            ->using(Participant::class);
    }

    public function subscribes(): BelongsToMany
    {
        return $this->belongsToMany(Organization::class, 'subscribers');
    }

    public function organizations(): BelongsToMany
    {
        return $this->belongsToMany(Organization::class, 'organization_user');
    }

    public function chatMember(): MorphMany
    {
        return $this->morphMany(ChatMember::class, 'memberable');
    }

    public function chats(): HasManyThrough
    {
        return $this
            ->hasManyThrough(
                Chat::class,
                ChatMember::class,
                'memberable_id',
                'id',
                'id',
                'chat_id'
            )
            ->where('memberable_type', static::class);
    }

    public function notifications(): HasMany
    {
        return $this->hasMany(Notification::class);
    }

    public function applications(): HasMany
    {
        return $this->hasMany(EventApplication::class);
    }

    public function getAgeAttribute(): ?int
    {
        return $this->birthday?->age;
    }

    public function getLevelAttribute(): int
    {
        return Scorer::currentLevel($this->score ?? 0);
    }

    public function getNeedToNewLevelAttribute(): int
    {
        return Scorer::needForLevel($this->level + 1) - $this->score;
    }

    public function getAvatarAttribute()
    {
        return $this->media()->first();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(100 * 4)
            ->height(125 * 4);
    }
}
