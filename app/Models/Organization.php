<?php

namespace App\Models;

use App\Traits\AddSingleMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Attributes\SearchUsingFullText;
use Laravel\Scout\Attributes\SearchUsingPrefix;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Organization extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, HasRelationships,
        SoftDeletes, InteractsWithMedia, AddSingleMedia, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'login',
        'name',
        'description',
        'contacts',
    ];

    protected $hidden = [
        'password',
    ];

    #[SearchUsingPrefix(['name'])]
    #[SearchUsingFullText(['name', 'description'])]
    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
        ];
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    public function subscribers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'subscribers');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'organization_user');
    }

    public function participants(): HasManyThrough
    {
        return $this->hasManyDeep(User::class, [
            Event::class,
            Participant::class,
        ]);
    }

    public function chatMember(): MorphMany
    {
        return $this->morphMany(ChatMember::class, 'memberable');
    }

    public function chat(): MorphOne
    {
        return $this->morphOne(Chat::class, 'chatable');
    }

    public function chats(): HasManyThrough
    {
        return $this
            ->hasManyThrough(
                Chat::class,
                ChatMember::class,
                'memberable_id',
                'id',
                'id',
                'chat_id'
            )
            ->where('memberable_type', static::class);
    }

    public function template(): HasOne
    {
        return $this->hasOne(ApplicationTemplate::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(100 * 4)
            ->height(100 * 4);
    }

    public function getAvatarAttribute()
    {
        return $this->media()->first();
    }

}
