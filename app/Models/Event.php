<?php

namespace App\Models;

use App\Traits\AddSingleMedia;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Event extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, InteractsWithMedia, AddSingleMedia;

    protected $fillable = [
        'title',
        'description',
        'place',
        'start_at',
        'min_age',
        'is_offline',
        'city_id',
    ];

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function participations(): HasMany
    {
        return $this->hasMany(Participant::class);
    }

    public function participants(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class, 'participants')
            ->using(Participant::class);
    }

    public function getAvatarAttribute()
    {
        return $this->media()->first();
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function chat(): MorphOne
    {
        return $this->morphOne(Chat::class, 'chatable');
    }

    public function roles(): HasMany
    {
        return $this->hasMany(Role::class);
    }

    public function scopeAged(Builder $query, User $user): Builder
    {
        return $query->where('min_age', '<=', $user->age ?? 0);
    }
}
