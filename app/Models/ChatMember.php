<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property-read string $memberable_type
 * @property-read int $memberable_id
 */
class ChatMember extends Model
{
    use HasFactory;

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }

    public function memberable(): MorphTo
    {
        return $this->morphTo();
    }
}
