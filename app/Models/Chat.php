<?php

namespace App\Models;

use App\Enums\MessageType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Ramsey\Uuid\Uuid;

class Chat extends Model
{
    use HasFactory;

    public function chatable(): MorphTo
    {
        return $this->morphTo();
    }

    public function members(): HasMany
    {
        return $this->hasMany(ChatMember::class);
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }

    public function sendMessage(
        $memberID,
        string $message,
        ?string $iid = null,
        bool $silent = false,
    ): Message|Model
    {
        if ($iid === "" || $iid === null) {
            $iid = Uuid::uuid4()->toString();
        }

        $this->updateTimestamps();
        $this->save();

        return $this->messages()->create([
            'iid' => $iid,
            'type' => MessageType::MESSAGE,
            'chat_member_id' => $memberID,
            'message' => $message,
            'silent' => $silent,
        ]);
    }

    public function sendSystemMessage(
        $memberID,
        string $code,
        bool $silent = false,
    ): Message|Model
    {
        $this->updateTimestamps();
        $this->save();

        return $this->messages()->create([
            'type' => MessageType::SYSTEM,
            'member_id' => $memberID,
            'message' => __($code),
            'code' => $code,
            'silent' => $silent,
        ]);
    }
}
