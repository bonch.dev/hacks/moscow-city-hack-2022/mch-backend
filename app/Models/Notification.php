<?php

namespace App\Models;

use App\Enums\NotificationType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected static function booted()
    {
        static::addGlobalScope('ordering', function (Builder $builder) {
            $builder->orderByDesc('created_at');
        });
    }

    protected $fillable = [
        'type',
        'data',
    ];

    protected $casts = [
        'data' => 'object',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function FriendshipRequest(User $sender, User $target) {
        $notification = Notification::make([
            'type' => NotificationType::FRIENDSHIP_REQUEST,
            'data' => [
                'sender_type' => 'user',
                'sender_id' => $sender->id,
            ],
        ]);

        $notification->user()->associate($target);

        $notification->save();
    }

    public static function FriendParticipateEvent(User $sender, User $target, Event $event)
    {
        $notification = Notification::make([
            'type' => NotificationType::FRIEND_PARTICIPATE_EVENT,
            'data' => [
                'sender_id' => $sender->id,
                'sender_type' => 'user',
                'event_id' => $event->id,
            ]
        ]);

        $notification->user()->associate($target);

        $notification->save();
    }

    public static function OrganizationCreateEvent(Organization $sender, User $target, Event $event)
    {
        $notification = Notification::make([
            'type' => NotificationType::ORGANIZATION_EVENT,
            'data' => [
                'sender_id' => $sender->id,
                'sender_type' => 'organization',
                'event_id' => $event->id,
            ]
        ]);

        $notification->user()->associate($target);

        $notification->save();
    }

    public static function OrganizationNotification(Organization $sender, User $target, string $message)
    {
        $notification = Notification::make([
            'type' => NotificationType::ORGANIZATION_EVENT,
            'data' => [
                'sender_id' => $sender->id,
                'sender_type' => 'organization',
                'message' => $message,
            ]
        ]);

        $notification->user()->associate($target);

        $notification->save();
    }

    public static function OrganizationInvite(Organization $sender, User $target)
    {
        $notification = Notification::make([
            'type' => NotificationType::ORGANIZATION_INVITE,
            'data' => [
                'sender_id' => $sender->id,
                'sender_type' => 'organization',
            ]
        ]);

        $notification->user()->associate($target);

        $notification->save();
    }
}
