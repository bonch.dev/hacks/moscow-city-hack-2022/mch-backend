<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OneTimePassword extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'phone',
        'requested_at',
    ];

    protected $casts = [
        'additional_data' => 'array'
    ];

    public function scopeActive(Builder $query): Builder
    {
        return $query->whereRaw('requested_at > now() - interval \'1 minute\'');
    }

    public function scopeAlive(Builder $query): Builder
    {
        return $query->whereRaw('requested_at > now() - interval \'1 hour\'');
    }
}
