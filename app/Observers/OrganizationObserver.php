<?php

namespace App\Observers;

use App\Models\ApplicationTemplate;
use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\Organization;

class OrganizationObserver
{
    public function created(Organization $organization)
    {
        $chat = Chat::make();
        $chat->chatable()->associate($organization);
        $chat->save();

        $member = ChatMember::make();
        $member->chat()->associate($chat);
        $member->memberable()->associate($organization);
        $member->save();

        $organization->template()->create();
    }
}
