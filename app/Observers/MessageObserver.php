<?php

namespace App\Observers;

use App\Events\ChatMessageEvent;
use App\Models\Message;

class MessageObserver
{
    /**
     * Handle the Message "created" event.
     *
     * @param  \App\Models\Message  $message
     * @return void
     */
    public function created(Message $message)
    {
        ChatMessageEvent::dispatch($message);
    }
}
