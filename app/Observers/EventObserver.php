<?php

namespace App\Observers;

use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\Event;

class EventObserver
{
    public function created(Event $event)
    {
        $chat = Chat::make();
        $chat->chatable()->associate($event);
        $chat->save();

        $member = ChatMember::make();
        $member->chat()->associate($chat);
        $member->memberable()->associate($event->organization);
        $member->save();
    }
}
