<?php

namespace App\Observers;

use App\Models\EventApplication;
use App\Services\Scorer;

class EventApplicationObserver
{
    public function created(EventApplication $eventApplication)
    {
        $user = $eventApplication->user;
        $user->score = $user->score + Scorer::AddScoreCount;
        $user->save();
    }
}
