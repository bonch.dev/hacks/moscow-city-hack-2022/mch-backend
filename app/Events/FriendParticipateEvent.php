<?php

namespace App\Events;

use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FriendParticipateEvent
{
    use Dispatchable, SerializesModels;

    public User $user;
    public Event $event;

    public function __construct(User $user, Event $event)
    {
        $this->event = $event;
        $this->user = $user;
    }
}
