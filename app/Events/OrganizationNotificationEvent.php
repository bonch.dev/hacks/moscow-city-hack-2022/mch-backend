<?php

namespace App\Events;

use App\Models\Organization;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrganizationNotificationEvent
{
    use Dispatchable, SerializesModels;

    public Organization $organization;
    public string $message;

    public function __construct(Organization $organization, string $message)
    {
        $this->organization = $organization;
        $this->message = $message;
    }
}
