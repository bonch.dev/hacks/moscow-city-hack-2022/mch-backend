<?php

namespace App\Events;

use App\Http\Resources\MediaResource;
use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\Event;
use App\Models\Media;
use App\Models\Message;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Message $message;

    /**
     * The name of the queue connection to use when broadcasting the event.
     *
     * @var string
     */
    public $connection = 'sync';

    /**
     * The name of the queue on which to place the broadcasting job.
     *
     * @var string
     */
    public $queue = 'default';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn(): array
    {
        $channels = [];

        $chatID = $this->message->chat_id;

        $userIDs = \Cache::remember("chat:user_ids#$chatID", 300, function () {
            $memberIDs = [];
            $members = $this->message->chat->members;

            /** @var ChatMember $member */
            foreach ($members as $member) {
                switch ($member->memberable_type) {
                    case User::class:
                        $memberIDs[] = "user::$member->memberable_id"; break;
                    case Organization::class:
                        $memberIDs[] = "organization::$member->memberable_id"; break;
                }
            }

            return $memberIDs;
        });

        foreach ($userIDs as $userId) {
            $channels[] = (string)(new Channel("chats:user#$userId"));
        }

        return $channels;
    }

    public function broadcastWith(): array
    {
        $chatID = $this->message->chat_id;
        $memberID = $this->message->chat_member_id;

        /** @var Chat $chat */
        $chat = \Cache::remember("chat:data#$chatID", 300, function () {
            return $this->message
                ->chat()
                ->with([
                    'members.memberable',
                ])
                ->first();
        });

        $memberResource = [
            'id' => 0,
            'type' => 'system',
            'name' => 'System',
            'avatar' => null,
        ];

        if ($memberID != null) {
            $memberResource = \Cache::remember("chat:user_resource#$memberID", 300, function () {
                $sender = ChatMember::find($this->message->chat_member_id);

                /** @var User|Organization $memberable */
                $memberable = $sender->memberable;

                /** @var null|Media $avatar */
                $avatar = $memberable->avatar;

                return [
                    'id' => $memberable->id,
                    'type' => match ($memberable::class) {
                        Organization::class => 'organization',
                        User::class => 'user',
                    },
                    'name' => $memberable->name,
                    'avatar' => $avatar
                        ? MediaResource::make($memberable->avatar)
                        : null,
                ];
            });
        }

        $chatableType = match ($chat->chatable_type) {
            Organization::class => 'organization',
            Event::class => 'event',
            default => 'unknown',
        };

        return [
            'message' => [
                'id' => $this->message->id,
                'iid' => $this->message->iid,
                'message' => $this->message->message,
                'type' => $this->message->type,
                'code' => $this->message->code,
                'chatable_id' => $chat->chatable_id,
                'chatable_type' => $chatableType,
                'user' => $memberResource,
                'member_id' => $memberID,
                'chat_id' => $chatID,
                'created_at' => $this->message->created_at,
                'updated_at' => $this->message->updated_at,
            ]
        ];
    }
}
