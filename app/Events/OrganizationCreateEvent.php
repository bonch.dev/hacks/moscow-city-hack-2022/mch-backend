<?php

namespace App\Events;

use App\Models\Event;
use App\Models\Organization;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrganizationCreateEvent
{
    use Dispatchable, SerializesModels;

    public Organization $organization;
    public Event $event;

    public function __construct(Organization $organization, Event $event)
    {
        $this->organization = $organization;
        $this->event = $event;
    }
}
