<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    protected function invalidJson($request, ValidationException $exception)
    {
        return response()->json([
            'title' => __($exception->getMessage()),
            'message' => __('Some fields were filled in incorrectly.'),
            'errors' => $exception->errors(),
        ], $exception->status);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'title' => __('Unauthenticated.'),
                'message' => __('You must be authenticated, to perform this request.'),
                'error' => $exception->getMessage(),
            ], 401);
        }

        // return a plain 401 response even when not a json call
        abort(401);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Convert the given exception to an array.
     *
     * @param  \Throwable  $e
     * @return array
     */
    protected function convertExceptionToArray(Throwable $e): array
    {
        $collection = collect([
            'message' => 'Server error'
        ]);

        if ($e instanceof CustomErrorThrowable) {
            $collection->put('title', $e->getTitle());
        } else if ($e instanceof AccessDeniedHttpException) {
            $collection->put('title', __('Action forbidden'));
        }

        if (config('app.debug') || $this->isHttpException($e)) {
            $collection->put('message', $e->getMessage());
        }

        if (config('app.debug')) {
            $collection->merge(collect([
                'exception' => get_class($e),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => collect($e->getTrace())->map(function ($trace) {
                    return Arr::except($trace, ['args']);
                })->all(),
            ]));
        }

        return $collection->toArray();
    }
}
