<?php

namespace App\Exceptions;

class LoginFailedThrowable extends CustomErrorException
{
    public function __construct()
    {
        parent::__construct(
            401,
            __('Authorization failed'),
            __('You tried to login, using invalid credentials')
        );
    }
}
