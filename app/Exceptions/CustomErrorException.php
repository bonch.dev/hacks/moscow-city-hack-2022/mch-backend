<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomErrorException extends HttpException
{
    protected string $title;

    public function __construct(int $statusCode, ?string $title = '', ?string $message = '')
    {
        $this->title = $title;

        parent::__construct($statusCode, $message, null, [], 0);
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
