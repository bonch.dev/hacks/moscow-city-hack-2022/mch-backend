<?php

namespace App\Exceptions;

interface CustomErrorThrowable extends \Throwable
{
    /**
     * Returns Title of Custom Error
     *
     * @return string
     */
    public function getTitle(): string;
}
