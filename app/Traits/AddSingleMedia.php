<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

trait AddSingleMedia
{
    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function addSingleMedia(UploadedFile $file): Collection
    {
        $this->clearMediaCollection('photos');

        $mediables = collect();

        $mediables->add(
            $this->addMedia($file)
                ->setFileName(sprintf('media.%s', $file->extension()))
                ->setName('media')
                ->toMediaCollection('photos', 's3')
        );

        return $mediables;
    }
}
