<?php

namespace App\Listeners;

use App\Enums\NotificationType;
use App\Models\Notification;
use App\Models\User;

class FriendshipListener
{
    public function __construct()
    {
        //
    }

    public function handle($event)
    {
        /** @var User $sender */
        $sender = $event[0];

        /** @var User $recepient */
        $recepient = $event[1];

         Notification::FriendshipRequest($sender, $recepient);
    }
}
