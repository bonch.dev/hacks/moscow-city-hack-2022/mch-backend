<?php

namespace App\Services\UCaller;

use App\Models\Auth\OneTimePassword;
use Carbon\Carbon;
use Propaganistas\LaravelPhone\PhoneNumber;

class UCaller
{
    public static function perform(string $phoneNumber, ?OneTimePassword $otp): OneTimePassword
    {
        if (\Unleash::isFeatureEnabled('ucaller_active')) {
            return self::performReal($phoneNumber, $otp);
        }

        return self::performMock($phoneNumber, $otp);
    }

    /**
     * @throws \Exception
     */
    protected static function performReal(string $phoneNumber, ?OneTimePassword $otp): OneTimePassword {
        if ($otp->exists) {
            $uCallerResponse = \UCaller::initRepeat(
                $otp->additional_data['ucaller_id']
            );
            $otp->retries++;
        } else {
            $otp->code = (string) random_int(1000, 9999);
            $otp->phone = PhoneNumber::make($phoneNumber)->ofCountry('RU')->formatE164();
            $otp->requested_at = Carbon::now();
            $otp->retries = 1;

            $uCallerResponse = \UCaller::initCall(
                str_replace('+', '', $otp->phone),
                $otp->code,
            );
        }

        $otp->additional_data = (array) $uCallerResponse->responseBody();

        return $otp;
    }

    protected static function performMock(string $phoneNumber, ?OneTimePassword $otp): OneTimePassword {
        $otp->code = "1234";
        $otp->phone = PhoneNumber::make($phoneNumber)->ofCountry('RU')->formatE164();
        $otp->requested_at = Carbon::now();
        $otp->additional_data = ['status' => true];
        $otp->retries++;

        return $otp;
    }
}
