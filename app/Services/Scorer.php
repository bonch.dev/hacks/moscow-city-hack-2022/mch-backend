<?php

namespace App\Services;

class Scorer
{
    const AddScoreCount = 10;

    const LevelScoreMultiplier = 20;

    public static function needForLevel(int $level): int {
        return static::LevelScoreMultiplier * (2 ** $level);
    }

    public static function currentLevel(int $score): int {
        $level = 0;

        while (true) {
            if (static::needForLevel($level++) < $score) {
                continue;
            }

            return $level - 1;
        }
    }
}
