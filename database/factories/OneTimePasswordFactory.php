<?php

namespace Database\Factories;

use App\Models\Auth\OneTimePassword;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class OneTimePasswordFactory extends Factory
{
    protected $model = OneTimePassword::class;

    public function definition(): array
    {
        return [
            'phone' => $this->faker->phoneNumber(),
            'code' => $this->faker->randomNumber(4),
            'requested_at' => Carbon::now(),
            'retries' => $this->faker->randomNumber(1),
        ];
    }
}
