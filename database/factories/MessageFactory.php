<?php

namespace Database\Factories;

use App\Enums\MessageType;
use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class MessageFactory extends Factory
{
    protected $model = Message::class;

    public function definition(): array
    {
        return [
            'iid' => $this->faker->word(),
            'chat_member_id' => ChatMember::factory(),
            'chat_id' => Chat::factory(),
            'type' => MessageType::MESSAGE,
            'code' => $this->faker->word(),
            'message' => $this->faker->word(),
            'silent' => $this->faker->boolean(),
            'deleted_at' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
