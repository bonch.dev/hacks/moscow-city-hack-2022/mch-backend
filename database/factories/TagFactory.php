<?php

namespace Database\Factories;

use App\Models\Tag;
use App\Models\TagGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement([
                'tag A',
                'tag B',
                'tag C',
                'tag D',
                'tag E',
                'tag F',
                'tag G',
                'tag H',
                'tag I',
                'tag J',
            ]),
            'tag_group_id' => TagGroup::factory()
        ];
    }
}
