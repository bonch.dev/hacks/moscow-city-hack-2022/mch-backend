<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Event;
use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    public function configure(): static
    {
        return $this->afterCreating(function (Event $event) {
            $event->loadMissing([
                'media',
                'organization',
                'organization.media',
                'city',
                'tags',
                'tags.tagGroup',
            ])
                ->loadCount('participants');
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->realTextBetween(10, 20),
            'description' => $this->faker->text,
            'place' => $this->faker->address,
            'min_age' => $this->faker->numberBetween(0, 30),
            'is_offline' => $this->faker->boolean,
            'start_at' => Carbon::now()->addMinutes($this->faker->numberBetween(int2: 60 * 24 * 7)),
            'organization_id' => Organization::factory(),
            'city_id' => City::factory(),
        ];
    }
}
