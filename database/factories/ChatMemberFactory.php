<?php

namespace Database\Factories;

use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ChatMemberFactory extends Factory
{
    protected $model = ChatMember::class;

    public function definition(): array
    {
        return [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'memberable_type' => User::class,
            'memberable_id' => User::factory(),
            'chat_id' => Chat::factory(),
        ];
    }
}
