<?php

namespace Database\Factories;

use App\Models\Event;
use App\Models\EventApplication;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class EventApplicationFactory extends Factory
{
    protected $model = EventApplication::class;

    public function definition(): array
    {
        return [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'event_id' => Event::factory(),
            'user_id' => User::factory(),
            'organization_id' => Organization::factory(),
        ];
    }
}
