<?php

namespace Database\Factories;

use App\Enums\NotificationType;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class NotificationFactory extends Factory
{
    protected $model = Notification::class;

    private $data = [
        ["type" => NotificationType::FRIENDSHIP_REQUEST, "data" =>  '{"sender_type": "user", "sender_id": 2}'],
        ["type" => NotificationType::FRIEND_PARTICIPATE_EVENT, "data" =>  '{"sender_type": "user", "sender_id": 2, "event_id": 1}'],
        ["type" => NotificationType::ORGANIZATION_NOTIFICATION, "data" =>  '{"sender_type": "organization", "sender_id": 1, "message": "message"}'],
        ["type" => NotificationType::ORGANIZATION_EVENT, "data" =>  '{"sender_type": "organization", "sender_id": 1, "event_id": 1}'],
        ["type" => NotificationType::ORGANIZATION_INVITE, "data" => '{"sender_type": "organization", "sender_id": 1}'],
    ];

    public function definition(): array
    {
        $selectedDataID = $this->faker->numberBetween(0, count($this->data) - 1);
        $selectedData = $this->data[$selectedDataID];

        return [
            'user_id' => User::factory(),
            'type' => $selectedData["type"],
            'data' => $selectedData["data"],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
