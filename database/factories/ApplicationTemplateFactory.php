<?php

namespace Database\Factories;

use App\Models\ApplicationTemplate;
use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ApplicationTemplateFactory extends Factory
{
    protected $model = ApplicationTemplate::class;

    public function definition(): array
    {
        return [
            'general_title' => $this->faker->word(),
            'organization_title' => $this->faker->word(),

            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'organization_id' => Organization::factory(),
        ];
    }
}
