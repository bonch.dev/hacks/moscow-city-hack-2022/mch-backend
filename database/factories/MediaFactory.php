<?php

namespace Database\Factories;

use App\Models\Media;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Scribe only
 */
class MediaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Media::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        /** @var Factory $model */
        $modelFactory = $this->faker->randomElement([
            User::factory(),
        ]);

        return [
            'model_id' => $modelFactory,
            'model_type' => $modelFactory->modelName(),
            'uuid' => $this->faker->uuid(),
            'collection_name' => 'photos',
            'name' => 'media',
            'file_name' => 'media.jpg',
            'mime_type' => 'image/png',
            'disk' => 's3',
            'conversions_disk' => 's3',
            'size' => 1724922,
            'generated_conversions' => [],
            'responsive_images' => [],
            'manipulations' => [],
            'custom_properties' => [],
            'order_column' => $this->faker->numberBetween(int2: 5),
        ];
    }
}
