<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'score' => $this->faker->randomNumber,
            'birthday' => $this->faker->dateTimeBetween('-30 years', '-18 years'),
            'description' => $this->faker->text,
            'registered_at' => $this->faker->dateTime,
            'city_id' => City::factory(),
        ];
    }
}
