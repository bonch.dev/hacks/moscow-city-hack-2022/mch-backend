<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->fullText('name');
            $table->fullText('description');
        });

        Schema::table('organizations', function (Blueprint $table) {
            $table->fullText('name');
            $table->fullText('description');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropFullText('name');
            $table->dropFullText('description');
        });

        Schema::table('organizations', function (Blueprint $table) {
            $table->dropFullText('name');
            $table->dropFullText('description');
        });
    }
};
