<?php

use App\Models\Tag;
use App\Models\TagGroup;
use Database\Seeders\TagGroupSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new TagGroupSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tag::whereRaw('true')->delete();
        TagGroup::whereRaw('true')->delete();
    }
};
