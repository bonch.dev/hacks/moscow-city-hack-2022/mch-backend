<?php

use App\Models\Organization;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Organization::class);

            $table->string('title');
            $table->text('description')->nullable();
            $table->string('place')->nullable();
            $table->integer('min_age')->default(0);
            $table->boolean('is_offline')->nullable();
            $table->boolean('is_published')->default(false);

            $table->timestamp('start_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('events');
    }
};
