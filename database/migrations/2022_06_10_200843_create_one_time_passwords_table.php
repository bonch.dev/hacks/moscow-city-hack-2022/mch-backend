<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('one_time_passwords', function (Blueprint $table) {
            $table->id();

            $table->string('phone');
            $table->string('code');
            $table->timestamp('requested_at');
            $table->json('additional_data')->nullable();
            $table->unsignedInteger('retries')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('one_time_passwords');
    }
};
