<?php

use App\Models\Event;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('event_applications', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Event::class);
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(Organization::class);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('event_applications');
    }
};
