<?php

use App\Models\User;
use App\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Event::class);
            $table->foreignIdFor(User::class);
            $table->jsonb('role_ids')->default('[]');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('participants');
    }
};
