<?php

use App\Models\City;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up()
    {
        collect([
            ['name' => 'Санкт-Петербург'],
            ['name' => 'Москва'],
        ])->each(function ($item, $key) {
            City::create($item);
        });
    }
};
