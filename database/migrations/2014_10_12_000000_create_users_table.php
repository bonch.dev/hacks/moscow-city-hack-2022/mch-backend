<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->date('birthday')->nullable();
            $table->text('description')->nullable();

            $table->string('phone')->nullable()->unique();
            $table->string('old_phone')->nullable();

            $table->timestamp('registered_at')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index('deleted_at');
            $table->index(['id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
