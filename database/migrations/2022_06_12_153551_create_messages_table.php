<?php

use App\Enums\MessageType;
use App\Models\Chat;
use App\Models\ChatMember;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->string('iid')->nullable();
            $table->foreignIdFor(Chat::class)->index();
            $table->foreignIdFor(ChatMember::class)->index();
            $table->enum('type', MessageType::toArray());
            $table->string('code')->nullable();
            $table->string('message', 4096)->nullable();
            $table->boolean('silent')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->index(['chat_id', 'chat_member_id']);
            $table->index('deleted_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('messages');
    }
};
