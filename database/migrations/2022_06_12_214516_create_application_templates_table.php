<?php

use App\Models\Organization;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('application_templates', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Organization::class);
            $table->string('general_title')->nullable();
            $table->string('organization_title')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('application_templates');
    }
};
