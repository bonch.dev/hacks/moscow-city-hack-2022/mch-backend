<?php

use App\Models\Chat;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('chat_members', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Chat::class);
            $table->numericMorphs("memberable");

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('chat_members');
    }
};
