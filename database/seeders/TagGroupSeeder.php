<?php

namespace Database\Seeders;

use App\Models\TagGroup;
use Illuminate\Database\Seeder;

class TagGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TagGroup::create(['name' => 'Социальное'])->tags()->createMany([
            ['name' => 'Помощь инвалидам'],
            ['name' => 'Помощь бездомным'],
            ['name' => 'Помощь детям'],
            ['name' => 'Помощь взрослым'],
        ]);

        TagGroup::create(['name' => 'Событийное'])->tags()->createMany([
            ['name' => 'Военно-патриотическое'],
            ['name' => 'Спортивное'],
            ['name' => 'Образовательное'],
            ['name' => 'Социальное'],
            ['name' => 'Культурное'],
        ]);

        TagGroup::create(['name' => 'Экологическое'])->tags()->createMany([
            ['name' => 'Уборка мусора'],
            ['name' => 'Восстановление леса'],
            ['name' => 'Работа с животными'],
            ['name' => 'Экспедиции'],
        ]);

        TagGroup::create(['name' => 'Медицинское'])->tags()->createMany([
            ['name' => 'Донорство'],
            ['name' => 'ЗОЖ'],
            ['name' => 'Уход за пациентами'],
        ]);

        TagGroup::create(['name' => 'Другое'])->tags()->createMany([
            ['name' => 'Другое'],
        ]);
    }
}
