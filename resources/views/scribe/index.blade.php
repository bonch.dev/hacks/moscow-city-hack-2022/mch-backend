<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Laravel Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("/vendor/scribe/css/theme-default.style.css") }}" media="screen">
    <link rel="stylesheet" href="{{ asset("/vendor/scribe/css/theme-default.print.css") }}" media="print">

    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

    <link rel="stylesheet"
          href="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/styles/obsidian.min.css">
    <script src="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/highlight.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jets/0.14.1/jets.min.js"></script>

    <style id="language-style">
        /* starts out as display none and is replaced with js later  */
                    body .content .bash-example code { display: none; }
                    body .content .javascript-example code { display: none; }
            </style>

    <script>
        var baseUrl = "https://mch-backend-staging.server.bonch.dev";
        var useCsrf = Boolean();
        var csrfUrl = "/sanctum/csrf-cookie";
    </script>
    <script src="{{ asset("/vendor/scribe/js/tryitout-3.29.1.js") }}"></script>

    <script src="{{ asset("/vendor/scribe/js/theme-default-3.29.1.js") }}"></script>

</head>

<body data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">

<a href="#" id="nav-button">
    <span>
        MENU
        <img src="{{ asset("/vendor/scribe/images/navbar.png") }}" alt="navbar-image" />
    </span>
</a>
<div class="tocify-wrapper">
    
            <div class="lang-selector">
                                            <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                            <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                    </div>
    
    <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>

    <div id="toc">
                                                                            <ul id="tocify-header-0" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="introduction">
                        <a href="#introduction">Introduction</a>
                    </li>
                                            
                                                                    </ul>
                                                <ul id="tocify-header-1" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="authenticating-requests">
                        <a href="#authenticating-requests">Authenticating requests</a>
                    </li>
                                            
                                                </ul>
                    
                    <ul id="tocify-header-2" class="tocify-header">
                <li class="tocify-item level-1" data-unique="city">
                    <a href="#city">City</a>
                </li>
                                    <ul id="tocify-subheader-city" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="city-GETapi-city">
                        <a href="#city-GETapi-city">Display a listing of the resource.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="city-GETapi-city--city-">
                        <a href="#city-GETapi-city--city-">Show information about specified City.</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-3" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-application-template">
                    <a href="#panel-application-template">Panel Application Template</a>
                </li>
                                    <ul id="tocify-subheader-panel-application-template" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-application-template-GETapi-panel-templates">
                        <a href="#panel-application-template-GETapi-panel-templates">Show current application template</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-application-template-POSTapi-panel-templates">
                        <a href="#panel-application-template-POSTapi-panel-templates">Update current application template</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-application-template-POSTapi-panel-templates-seal">
                        <a href="#panel-application-template-POSTapi-panel-templates-seal">Store seal for application template</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-application-template-POSTapi-panel-templates-sign">
                        <a href="#panel-application-template-POSTapi-panel-templates-sign">Store sign to application template</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-4" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-auth">
                    <a href="#panel-auth">Panel Auth</a>
                </li>
                                    <ul id="tocify-subheader-panel-auth" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-auth-POSTapi-panel-auth-login">
                        <a href="#panel-auth-POSTapi-panel-auth-login">Login function</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-5" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-chats">
                    <a href="#panel-chats">Panel Chats</a>
                </li>
                                    <ul id="tocify-subheader-panel-chats" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-chats-GETapi-panel-chats">
                        <a href="#panel-chats-GETapi-panel-chats">Show current organizations chats</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-chats-GETapi-panel-chats--chat-">
                        <a href="#panel-chats-GETapi-panel-chats--chat-">Show chat resource</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-chats-POSTapi-panel-chats--chat--message">
                        <a href="#panel-chats-POSTapi-panel-chats--chat--message">Send message to chat</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-6" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-events">
                    <a href="#panel-events">Panel Events</a>
                </li>
                                    <ul id="tocify-subheader-panel-events" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-events-GETapi-panel-events">
                        <a href="#panel-events-GETapi-panel-events">Shows events of current organization with pagination</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-GETapi-panel-events--event-">
                        <a href="#panel-events-GETapi-panel-events--event-">Shows information of selected event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-GETapi-panel-events--event--participants">
                        <a href="#panel-events-GETapi-panel-events--event--participants">Show participants of selected event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-POSTapi-panel-events">
                        <a href="#panel-events-POSTapi-panel-events">Store a newly created event.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-POSTapi-panel-events--event--media">
                        <a href="#panel-events-POSTapi-panel-events--event--media">Store (or replace it) media for recently created event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-POSTapi-panel-events--event--activate">
                        <a href="#panel-events-POSTapi-panel-events--event--activate">Activate (Publish) stored event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-POSTapi-panel-events--event--deactivate">
                        <a href="#panel-events-POSTapi-panel-events--event--deactivate">Deactivate (unpublish) stored event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-PATCHapi-panel-events--event-">
                        <a href="#panel-events-PATCHapi-panel-events--event-">Update event.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-DELETEapi-panel-events--event-">
                        <a href="#panel-events-DELETEapi-panel-events--event-">Delete event.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-events-POSTapi-panel-events--event--approve">
                        <a href="#panel-events-POSTapi-panel-events--event--approve">Approve, that user had been on event</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-7" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-notifications">
                    <a href="#panel-notifications">Panel Notifications</a>
                </li>
                                    <ul id="tocify-subheader-panel-notifications" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-notifications-POSTapi-panel-notifications">
                        <a href="#panel-notifications-POSTapi-panel-notifications">Send notification for users</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-8" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-organization">
                    <a href="#panel-organization">Panel Organization</a>
                </li>
                                    <ul id="tocify-subheader-panel-organization" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-organization-GETapi-panel-me">
                        <a href="#panel-organization-GETapi-panel-me">Display information about current organization.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-organization-PATCHapi-panel-me">
                        <a href="#panel-organization-PATCHapi-panel-me">Update information about current organization.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-organization-POSTapi-panel-me-media">
                        <a href="#panel-organization-POSTapi-panel-me-media">Update media for current organization.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-organization-GETapi-panel-me-ctoken">
                        <a href="#panel-organization-GETapi-panel-me-ctoken">Generate Centrifugo Broadcast authentication token for current user</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-9" class="tocify-header">
                <li class="tocify-item level-1" data-unique="panel-users">
                    <a href="#panel-users">Panel Users</a>
                </li>
                                    <ul id="tocify-subheader-panel-users" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="panel-users-GETapi-panel-users-members">
                        <a href="#panel-users-GETapi-panel-users-members">Show organization members</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-users-GETapi-panel-users-subscribers">
                        <a href="#panel-users-GETapi-panel-users-subscribers">Show organization subscribers</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-users-GETapi-panel-users-participants">
                        <a href="#panel-users-GETapi-panel-users-participants">Show participants of organizations events</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-users-GETapi-panel-users-search">
                        <a href="#panel-users-GETapi-panel-users-search">Return users from search.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-users-GETapi-panel-users--user-">
                        <a href="#panel-users-GETapi-panel-users--user-">Show information about user</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="panel-users-POSTapi-panel-users--user--invite">
                        <a href="#panel-users-POSTapi-panel-users--user--invite">Invite user to organization</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-10" class="tocify-header">
                <li class="tocify-item level-1" data-unique="tag">
                    <a href="#tag">Tag</a>
                </li>
                                    <ul id="tocify-subheader-tag" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="tag-GETapi-tags">
                        <a href="#tag-GETapi-tags">Return list of present tags</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-11" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-auth">
                    <a href="#user-auth">User Auth</a>
                </li>
                                    <ul id="tocify-subheader-user-auth" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-auth-POSTapi-auth-otp">
                        <a href="#user-auth-POSTapi-auth-otp">Creates and send OTP to specified PhoneNumber.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-auth-POSTapi-auth-login">
                        <a href="#user-auth-POSTapi-auth-login">Login function</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-12" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-chats">
                    <a href="#user-chats">User Chats</a>
                </li>
                                    <ul id="tocify-subheader-user-chats" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-chats-GETapi-chats">
                        <a href="#user-chats-GETapi-chats">Show current user chats</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-chats-GETapi-chats-events">
                        <a href="#user-chats-GETapi-chats-events">Show current user events chats</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-chats-GETapi-chats-organizations">
                        <a href="#user-chats-GETapi-chats-organizations">Show current user events chats</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-chats-GETapi-chats--chat-">
                        <a href="#user-chats-GETapi-chats--chat-">Show chat resource</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-chats-POSTapi-chats--chat--message">
                        <a href="#user-chats-POSTapi-chats--chat--message">Send message to chat</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-13" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-events">
                    <a href="#user-events">User Events</a>
                </li>
                                    <ul id="tocify-subheader-user-events" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-events-GETapi-events">
                        <a href="#user-events-GETapi-events">Show available events</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-events-GETapi-events--event-">
                        <a href="#user-events-GETapi-events--event-">Show information about event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-events-GETapi-events--event--participants">
                        <a href="#user-events-GETapi-events--event--participants">Show participants of selected event</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-events-POSTapi-events--event--participate">
                        <a href="#user-events-POSTapi-events--event--participate">Send participate request for join to event.</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-14" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-me">
                    <a href="#user-me">User Me</a>
                </li>
                                    <ul id="tocify-subheader-user-me" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me">
                        <a href="#user-me-GETapi-me">Display information about current user.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-PATCHapi-me">
                        <a href="#user-me-PATCHapi-me">Update information about current user.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-organizations">
                        <a href="#user-me-GETapi-me-organizations">Display organizations of current user (when user is member)</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-events">
                        <a href="#user-me-GETapi-me-events">Display events of current user (when user is participant)</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-subscribes">
                        <a href="#user-me-GETapi-me-subscribes">Display subscribes of current user</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-ctoken">
                        <a href="#user-me-GETapi-me-ctoken">Generate Centrifugo Broadcast authentication token for current user</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-notifications">
                        <a href="#user-me-GETapi-me-notifications">Show users notifications</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-applications">
                        <a href="#user-me-GETapi-me-applications">Show information about user event applications</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-me-GETapi-me-possible-friends">
                        <a href="#user-me-GETapi-me-possible-friends">Show possible friends (max 10, cause it slow)</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-15" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-media">
                    <a href="#user-media">User Media</a>
                </li>
                                    <ul id="tocify-subheader-user-media" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-media-POSTapi-media">
                        <a href="#user-media-POSTapi-media">Store media file</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-media-DELETEapi-media--media-">
                        <a href="#user-media-DELETEapi-media--media-">Deleting media, uploaded by user</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-16" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-organizations">
                    <a href="#user-organizations">User Organizations</a>
                </li>
                                    <ul id="tocify-subheader-user-organizations" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-organizations-GETapi-organization">
                        <a href="#user-organizations-GETapi-organization">Show available organizations</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-GETapi-organization--organization-">
                        <a href="#user-organizations-GETapi-organization--organization-">Show information about organization</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-GETapi-organization--organization--members">
                        <a href="#user-organizations-GETapi-organization--organization--members">Show members in organization</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-POSTapi-organization--organization--subscribe">
                        <a href="#user-organizations-POSTapi-organization--organization--subscribe">Subscribe for notifications from organizations</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-POSTapi-organization--organization--unsubscribe">
                        <a href="#user-organizations-POSTapi-organization--organization--unsubscribe">Unsubscribe for notifications from organizations</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-POSTapi-organization--organization--join">
                        <a href="#user-organizations-POSTapi-organization--organization--join">Join onto organizations</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-POSTapi-organization--organization--leave">
                        <a href="#user-organizations-POSTapi-organization--organization--leave">Leave from organizations</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-GETapi-organization-search">
                        <a href="#user-organizations-GETapi-organization-search">Return organization from search.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-organizations-GETapi-organization--organization--events">
                        <a href="#user-organizations-GETapi-organization--organization--events">Show events created by organization</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-17" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-ratings">
                    <a href="#user-ratings">User Ratings</a>
                </li>
                                    <ul id="tocify-subheader-user-ratings" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-ratings-GETapi-ratings-users">
                        <a href="#user-ratings-GETapi-ratings-users">Show users ratings</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-ratings-GETapi-ratings-organizations">
                        <a href="#user-ratings-GETapi-ratings-organizations">Show organizations ratings</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-18" class="tocify-header">
                <li class="tocify-item level-1" data-unique="user-users">
                    <a href="#user-users">User Users</a>
                </li>
                                    <ul id="tocify-subheader-user-users" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="user-users-GETapi-users">
                        <a href="#user-users-GETapi-users">Return informations about users in system</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-users-GETapi-users-search">
                        <a href="#user-users-GETapi-users-search">Return users from search.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-users-GETapi-users--user-">
                        <a href="#user-users-GETapi-users--user-">Show information about user</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-users-GETapi-users--user--events">
                        <a href="#user-users-GETapi-users--user--events">Show events participated by user</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-users-POSTapi-users--user--friends-add">
                        <a href="#user-users-POSTapi-users--user--friends-add">Add user to friendlist</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="user-users-POSTapi-users--user--friends-accept">
                        <a href="#user-users-POSTapi-users--user--friends-accept">Accept friend request</a>
                    </li>
                                                    </ul>
                            </ul>
        
                        
            </div>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI spec</a></li>
                            <li><a href="http://github.com/knuckleswtf/scribe">Documentation powered by Scribe ✍</a></li>
                    </ul>
        <ul class="toc-footer" id="last-updated">
        <li>Last updated: June 13 2022</li>
    </ul>
</div>

<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1 id="introduction">Introduction</h1>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">https://mch-backend-staging.server.bonch.dev</code></pre>

        <h1 id="authenticating-requests">Authenticating requests</h1>
<p>This API is authenticated by sending an <strong><code>Authorization</code></strong> header with the value <strong><code>"Bearer {YOUR_AUTH_KEY}"</code></strong>.</p>
<p>All authenticated endpoints are marked with a <code>requires authentication</code> badge in the documentation below.</p>
<p>You can retrieve your token by visiting your dashboard and clicking <b>Generate API token</b>.</p>

        <h1 id="city">City</h1>

    

            <h2 id="city-GETapi-city">Display a listing of the resource.</h2>

<p>
</p>



<span id="example-requests-GETapi-city">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/city" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/city"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-city">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 80,
            &quot;name&quot;: &quot;Mr. Jalon Quitzon PhD&quot;
        },
        {
            &quot;id&quot;: 81,
            &quot;name&quot;: &quot;Betsy Vandervort&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-city" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-city"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-city"></code></pre>
</span>
<span id="execution-error-GETapi-city" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-city"></code></pre>
</span>
<form id="form-GETapi-city" data-method="GET"
      data-path="api/city"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-city', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-city"
                    onclick="tryItOut('GETapi-city');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-city"
                    onclick="cancelTryOut('GETapi-city');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-city" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/city</code></b>
        </p>
                    </form>

            <h2 id="city-GETapi-city--city-">Show information about specified City.</h2>

<p>
</p>



<span id="example-requests-GETapi-city--city-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/city/1" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/city/1"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-city--city-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 82,
        &quot;name&quot;: &quot;Pearl Stanton&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-city--city-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-city--city-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-city--city-"></code></pre>
</span>
<span id="execution-error-GETapi-city--city-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-city--city-"></code></pre>
</span>
<form id="form-GETapi-city--city-" data-method="GET"
      data-path="api/city/{city}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-city--city-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-city--city-"
                    onclick="tryItOut('GETapi-city--city-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-city--city-"
                    onclick="cancelTryOut('GETapi-city--city-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-city--city-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/city/{city}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>city</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="city"
               data-endpoint="GETapi-city--city-"
               value="1"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

        <h1 id="panel-application-template">Panel Application Template</h1>

    

            <h2 id="panel-application-template-GETapi-panel-templates">Show current application template</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-templates">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/templates" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-templates">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 62,
        &quot;general_title&quot;: &quot;hic&quot;,
        &quot;organization_title&quot;: &quot;sit&quot;,
        &quot;sign&quot;: null,
        &quot;seal&quot;: null,
        &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
        &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-templates" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-templates"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-templates"></code></pre>
</span>
<span id="execution-error-GETapi-panel-templates" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-templates"></code></pre>
</span>
<form id="form-GETapi-panel-templates" data-method="GET"
      data-path="api/panel/templates"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-templates', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-templates"
                    onclick="tryItOut('GETapi-panel-templates');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-templates"
                    onclick="cancelTryOut('GETapi-panel-templates');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-templates" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/templates</code></b>
        </p>
                    </form>

            <h2 id="panel-application-template-POSTapi-panel-templates">Update current application template</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-templates">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"organization_title\": \"qwhsyqsuqyauysadmglahovdudhamlvtloqqlyrldttjvaqijhqegcaczicojlhdozfuexocsfgsckpmancrydxtggo\",
    \"general_title\": \"jsggtzvuhkigoftnammmvcmfbhvrihpskjxngsuymztitoumycopwnkntoqdxuygjlmkitvhtyzqjditxjrdadoqizqbyuwgcmeyuknjqmwdqcizljefmanotqnjqcpkfdgczfqrrraqxscxqkgzjyevjl\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "organization_title": "qwhsyqsuqyauysadmglahovdudhamlvtloqqlyrldttjvaqijhqegcaczicojlhdozfuexocsfgsckpmancrydxtggo",
    "general_title": "jsggtzvuhkigoftnammmvcmfbhvrihpskjxngsuymztitoumycopwnkntoqdxuygjlmkitvhtyzqjditxjrdadoqizqbyuwgcmeyuknjqmwdqcizljefmanotqnjqcpkfdgczfqrrraqxscxqkgzjyevjl"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-templates">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 64,
        &quot;general_title&quot;: &quot;voluptatem&quot;,
        &quot;organization_title&quot;: &quot;id&quot;,
        &quot;sign&quot;: null,
        &quot;seal&quot;: null,
        &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
        &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-templates" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-templates"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-templates"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-templates" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-templates"></code></pre>
</span>
<form id="form-POSTapi-panel-templates" data-method="POST"
      data-path="api/panel/templates"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-templates', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-templates"
                    onclick="tryItOut('POSTapi-panel-templates');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-templates"
                    onclick="cancelTryOut('POSTapi-panel-templates');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-templates" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/templates</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>organization_title</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="organization_title"
               data-endpoint="POSTapi-panel-templates"
               value="qwhsyqsuqyauysadmglahovdudhamlvtloqqlyrldttjvaqijhqegcaczicojlhdozfuexocsfgsckpmancrydxtggo"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 255 characters.</p>
        </p>
                <p>
            <b><code>general_title</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="general_title"
               data-endpoint="POSTapi-panel-templates"
               value="jsggtzvuhkigoftnammmvcmfbhvrihpskjxngsuymztitoumycopwnkntoqdxuygjlmkitvhtyzqjditxjrdadoqizqbyuwgcmeyuknjqmwdqcizljefmanotqnjqcpkfdgczfqrrraqxscxqkgzjyevjl"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 255 characters.</p>
        </p>
        </form>

            <h2 id="panel-application-template-POSTapi-panel-templates-seal">Store seal for application template</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-templates-seal">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates/seal" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"file\": \"ad\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates/seal"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "file": "ad"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-templates-seal">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;stored application template seal&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-templates-seal" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-templates-seal"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-templates-seal"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-templates-seal" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-templates-seal"></code></pre>
</span>
<form id="form-POSTapi-panel-templates-seal" data-method="POST"
      data-path="api/panel/templates/seal"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-templates-seal', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-templates-seal"
                    onclick="tryItOut('POSTapi-panel-templates-seal');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-templates-seal"
                    onclick="cancelTryOut('POSTapi-panel-templates-seal');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-templates-seal" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/templates/seal</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>file</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="file"
               data-endpoint="POSTapi-panel-templates-seal"
               value="ad"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="panel-application-template-POSTapi-panel-templates-sign">Store sign to application template</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-templates-sign">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates/sign" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"file\": \"et\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/templates/sign"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "file": "et"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-templates-sign">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;stored application template sign&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-templates-sign" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-templates-sign"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-templates-sign"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-templates-sign" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-templates-sign"></code></pre>
</span>
<form id="form-POSTapi-panel-templates-sign" data-method="POST"
      data-path="api/panel/templates/sign"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-templates-sign', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-templates-sign"
                    onclick="tryItOut('POSTapi-panel-templates-sign');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-templates-sign"
                    onclick="cancelTryOut('POSTapi-panel-templates-sign');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-templates-sign" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/templates/sign</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>file</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="file"
               data-endpoint="POSTapi-panel-templates-sign"
               value="et"
               data-component="body" hidden>
    <br>

        </p>
        </form>

        <h1 id="panel-auth">Panel Auth</h1>

    

            <h2 id="panel-auth-POSTapi-panel-auth-login">Login function</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-auth-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/auth/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"login\": \"login\",
    \"password\": \"password\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/auth/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "login": "login",
    "password": "password"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-auth-login">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 56
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;authenticated&quot;,
    &quot;data&quot;: {
        &quot;access_token&quot;: &quot;1|MPrCDmt6Vr3JitmlNKEUQCrSp7HoAG97vPBnqEJm&quot;,
        &quot;broadcast_token&quot;: &quot;eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJvcmdhbml6YXRpb246OjYxIn0.M0Y0ocqdCyQp0ZLCgeoYmkxCsvz4YSuYfvFcMjXiPwQ&quot;,
        &quot;organization&quot;: {
            &quot;id&quot;: 61,
            &quot;name&quot;: null,
            &quot;description&quot;: null,
            &quot;contacts&quot;: null,
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        }
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-auth-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-auth-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-auth-login"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-auth-login"></code></pre>
</span>
<form id="form-POSTapi-panel-auth-login" data-method="POST"
      data-path="api/panel/auth/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-auth-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-auth-login"
                    onclick="tryItOut('POSTapi-panel-auth-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-auth-login"
                    onclick="cancelTryOut('POSTapi-panel-auth-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-auth-login" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/auth/login</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>login</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="login"
               data-endpoint="POSTapi-panel-auth-login"
               value="login"
               data-component="body" hidden>
    <br>
<p>Organization Login.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-panel-auth-login"
               value="password"
               data-component="body" hidden>
    <br>
<p>Organization Password.</p>
        </p>
        </form>

        <h1 id="panel-chats">Panel Chats</h1>

    

            <h2 id="panel-chats-GETapi-panel-chats">Show current organizations chats</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-chats">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/chats" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/chats"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-chats">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 105,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 9,
                    &quot;iid&quot;: &quot;aperiam&quot;,
                    &quot;member_id&quot;: 115,
                    &quot;message&quot;: &quot;odit&quot;,
                    &quot;code&quot;: &quot;consectetur&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: false,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 111,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:27.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:27.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 60,
                            &quot;name&quot;: &quot;Dr. Sallie Yundt&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Non et iste et dolores. Qui laborum occaecati mollitia. Magni est molestiae rerum ut numquam id.&quot;,
                            &quot;age&quot;: 27,
                            &quot;score&quot;: 91727,
                            &quot;level&quot;: 13,
                            &quot;need_points_for_new_level&quot;: 235953,
                            &quot;city&quot;: {
                                &quot;id&quot;: 102,
                                &quot;name&quot;: &quot;Prof. Kurt Rau IV&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 105
                },
                {
                    &quot;id&quot;: 112,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:27.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:27.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 61,
                            &quot;name&quot;: &quot;Dr. Vern Effertz IV&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Accusamus et voluptatum laborum esse aut qui quibusdam. Numquam voluptates qui in fuga aut molestiae et. In nostrum architecto cum adipisci.&quot;,
                            &quot;age&quot;: 28,
                            &quot;score&quot;: 488,
                            &quot;level&quot;: 5,
                            &quot;need_points_for_new_level&quot;: 792,
                            &quot;city&quot;: {
                                &quot;id&quot;: 103,
                                &quot;name&quot;: &quot;Karl Raynor&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 105
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 37,
                &quot;name&quot;: &quot;Alice considered a.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 37,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:27.850654Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:27.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:27.000000Z&quot;
        },
        {
            &quot;id&quot;: 111,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 10,
                    &quot;iid&quot;: &quot;accusamus&quot;,
                    &quot;member_id&quot;: 122,
                    &quot;message&quot;: &quot;temporibus&quot;,
                    &quot;code&quot;: &quot;laborum&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: true,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 118,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 63,
                            &quot;name&quot;: &quot;Douglas Bogan&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Qui ipsum assumenda voluptate optio maiores et quo. Autem sint assumenda corrupti. Voluptatem fuga optio natus sed rerum dolorem.&quot;,
                            &quot;age&quot;: 22,
                            &quot;score&quot;: 1848611,
                            &quot;level&quot;: 17,
                            &quot;need_points_for_new_level&quot;: 3394269,
                            &quot;city&quot;: {
                                &quot;id&quot;: 107,
                                &quot;name&quot;: &quot;Alexandra Kertzmann&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 111
                },
                {
                    &quot;id&quot;: 119,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 64,
                            &quot;name&quot;: &quot;Nathan Schumm DVM&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Et dolorem sed in consequatur hic at. Quo repellat non vitae. Sit ut recusandae ad amet.&quot;,
                            &quot;age&quot;: 20,
                            &quot;score&quot;: 260965,
                            &quot;level&quot;: 14,
                            &quot;need_points_for_new_level&quot;: 394395,
                            &quot;city&quot;: {
                                &quot;id&quot;: 108,
                                &quot;name&quot;: &quot;Maiya Kiehn&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 111
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 39,
                &quot;name&quot;: &quot;Why, I do it again.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 39,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:28.105565Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-chats" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-chats"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-chats"></code></pre>
</span>
<span id="execution-error-GETapi-panel-chats" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-chats"></code></pre>
</span>
<form id="form-GETapi-panel-chats" data-method="GET"
      data-path="api/panel/chats"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-chats', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-chats"
                    onclick="tryItOut('GETapi-panel-chats');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-chats"
                    onclick="cancelTryOut('GETapi-panel-chats');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-chats" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/chats</code></b>
        </p>
                    </form>

            <h2 id="panel-chats-GETapi-panel-chats--chat-">Show chat resource</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-chats--chat-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/chats/11" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/chats/11"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-chats--chat-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 117,
        &quot;messages&quot;: [
            {
                &quot;id&quot;: 11,
                &quot;iid&quot;: &quot;commodi&quot;,
                &quot;member_id&quot;: 129,
                &quot;message&quot;: &quot;ducimus&quot;,
                &quot;code&quot;: &quot;quas&quot;,
                &quot;type&quot;: &quot;message&quot;,
                &quot;silent&quot;: true,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
            }
        ],
        &quot;members&quot;: [
            {
                &quot;id&quot;: 125,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                &quot;member&quot;: {
                    &quot;user&quot;: {
                        &quot;id&quot;: 66,
                        &quot;name&quot;: &quot;Dr. Camille Kshlerin&quot;,
                        &quot;avatar&quot;: null,
                        &quot;description&quot;: &quot;Similique et rerum vitae dolor distinctio. Id ut aut beatae rerum unde aut non aliquam. Voluptatem beatae fugiat quia voluptas molestiae velit aspernatur.&quot;,
                        &quot;age&quot;: 18,
                        &quot;score&quot;: 4257,
                        &quot;level&quot;: 8,
                        &quot;need_points_for_new_level&quot;: 5983,
                        &quot;city&quot;: {
                            &quot;id&quot;: 112,
                            &quot;name&quot;: &quot;Miss Agnes Hammes I&quot;
                        }
                    }
                },
                &quot;chat_id&quot;: 117
            },
            {
                &quot;id&quot;: 126,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
                &quot;member&quot;: {
                    &quot;user&quot;: {
                        &quot;id&quot;: 67,
                        &quot;name&quot;: &quot;Johnny Haag&quot;,
                        &quot;avatar&quot;: null,
                        &quot;description&quot;: &quot;Aut vitae deserunt autem quae ea quia nisi. Perferendis ad eaque error. Dignissimos voluptatum qui dignissimos maiores. Voluptate esse magnam et repudiandae quis. In quia quo id.&quot;,
                        &quot;age&quot;: 29,
                        &quot;score&quot;: 102874823,
                        &quot;level&quot;: 23,
                        &quot;need_points_for_new_level&quot;: 232669497,
                        &quot;city&quot;: {
                            &quot;id&quot;: 113,
                            &quot;name&quot;: &quot;Mr. Harrison Rippin&quot;
                        }
                    }
                },
                &quot;chat_id&quot;: 117
            }
        ],
        &quot;chatable&quot;: {
            &quot;id&quot;: 41,
            &quot;name&quot;: &quot;I&#039;ve got to come.&quot;,
            &quot;type&quot;: &quot;event&quot;,
            &quot;avatar&quot;: null
        },
        &quot;chatable_type&quot;: &quot;event&quot;,
        &quot;chatable_id&quot;: 41,
        &quot;is_closed&quot;: null,
        &quot;closed_at&quot;: null,
        &quot;deleted_at&quot;: &quot;2022-06-13T00:45:28.399914Z&quot;,
        &quot;created_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;,
        &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.000000Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-chats--chat-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-chats--chat-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-chats--chat-"></code></pre>
</span>
<span id="execution-error-GETapi-panel-chats--chat-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-chats--chat-"></code></pre>
</span>
<form id="form-GETapi-panel-chats--chat-" data-method="GET"
      data-path="api/panel/chats/{chat}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-chats--chat-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-chats--chat-"
                    onclick="tryItOut('GETapi-panel-chats--chat-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-chats--chat-"
                    onclick="cancelTryOut('GETapi-panel-chats--chat-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-chats--chat-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/chats/{chat}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>chat</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="chat"
               data-endpoint="GETapi-panel-chats--chat-"
               value="11"
               data-component="url" hidden>
    <br>
<p>The ID of chat.</p>
            </p>
                    </form>

            <h2 id="panel-chats-POSTapi-panel-chats--chat--message">Send message to chat</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-chats--chat--message">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/chats/48/message" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"message\": \"Test chat message! Listen me!\",
    \"iid\": \"EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/chats/48/message"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "message": "Test chat message! Listen me!",
    "iid": "EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-chats--chat--message">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 12,
        &quot;iid&quot;: &quot;consectetur&quot;,
        &quot;member_id&quot;: 132,
        &quot;message&quot;: &quot;nobis&quot;,
        &quot;code&quot;: &quot;iste&quot;,
        &quot;type&quot;: &quot;message&quot;,
        &quot;silent&quot;: false,
        &quot;created_at&quot;: &quot;2022-06-13T00:45:28.667676Z&quot;,
        &quot;updated_at&quot;: &quot;2022-06-13T00:45:28.667680Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-chats--chat--message" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-chats--chat--message"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-chats--chat--message"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-chats--chat--message" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-chats--chat--message"></code></pre>
</span>
<form id="form-POSTapi-panel-chats--chat--message" data-method="POST"
      data-path="api/panel/chats/{chat}/message"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-chats--chat--message', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-chats--chat--message"
                    onclick="tryItOut('POSTapi-panel-chats--chat--message');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-chats--chat--message"
                    onclick="cancelTryOut('POSTapi-panel-chats--chat--message');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-chats--chat--message" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/chats/{chat}/message</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>chat</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="chat"
               data-endpoint="POSTapi-panel-chats--chat--message"
               value="48"
               data-component="url" hidden>
    <br>
<p>The ID of chat, used for sending message.</p>
            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>message</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="message"
               data-endpoint="POSTapi-panel-chats--chat--message"
               value="Test chat message! Listen me!"
               data-component="body" hidden>
    <br>
<p>Message of chat message.</p>
        </p>
                <p>
            <b><code>iid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="iid"
               data-endpoint="POSTapi-panel-chats--chat--message"
               value="EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E"
               data-component="body" hidden>
    <br>
<p>IID of chat message.</p>
        </p>
        </form>

        <h1 id="panel-events">Panel Events</h1>

    

            <h2 id="panel-events-GETapi-panel-events">Shows events of current organization with pagination</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/events" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 30,
            &quot;title&quot;: &quot;I COULD NOT.&quot;,
            &quot;description&quot;: &quot;Nihil nulla sunt sapiente asperiores nihil. Ullam quia sit qui voluptatem animi ut est. Et est iste aliquid sit.&quot;,
            &quot;place&quot;: &quot;5633 Lois Trail Suite 458\nLake Ethel, LA 66446-3147&quot;,
            &quot;start_at&quot;: &quot;2022-06-14T14:15:25.733210Z&quot;,
            &quot;min_age&quot;: 26,
            &quot;is_offline&quot;: false,
            &quot;tags&quot;: [
                {
                    &quot;id&quot;: 21,
                    &quot;name&quot;: &quot;tag G&quot;,
                    &quot;group_name&quot;: &quot;group D&quot;
                }
            ],
            &quot;avatar&quot;: null,
            &quot;city&quot;: {
                &quot;id&quot;: 83,
                &quot;name&quot;: &quot;Prof. Vanessa Bogisich&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 44,
                &quot;name&quot;: &quot;Ms.&quot;,
                &quot;description&quot;: &quot;Molestiae ullam sit cumque et nisi rerum ea. Ipsam necessitatibus velit consequatur illo qui. Pariatur velit omnis ea qui est at rem.&quot;,
                &quot;contacts&quot;: &quot;3630 Suzanne Manor\nPort Amarifurt, WV 93907&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 46,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/46/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004525Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=13477fcda202119e5788f582f2af05ad78c826bcbcfb5153806531f85d76404d&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 0
        },
        {
            &quot;id&quot;: 31,
            &quot;title&quot;: &quot;How I wonder if I.&quot;,
            &quot;description&quot;: &quot;Voluptatem quia cumque dolor. Quo error illo architecto architecto molestiae. Perspiciatis neque et quod.&quot;,
            &quot;place&quot;: &quot;80430 Lockman Valleys\nQuigleyshire, IL 96007-2543&quot;,
            &quot;start_at&quot;: &quot;2022-06-19T20:14:25.831526Z&quot;,
            &quot;min_age&quot;: 0,
            &quot;is_offline&quot;: true,
            &quot;tags&quot;: [
                {
                    &quot;id&quot;: 22,
                    &quot;name&quot;: &quot;tag B&quot;,
                    &quot;group_name&quot;: &quot;group J&quot;
                }
            ],
            &quot;avatar&quot;: null,
            &quot;city&quot;: {
                &quot;id&quot;: 84,
                &quot;name&quot;: &quot;Emil Bednar III&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 45,
                &quot;name&quot;: &quot;Mr.&quot;,
                &quot;description&quot;: &quot;Quam dicta sunt commodi dolorem et illo nobis. Eos a reprehenderit nihil dolorum ea. Doloribus hic minus doloremque dolorum assumenda itaque fugiat. Quia illo quae architecto quas.&quot;,
                &quot;contacts&quot;: &quot;9476 Bartholome Isle Apt. 961\nOmaville, CA 73381&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 47,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/47/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004525Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=c72f065aa0d31aefc15b3322064ecbc086a479e867f5dd9252b548d5e6976d30&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-events"></code></pre>
</span>
<span id="execution-error-GETapi-panel-events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-events"></code></pre>
</span>
<form id="form-GETapi-panel-events" data-method="GET"
      data-path="api/panel/events"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-events"
                    onclick="tryItOut('GETapi-panel-events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-events"
                    onclick="cancelTryOut('GETapi-panel-events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/events</code></b>
        </p>
                    </form>

            <h2 id="panel-events-GETapi-panel-events--event-">Shows information of selected event</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-events--event-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/events/13" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/13"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-events--event-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 32,
        &quot;title&quot;: &quot;Seaography: then.&quot;,
        &quot;description&quot;: &quot;Et laudantium nostrum deleniti itaque atque eius asperiores. Voluptates iste facere ipsam.&quot;,
        &quot;place&quot;: &quot;7975 Rocky Port\nLake Elouise, SD 87179&quot;,
        &quot;start_at&quot;: &quot;2022-06-16T09:28:25.976267Z&quot;,
        &quot;min_age&quot;: 18,
        &quot;is_offline&quot;: true,
        &quot;tags&quot;: [
            {
                &quot;id&quot;: 23,
                &quot;name&quot;: &quot;tag J&quot;,
                &quot;group_name&quot;: &quot;group F&quot;
            }
        ],
        &quot;avatar&quot;: {
            &quot;id&quot;: 49,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/49/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=dac81db3a1dd3c0bd4e2af71e0109094234cc5703475885589e141791f8ea204&quot;
            }
        },
        &quot;city&quot;: {
            &quot;id&quot;: 85,
            &quot;name&quot;: &quot;Chadd Ullrich&quot;
        },
        &quot;organization&quot;: {
            &quot;id&quot;: 46,
            &quot;name&quot;: &quot;Dr.&quot;,
            &quot;description&quot;: &quot;Modi vel velit quidem dolores qui saepe. Ut et voluptatem officia dolorum.&quot;,
            &quot;contacts&quot;: &quot;47515 Towne Plains\nSchneidermouth, CO 12241-1294&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 48,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/48/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=a470484d7a728a3cc127675bd10fd4643f6a8ff9daf0401b3936d40bf3d798fe&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        &quot;roles&quot;: [
            {
                &quot;id&quot;: 2,
                &quot;name&quot;: &quot;Lucio Corwin Sr.&quot;,
                &quot;description&quot;: &quot;Eveniet et voluptas in assumenda non nam. Asperiores doloribus dignissimos est provident aut. Natus dignissimos rerum non qui omnis sapiente. Maiores quia repellendus voluptatum magni quas enim in.&quot;
            },
            {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Jade Green&quot;,
                &quot;description&quot;: &quot;Sint praesentium quasi omnis necessitatibus omnis distinctio. Autem rerum exercitationem vel et. Consequatur dolore at veritatis nesciunt. Assumenda ipsum adipisci ea quidem praesentium minima.&quot;
            }
        ],
        &quot;participants_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-events--event-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-events--event-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-events--event-"></code></pre>
</span>
<span id="execution-error-GETapi-panel-events--event-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-events--event-"></code></pre>
</span>
<form id="form-GETapi-panel-events--event-" data-method="GET"
      data-path="api/panel/events/{event}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-events--event-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-events--event-"
                    onclick="tryItOut('GETapi-panel-events--event-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-events--event-"
                    onclick="cancelTryOut('GETapi-panel-events--event-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-events--event-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/events/{event}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="GETapi-panel-events--event-"
               value="13"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="panel-events-GETapi-panel-events--event--participants">Show participants of selected event</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-events--event--participants">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/events/15/participants" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/15/participants"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-events--event--participants">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 49,
            &quot;name&quot;: &quot;Sienna Strosin&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 50,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/50/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=c53dd2f22d1cd7368bd3e2259d03a3fb1b9aa3e17de1aa67bb8f39b2e1733786&quot;
                }
            },
            &quot;description&quot;: &quot;Aut animi et exercitationem rerum at aliquam. Nisi temporibus quaerat accusantium tempora voluptatem. Molestiae ipsum id nihil sit fugit sed consectetur.&quot;,
            &quot;age&quot;: 18,
            &quot;score&quot;: 39297,
            &quot;level&quot;: 11,
            &quot;need_points_for_new_level&quot;: 42623,
            &quot;city&quot;: {
                &quot;id&quot;: 86,
                &quot;name&quot;: &quot;Abe Jast&quot;
            }
        },
        {
            &quot;id&quot;: 50,
            &quot;name&quot;: &quot;Dr. Micheal Cruickshank DDS&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 51,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/51/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=907ca66040230d22f7e3689b4ed40cc606ad9f44ac24f895108d5b1048743c19&quot;
                }
            },
            &quot;description&quot;: &quot;Ut cumque neque doloremque quam omnis eligendi eos. Exercitationem et omnis autem non. Laborum et est culpa qui qui. Incidunt ex expedita et aut aut soluta omnis.&quot;,
            &quot;age&quot;: 25,
            &quot;score&quot;: 91311,
            &quot;level&quot;: 13,
            &quot;need_points_for_new_level&quot;: 236369,
            &quot;city&quot;: {
                &quot;id&quot;: 87,
                &quot;name&quot;: &quot;Ted Nitzsche&quot;
            }
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-events--event--participants" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-events--event--participants"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-events--event--participants"></code></pre>
</span>
<span id="execution-error-GETapi-panel-events--event--participants" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-events--event--participants"></code></pre>
</span>
<form id="form-GETapi-panel-events--event--participants" data-method="GET"
      data-path="api/panel/events/{event}/participants"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-events--event--participants', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-events--event--participants"
                    onclick="tryItOut('GETapi-panel-events--event--participants');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-events--event--participants"
                    onclick="cancelTryOut('GETapi-panel-events--event--participants');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-events--event--participants" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/events/{event}/participants</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="GETapi-panel-events--event--participants"
               value="15"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="panel-events-POSTapi-panel-events">Store a newly created event.</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"title\": \"my best event.\",
    \"description\": \"best events for best peoples in best world.\",
    \"place\": \"MoscowCityHack\",
    \"start_at\": \"2022-12-03 15:42:05\",
    \"min_age\": 18,
    \"is_offline\": true,
    \"city_id\": 1,
    \"tags\": [
        1,
        2,
        3
    ],
    \"roles\": [
        {
            \"name\": \"gzkpgeydnemmuqokkdcfbbzvmhijpwtggwaivdpkwkrgefdrpzbdyemomasilmjabjauxfkhffzpmdccgkrkxxmbiswrxvqoustcfzrcwgpkumshjmcydpssazzclxbkepzzhiybsydxssqjwtkyqxvifamygyps\",
            \"description\": \"axyirpaemwezjrydkgcstxczrvactsvhnhpryibougjltgvxznhjslqyqjdkpaeptzwzowisusbkttzugrghlsxzjriduqqafccgsziwyzgiqqnukiwficnywuqodcftcflaxijchlfupsglpqaohrkpzfpjysewvzytmsqsrmrdorsccfawiugzywguryzzekjofodueikfoqbhxwuyesiynuzuheseqehdjzadoxaddgzwpmwsgtzrhighbjsyrdjuwyolbcbnekoznrayeyybejbljftiowabprrwxqfdvzmzrdvcryehwkysvhhcdbzexmimbtupkdpavpgkwiknntviyphtvvuilmomlmkzbbbzhzhgdlgfifoamokftvduqcgmjrsrxtmzzhidrgevquvxhyyommaiwidofzjdxfyhiezccpdrqli\"
        }
    ]
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "my best event.",
    "description": "best events for best peoples in best world.",
    "place": "MoscowCityHack",
    "start_at": "2022-12-03 15:42:05",
    "min_age": 18,
    "is_offline": true,
    "city_id": 1,
    "tags": [
        1,
        2,
        3
    ],
    "roles": [
        {
            "name": "gzkpgeydnemmuqokkdcfbbzvmhijpwtggwaivdpkwkrgefdrpzbdyemomasilmjabjauxfkhffzpmdccgkrkxxmbiswrxvqoustcfzrcwgpkumshjmcydpssazzclxbkepzzhiybsydxssqjwtkyqxvifamygyps",
            "description": "axyirpaemwezjrydkgcstxczrvactsvhnhpryibougjltgvxznhjslqyqjdkpaeptzwzowisusbkttzugrghlsxzjriduqqafccgsziwyzgiqqnukiwficnywuqodcftcflaxijchlfupsglpqaohrkpzfpjysewvzytmsqsrmrdorsccfawiugzywguryzzekjofodueikfoqbhxwuyesiynuzuheseqehdjzadoxaddgzwpmwsgtzrhighbjsyrdjuwyolbcbnekoznrayeyybejbljftiowabprrwxqfdvzmzrdvcryehwkysvhhcdbzexmimbtupkdpavpgkwiknntviyphtvvuilmomlmkzbbbzhzhgdlgfifoamokftvduqcgmjrsrxtmzzhidrgevquvxhyyommaiwidofzjdxfyhiezccpdrqli"
        }
    ]
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 33,
        &quot;title&quot;: &quot;Very soon the.&quot;,
        &quot;description&quot;: &quot;Est cum tempore omnis corrupti veritatis nihil numquam. Maxime inventore neque qui a est praesentium. Nemo dolorum placeat quisquam accusamus quia reprehenderit vel temporibus.&quot;,
        &quot;place&quot;: &quot;529 Kautzer Ridge\nFridaside, CO 72290&quot;,
        &quot;start_at&quot;: &quot;2022-06-19T22:28:26.198921Z&quot;,
        &quot;min_age&quot;: 12,
        &quot;is_offline&quot;: false,
        &quot;tags&quot;: [
            {
                &quot;id&quot;: 24,
                &quot;name&quot;: &quot;tag G&quot;,
                &quot;group_name&quot;: &quot;group D&quot;
            }
        ],
        &quot;avatar&quot;: {
            &quot;id&quot;: 53,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/53/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=916c989e482179f58e81ce96fc199006b4073e09ee550dd5a3567dcb5c4781fe&quot;
            }
        },
        &quot;city&quot;: {
            &quot;id&quot;: 88,
            &quot;name&quot;: &quot;Caleigh Dare MD&quot;
        },
        &quot;organization&quot;: {
            &quot;id&quot;: 47,
            &quot;name&quot;: &quot;Prof.&quot;,
            &quot;description&quot;: &quot;Ut et consequuntur est magni et. Magnam voluptatem eius beatae voluptatem inventore. Eaque eos harum eius qui nulla. Ut et quisquam non quia laudantium assumenda.&quot;,
            &quot;contacts&quot;: &quot;7714 Eve Plains\nNew Karli, SC 33274-1737&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 52,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/52/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=5e867ee8c5a9c3a81d6d45536d2e9a714cc8d3faa954820b8ddfe25b425a01ad&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        &quot;roles&quot;: [
            {
                &quot;id&quot;: 4,
                &quot;name&quot;: &quot;Ivy Kuphal&quot;,
                &quot;description&quot;: &quot;In earum odit numquam. Debitis vel deserunt velit quo iure. Velit alias quas minus modi aut dolorem. Doloremque odit repudiandae iste soluta et repellat. Aut est error et enim.&quot;
            }
        ],
        &quot;participants_count&quot;: 0
    },
    &quot;title&quot;: &quot;Event created&quot;,
    &quot;message&quot;: &quot;Event successful created&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-events"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-events"></code></pre>
</span>
<form id="form-POSTapi-panel-events" data-method="POST"
      data-path="api/panel/events"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-events"
                    onclick="tryItOut('POSTapi-panel-events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-events"
                    onclick="cancelTryOut('POSTapi-panel-events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/events</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>title</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="title"
               data-endpoint="POSTapi-panel-events"
               value="my best event."
               data-component="body" hidden>
    <br>
<p>Event title.</p>
        </p>
                <p>
            <b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="description"
               data-endpoint="POSTapi-panel-events"
               value="best events for best peoples in best world."
               data-component="body" hidden>
    <br>
<p>Event description.</p>
        </p>
                <p>
            <b><code>place</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="place"
               data-endpoint="POSTapi-panel-events"
               value="MoscowCityHack"
               data-component="body" hidden>
    <br>
<p>Events place.</p>
        </p>
                <p>
            <b><code>start_at</code></b>&nbsp;&nbsp;<small>date</small>  &nbsp;
                <input type="text"
               name="start_at"
               data-endpoint="POSTapi-panel-events"
               value="2022-12-03 15:42:05"
               data-component="body" hidden>
    <br>
<p>DateTime, when events starts.</p>
        </p>
                <p>
            <b><code>min_age</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="min_age"
               data-endpoint="POSTapi-panel-events"
               value="18"
               data-component="body" hidden>
    <br>
<p>Min age of participants of events.</p>
        </p>
                <p>
            <b><code>is_offline</code></b>&nbsp;&nbsp;<small>boolean</small>     <i>optional</i> &nbsp;
                <label data-endpoint="POSTapi-panel-events" hidden>
            <input type="radio" name="is_offline"
                   value="true"
                   data-endpoint="POSTapi-panel-events"
                   data-component="body"
            >
            <code>true</code>
        </label>
        <label data-endpoint="POSTapi-panel-events" hidden>
            <input type="radio" name="is_offline"
                   value="false"
                   data-endpoint="POSTapi-panel-events"
                   data-component="body"
            >
            <code>false</code>
        </label>
    <br>
<p>Is events offline or online.</p>
        </p>
                <p>
            <b><code>city_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="city_id"
               data-endpoint="POSTapi-panel-events"
               value="1"
               data-component="body" hidden>
    <br>
<p>City of event.</p>
        </p>
                <p>
            <b><code>tags</code></b>&nbsp;&nbsp;<small>integer[]</small>     <i>optional</i> &nbsp;
                <input type="number"
               name="tags[0]"
               data-endpoint="POSTapi-panel-events"
               data-component="body" hidden>
        <input type="number"
               name="tags[1]"
               data-endpoint="POSTapi-panel-events"
               data-component="body" hidden>
    <br>
<p>Id of tags, used for this event.</p>
        </p>
                <p>
        <details>
            <summary style="padding-bottom: 10px;">
                <b><code>roles</code></b>&nbsp;&nbsp;<small>object[]</small>     <i>optional</i> &nbsp;
<br>

            </summary>
                                                <p>
                        <b><code>roles[].name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="roles.0.name"
               data-endpoint="POSTapi-panel-events"
               value="gzkpgeydnemmuqokkdcfbbzvmhijpwtggwaivdpkwkrgefdrpzbdyemomasilmjabjauxfkhffzpmdccgkrkxxmbiswrxvqoustcfzrcwgpkumshjmcydpssazzclxbkepzzhiybsydxssqjwtkyqxvifamygyps"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 255 characters.</p>
                    </p>
                                                                <p>
                        <b><code>roles[].description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="roles.0.description"
               data-endpoint="POSTapi-panel-events"
               value="axyirpaemwezjrydkgcstxczrvactsvhnhpryibougjltgvxznhjslqyqjdkpaeptzwzowisusbkttzugrghlsxzjriduqqafccgsziwyzgiqqnukiwficnywuqodcftcflaxijchlfupsglpqaohrkpzfpjysewvzytmsqsrmrdorsccfawiugzywguryzzekjofodueikfoqbhxwuyesiynuzuheseqehdjzadoxaddgzwpmwsgtzrhighbjsyrdjuwyolbcbnekoznrayeyybejbljftiowabprrwxqfdvzmzrdvcryehwkysvhhcdbzexmimbtupkdpavpgkwiknntviyphtvvuilmomlmkzbbbzhzhgdlgfifoamokftvduqcgmjrsrxtmzzhidrgevquvxhyyommaiwidofzjdxfyhiezccpdrqli"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 4096 characters.</p>
                    </p>
                                                                <p>
        <details>
            <summary style="padding-bottom: 10px;">
                <b><code>roles.*</code></b>&nbsp;&nbsp;<small>object</small>  &nbsp;
<br>

            </summary>
                                                <p>
                        <b><code>roles.*.name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="roles.*.name"
               data-endpoint="POSTapi-panel-events"
               value="Best role"
               data-component="body" hidden>
    <br>
<p>Role name.</p>
                    </p>
                                                                <p>
                        <b><code>roles.*.description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="roles.*.description"
               data-endpoint="POSTapi-panel-events"
               value="best best role for best best organization"
               data-component="body" hidden>
    <br>
<p>Role description.</p>
                    </p>
                                    </details>
        </p>
                                        </details>
        </p>
        </form>

            <h2 id="panel-events-POSTapi-panel-events--event--media">Store (or replace it) media for recently created event</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-events--event--media">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/14/media" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"file\": \"voluptas\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/14/media"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "file": "voluptas"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-events--event--media">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Unauthenticated.&quot;,
    &quot;message&quot;: &quot;You must be authenticated, to perform this request.&quot;,
    &quot;error&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-events--event--media" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-events--event--media"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-events--event--media"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-events--event--media" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-events--event--media"></code></pre>
</span>
<form id="form-POSTapi-panel-events--event--media" data-method="POST"
      data-path="api/panel/events/{event}/media"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-events--event--media', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-events--event--media"
                    onclick="tryItOut('POSTapi-panel-events--event--media');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-events--event--media"
                    onclick="cancelTryOut('POSTapi-panel-events--event--media');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-events--event--media" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/events/{event}/media</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="POSTapi-panel-events--event--media"
               value="14"
               data-component="url" hidden>
    <br>

            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>file</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="file"
               data-endpoint="POSTapi-panel-events--event--media"
               value="voluptas"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="panel-events-POSTapi-panel-events--event--activate">Activate (Publish) stored event</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-events--event--activate">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/12/activate" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/12/activate"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-events--event--activate">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 34,
        &quot;title&quot;: &quot;March Hare said in.&quot;,
        &quot;description&quot;: &quot;Facilis est quas magnam consectetur. Adipisci impedit et libero doloremque modi rerum ut libero. Iste eos eaque et. Recusandae itaque sapiente ratione fugiat iusto reprehenderit molestiae animi.&quot;,
        &quot;place&quot;: &quot;296 Neha Inlet\nXavierburgh, CT 50445-6106&quot;,
        &quot;start_at&quot;: &quot;2022-06-15T21:40:26.833307Z&quot;,
        &quot;min_age&quot;: 14,
        &quot;is_offline&quot;: true,
        &quot;tags&quot;: [
            {
                &quot;id&quot;: 25,
                &quot;name&quot;: &quot;tag B&quot;,
                &quot;group_name&quot;: &quot;group A&quot;
            }
        ],
        &quot;avatar&quot;: {
            &quot;id&quot;: 55,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/55/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=eb8289e6a39b73b59c1d7391c25b587eeb06a9ed9ad266861484d9ab5a678fac&quot;
            }
        },
        &quot;city&quot;: {
            &quot;id&quot;: 89,
            &quot;name&quot;: &quot;Hugh Jenkins&quot;
        },
        &quot;organization&quot;: {
            &quot;id&quot;: 48,
            &quot;name&quot;: &quot;Mrs.&quot;,
            &quot;description&quot;: &quot;Id non itaque et maxime. Cupiditate voluptatem voluptas nisi nesciunt. Veritatis eos et deleniti veniam eligendi omnis repellendus quibusdam.&quot;,
            &quot;contacts&quot;: &quot;130 Schneider Manors\nNew Franco, AK 33025&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 54,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/54/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004526Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=eaaf96c46f15d3e5689ca708dbcf5c5e60f9c185e6563794bc292d120988b92a&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        &quot;participants_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-events--event--activate" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-events--event--activate"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-events--event--activate"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-events--event--activate" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-events--event--activate"></code></pre>
</span>
<form id="form-POSTapi-panel-events--event--activate" data-method="POST"
      data-path="api/panel/events/{event}/activate"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-events--event--activate', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-events--event--activate"
                    onclick="tryItOut('POSTapi-panel-events--event--activate');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-events--event--activate"
                    onclick="cancelTryOut('POSTapi-panel-events--event--activate');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-events--event--activate" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/events/{event}/activate</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="POSTapi-panel-events--event--activate"
               value="12"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="panel-events-POSTapi-panel-events--event--deactivate">Deactivate (unpublish) stored event</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-events--event--deactivate">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/16/deactivate" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/16/deactivate"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-events--event--deactivate">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 35,
        &quot;title&quot;: &quot;Improve his.&quot;,
        &quot;description&quot;: &quot;Nesciunt aut aut qui sint. Ut praesentium voluptas commodi velit accusamus. Quia amet doloremque aut voluptates aut cupiditate porro.&quot;,
        &quot;place&quot;: &quot;6093 Josue Ports Apt. 682\nSouth Reidfort, LA 20718-8786&quot;,
        &quot;start_at&quot;: &quot;2022-06-19T23:00:27.006488Z&quot;,
        &quot;min_age&quot;: 2,
        &quot;is_offline&quot;: true,
        &quot;tags&quot;: [
            {
                &quot;id&quot;: 26,
                &quot;name&quot;: &quot;tag D&quot;,
                &quot;group_name&quot;: &quot;group I&quot;
            }
        ],
        &quot;avatar&quot;: {
            &quot;id&quot;: 57,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/57/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=3ae26de64ec42d173a3b2f2a56229aa51f7946895ed691f62cfd98cfac15b5b3&quot;
            }
        },
        &quot;city&quot;: {
            &quot;id&quot;: 90,
            &quot;name&quot;: &quot;Jeff Lynch&quot;
        },
        &quot;organization&quot;: {
            &quot;id&quot;: 49,
            &quot;name&quot;: &quot;Mrs.&quot;,
            &quot;description&quot;: &quot;Minima sit nesciunt ut veritatis et facilis. Nemo beatae ipsa quae odit temporibus autem voluptatum. Harum dolorem consectetur nobis totam ut.&quot;,
            &quot;contacts&quot;: &quot;2163 Ransom Roads Suite 055\nStammside, NM 08383-9722&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 56,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/56/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=25ab8c595b4d71cdc5d8711fa1193e06b7b8220f26fb5fce25f7d0ee2b29ca17&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        &quot;participants_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-events--event--deactivate" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-events--event--deactivate"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-events--event--deactivate"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-events--event--deactivate" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-events--event--deactivate"></code></pre>
</span>
<form id="form-POSTapi-panel-events--event--deactivate" data-method="POST"
      data-path="api/panel/events/{event}/deactivate"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-events--event--deactivate', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-events--event--deactivate"
                    onclick="tryItOut('POSTapi-panel-events--event--deactivate');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-events--event--deactivate"
                    onclick="cancelTryOut('POSTapi-panel-events--event--deactivate');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-events--event--deactivate" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/events/{event}/deactivate</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="POSTapi-panel-events--event--deactivate"
               value="16"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="panel-events-PATCHapi-panel-events--event-">Update event.</h2>

<p>
</p>



<span id="example-requests-PATCHapi-panel-events--event-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request PATCH \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/8" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"title\": \"my best event.\",
    \"description\": \"best events for best peoples in best world.\",
    \"place\": \"MoscowCityHack\",
    \"start_at\": \"2022-12-03 15:42:05\",
    \"min_age\": 18,
    \"is_offline\": true,
    \"city_id\": 1,
    \"tags\": [
        1,
        2,
        3
    ],
    \"roles\": [
        {
            \"name\": \"fgrgwypyrtjdsi\",
            \"description\": \"dpbqrsskwerriqxoslouqiqvwqjobdciwxnotzckepqixaenlzziyfjprhamxwdhjmfhbohrqbnekvaqdqggnhqwhzvbeoorvhpwxxblfakizoshlmchmkxtsccjbxfuotknuvbkgzqtvwejhazfzlqfgwaumubztqxgulmahktxsjskftjhlhktwqjynatqnqtuemugbbygcywknxafgytbkwsywzbcjsvwgfxeakcdbkdivqpuacoglzsvyeolmdvvbjfxtzrumjgrqpannixxuorfapfrrcwncofwgxmzkhudodtuojkxisesnfsnwygfoowtmayufehqsfwlexjlqpzadpmiksdnsizkxwfyuzlendpnmkjzdbzjidapljlpdxnsvefjcxxnwlvkxshtlfczdtdzgiuirnxxinaziribpaannsesrbannmyqiumolwrayaoyxdgxdbmlckzpiswnezstpwogkgorjgrxjoiftqissqkeosnithrisskejqrxvvpjmklhxuprnbfuxrdztwyatarjsbotmxgcqzvjvvtlgweeqgdmabczjgytjtlvxnkhodjgyzypeutbttulpncwijwywxufqwmemsplorgzxxetdfrteukugclyzqpfsyhjlhwyhbiskvwtzqrbhdwmqgzhyrvilgibjwrzaokypixvirvljciocjljcoolmuaiqrdmgcvpsjmwsvesjzmgdrjnpkvdsxaosettjfbzajoggyqiqxmcemhgmpkmlpgbdfqygogvivdftxhmnmdcmhtjrmzwmzynthaobdhteugibvqbdcmzopypzioblkwmepyzynfxehesffskmjpuboacfdpotybfvsnpaahgbgwxzzatnosxfeheopdabxcakaplxsjwciijbarlnyxesokdizyejoxmkokatsyjbflfdbgjljmzghzytvwtkygurdysryeolfwpzageckgzoxniaokcgurqtwmcwxmuuwfrbtkelsqvdrifkanenpxumnbdasoekpsgmlpuiauowrayiwxmwnrpsweqyvbnpceobqnvatcgcsaqcgddosphrmhuzwpqzedcjcqentfneehyahjjbosddgojxcbullewwipcocetwkoyzlukppmafntisrnjuocddudisdzvkwszgxyfrehhsrsehlwttknvmvvaqklfhdzebjxsuyrocdtnnsjyfexsrqammeeoommeduuikakwhltxtyvfbhwnrpdgexxmygfrzglsrivephalweepknkymblh\"
        }
    ]
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/8"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "my best event.",
    "description": "best events for best peoples in best world.",
    "place": "MoscowCityHack",
    "start_at": "2022-12-03 15:42:05",
    "min_age": 18,
    "is_offline": true,
    "city_id": 1,
    "tags": [
        1,
        2,
        3
    ],
    "roles": [
        {
            "name": "fgrgwypyrtjdsi",
            "description": "dpbqrsskwerriqxoslouqiqvwqjobdciwxnotzckepqixaenlzziyfjprhamxwdhjmfhbohrqbnekvaqdqggnhqwhzvbeoorvhpwxxblfakizoshlmchmkxtsccjbxfuotknuvbkgzqtvwejhazfzlqfgwaumubztqxgulmahktxsjskftjhlhktwqjynatqnqtuemugbbygcywknxafgytbkwsywzbcjsvwgfxeakcdbkdivqpuacoglzsvyeolmdvvbjfxtzrumjgrqpannixxuorfapfrrcwncofwgxmzkhudodtuojkxisesnfsnwygfoowtmayufehqsfwlexjlqpzadpmiksdnsizkxwfyuzlendpnmkjzdbzjidapljlpdxnsvefjcxxnwlvkxshtlfczdtdzgiuirnxxinaziribpaannsesrbannmyqiumolwrayaoyxdgxdbmlckzpiswnezstpwogkgorjgrxjoiftqissqkeosnithrisskejqrxvvpjmklhxuprnbfuxrdztwyatarjsbotmxgcqzvjvvtlgweeqgdmabczjgytjtlvxnkhodjgyzypeutbttulpncwijwywxufqwmemsplorgzxxetdfrteukugclyzqpfsyhjlhwyhbiskvwtzqrbhdwmqgzhyrvilgibjwrzaokypixvirvljciocjljcoolmuaiqrdmgcvpsjmwsvesjzmgdrjnpkvdsxaosettjfbzajoggyqiqxmcemhgmpkmlpgbdfqygogvivdftxhmnmdcmhtjrmzwmzynthaobdhteugibvqbdcmzopypzioblkwmepyzynfxehesffskmjpuboacfdpotybfvsnpaahgbgwxzzatnosxfeheopdabxcakaplxsjwciijbarlnyxesokdizyejoxmkokatsyjbflfdbgjljmzghzytvwtkygurdysryeolfwpzageckgzoxniaokcgurqtwmcwxmuuwfrbtkelsqvdrifkanenpxumnbdasoekpsgmlpuiauowrayiwxmwnrpsweqyvbnpceobqnvatcgcsaqcgddosphrmhuzwpqzedcjcqentfneehyahjjbosddgojxcbullewwipcocetwkoyzlukppmafntisrnjuocddudisdzvkwszgxyfrehhsrsehlwttknvmvvaqklfhdzebjxsuyrocdtnnsjyfexsrqammeeoommeduuikakwhltxtyvfbhwnrpdgexxmygfrzglsrivephalweepknkymblh"
        }
    ]
};

fetch(url, {
    method: "PATCH",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-PATCHapi-panel-events--event-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 36,
        &quot;title&quot;: &quot;Mouse with an air.&quot;,
        &quot;description&quot;: &quot;Est omnis illo et dolores dolorum assumenda. Enim modi incidunt eos qui totam nesciunt quas et. Nam velit minima laudantium quam qui id qui. Reprehenderit inventore officiis inventore quod.&quot;,
        &quot;place&quot;: &quot;280 Kiley Burg\nNew Giamouth, CA 93193&quot;,
        &quot;start_at&quot;: &quot;2022-06-17T10:21:27.139040Z&quot;,
        &quot;min_age&quot;: 2,
        &quot;is_offline&quot;: false,
        &quot;tags&quot;: [
            {
                &quot;id&quot;: 27,
                &quot;name&quot;: &quot;tag B&quot;,
                &quot;group_name&quot;: &quot;group A&quot;
            }
        ],
        &quot;avatar&quot;: {
            &quot;id&quot;: 59,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/59/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=6195444cd26078d840e7cad41f9ab88fcb9ecfbd285d931dbdc19f33ec800a0f&quot;
            }
        },
        &quot;city&quot;: {
            &quot;id&quot;: 91,
            &quot;name&quot;: &quot;Lottie Raynor DVM&quot;
        },
        &quot;organization&quot;: {
            &quot;id&quot;: 50,
            &quot;name&quot;: &quot;Prof.&quot;,
            &quot;description&quot;: &quot;Molestiae aut eos corporis. Eaque sint necessitatibus quod. Qui illum voluptatem perspiciatis praesentium. Consequatur aut pariatur molestiae inventore eaque voluptate. Quasi hic nihil qui sit qui.&quot;,
            &quot;contacts&quot;: &quot;5028 Little Overpass Suite 506\nJadamouth, PA 75892&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 58,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/58/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=689405aab70e1b5f3f5776e2e105cfd3672fb7df636aa51204d5674f416cc8b3&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        &quot;roles&quot;: [
            {
                &quot;id&quot;: 5,
                &quot;name&quot;: &quot;Vincenzo Boyer&quot;,
                &quot;description&quot;: &quot;Et velit eaque similique repellat velit vero veritatis. Modi quia eveniet incidunt cupiditate sint ratione. Voluptatum mollitia quo necessitatibus maxime. Ut fuga quidem autem omnis.&quot;
            }
        ],
        &quot;participants_count&quot;: 0
    },
    &quot;title&quot;: &quot;Event created&quot;,
    &quot;message&quot;: &quot;Event successful created&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-PATCHapi-panel-events--event-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-PATCHapi-panel-events--event-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-PATCHapi-panel-events--event-"></code></pre>
</span>
<span id="execution-error-PATCHapi-panel-events--event-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PATCHapi-panel-events--event-"></code></pre>
</span>
<form id="form-PATCHapi-panel-events--event-" data-method="PATCH"
      data-path="api/panel/events/{event}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('PATCHapi-panel-events--event-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-PATCHapi-panel-events--event-"
                    onclick="tryItOut('PATCHapi-panel-events--event-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-PATCHapi-panel-events--event-"
                    onclick="cancelTryOut('PATCHapi-panel-events--event-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-PATCHapi-panel-events--event-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-purple">PATCH</small>
            <b><code>api/panel/events/{event}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="PATCHapi-panel-events--event-"
               value="8"
               data-component="url" hidden>
    <br>

            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>title</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="title"
               data-endpoint="PATCHapi-panel-events--event-"
               value="my best event."
               data-component="body" hidden>
    <br>
<p>Event title.</p>
        </p>
                <p>
            <b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="description"
               data-endpoint="PATCHapi-panel-events--event-"
               value="best events for best peoples in best world."
               data-component="body" hidden>
    <br>
<p>Event description.</p>
        </p>
                <p>
            <b><code>place</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="place"
               data-endpoint="PATCHapi-panel-events--event-"
               value="MoscowCityHack"
               data-component="body" hidden>
    <br>
<p>Events place.</p>
        </p>
                <p>
            <b><code>start_at</code></b>&nbsp;&nbsp;<small>date</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="start_at"
               data-endpoint="PATCHapi-panel-events--event-"
               value="2022-12-03 15:42:05"
               data-component="body" hidden>
    <br>
<p>DateTime, when events starts.</p>
        </p>
                <p>
            <b><code>min_age</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
                <input type="number"
               name="min_age"
               data-endpoint="PATCHapi-panel-events--event-"
               value="18"
               data-component="body" hidden>
    <br>
<p>Min age of participants of events.</p>
        </p>
                <p>
            <b><code>is_offline</code></b>&nbsp;&nbsp;<small>boolean</small>     <i>optional</i> &nbsp;
                <label data-endpoint="PATCHapi-panel-events--event-" hidden>
            <input type="radio" name="is_offline"
                   value="true"
                   data-endpoint="PATCHapi-panel-events--event-"
                   data-component="body"
            >
            <code>true</code>
        </label>
        <label data-endpoint="PATCHapi-panel-events--event-" hidden>
            <input type="radio" name="is_offline"
                   value="false"
                   data-endpoint="PATCHapi-panel-events--event-"
                   data-component="body"
            >
            <code>false</code>
        </label>
    <br>
<p>Is events offline or online.</p>
        </p>
                <p>
            <b><code>city_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
                <input type="number"
               name="city_id"
               data-endpoint="PATCHapi-panel-events--event-"
               value="1"
               data-component="body" hidden>
    <br>
<p>City of event.</p>
        </p>
                <p>
            <b><code>tags</code></b>&nbsp;&nbsp;<small>integer[]</small>     <i>optional</i> &nbsp;
                <input type="number"
               name="tags[0]"
               data-endpoint="PATCHapi-panel-events--event-"
               data-component="body" hidden>
        <input type="number"
               name="tags[1]"
               data-endpoint="PATCHapi-panel-events--event-"
               data-component="body" hidden>
    <br>
<p>Id of tags, used for this event.</p>
        </p>
                <p>
        <details>
            <summary style="padding-bottom: 10px;">
                <b><code>roles</code></b>&nbsp;&nbsp;<small>object[]</small>     <i>optional</i> &nbsp;
<br>

            </summary>
                                                <p>
                        <b><code>roles[].name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="roles.0.name"
               data-endpoint="PATCHapi-panel-events--event-"
               value="fgrgwypyrtjdsi"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 255 characters.</p>
                    </p>
                                                                <p>
                        <b><code>roles[].description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="roles.0.description"
               data-endpoint="PATCHapi-panel-events--event-"
               value="dpbqrsskwerriqxoslouqiqvwqjobdciwxnotzckepqixaenlzziyfjprhamxwdhjmfhbohrqbnekvaqdqggnhqwhzvbeoorvhpwxxblfakizoshlmchmkxtsccjbxfuotknuvbkgzqtvwejhazfzlqfgwaumubztqxgulmahktxsjskftjhlhktwqjynatqnqtuemugbbygcywknxafgytbkwsywzbcjsvwgfxeakcdbkdivqpuacoglzsvyeolmdvvbjfxtzrumjgrqpannixxuorfapfrrcwncofwgxmzkhudodtuojkxisesnfsnwygfoowtmayufehqsfwlexjlqpzadpmiksdnsizkxwfyuzlendpnmkjzdbzjidapljlpdxnsvefjcxxnwlvkxshtlfczdtdzgiuirnxxinaziribpaannsesrbannmyqiumolwrayaoyxdgxdbmlckzpiswnezstpwogkgorjgrxjoiftqissqkeosnithrisskejqrxvvpjmklhxuprnbfuxrdztwyatarjsbotmxgcqzvjvvtlgweeqgdmabczjgytjtlvxnkhodjgyzypeutbttulpncwijwywxufqwmemsplorgzxxetdfrteukugclyzqpfsyhjlhwyhbiskvwtzqrbhdwmqgzhyrvilgibjwrzaokypixvirvljciocjljcoolmuaiqrdmgcvpsjmwsvesjzmgdrjnpkvdsxaosettjfbzajoggyqiqxmcemhgmpkmlpgbdfqygogvivdftxhmnmdcmhtjrmzwmzynthaobdhteugibvqbdcmzopypzioblkwmepyzynfxehesffskmjpuboacfdpotybfvsnpaahgbgwxzzatnosxfeheopdabxcakaplxsjwciijbarlnyxesokdizyejoxmkokatsyjbflfdbgjljmzghzytvwtkygurdysryeolfwpzageckgzoxniaokcgurqtwmcwxmuuwfrbtkelsqvdrifkanenpxumnbdasoekpsgmlpuiauowrayiwxmwnrpsweqyvbnpceobqnvatcgcsaqcgddosphrmhuzwpqzedcjcqentfneehyahjjbosddgojxcbullewwipcocetwkoyzlukppmafntisrnjuocddudisdzvkwszgxyfrehhsrsehlwttknvmvvaqklfhdzebjxsuyrocdtnnsjyfexsrqammeeoommeduuikakwhltxtyvfbhwnrpdgexxmygfrzglsrivephalweepknkymblh"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 4096 characters.</p>
                    </p>
                                    </details>
        </p>
        </form>

            <h2 id="panel-events-DELETEapi-panel-events--event-">Delete event.</h2>

<p>
</p>



<span id="example-requests-DELETEapi-panel-events--event-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/6" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/6"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-panel-events--event-">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Unauthenticated.&quot;,
    &quot;message&quot;: &quot;You must be authenticated, to perform this request.&quot;,
    &quot;error&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-DELETEapi-panel-events--event-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-panel-events--event-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-panel-events--event-"></code></pre>
</span>
<span id="execution-error-DELETEapi-panel-events--event-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-panel-events--event-"></code></pre>
</span>
<form id="form-DELETEapi-panel-events--event-" data-method="DELETE"
      data-path="api/panel/events/{event}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-panel-events--event-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-panel-events--event-"
                    onclick="tryItOut('DELETEapi-panel-events--event-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-panel-events--event-"
                    onclick="cancelTryOut('DELETEapi-panel-events--event-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-panel-events--event-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/panel/events/{event}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="DELETEapi-panel-events--event-"
               value="6"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="panel-events-POSTapi-panel-events--event--approve">Approve, that user had been on event</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-events--event--approve">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/2/approve" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"code\": \"non\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/events/2/approve"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "code": "non"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-events--event--approve">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;succesfully approved user for event&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-events--event--approve" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-events--event--approve"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-events--event--approve"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-events--event--approve" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-events--event--approve"></code></pre>
</span>
<form id="form-POSTapi-panel-events--event--approve" data-method="POST"
      data-path="api/panel/events/{event}/approve"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-events--event--approve', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-events--event--approve"
                    onclick="tryItOut('POSTapi-panel-events--event--approve');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-events--event--approve"
                    onclick="cancelTryOut('POSTapi-panel-events--event--approve');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-events--event--approve" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/events/{event}/approve</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="POSTapi-panel-events--event--approve"
               value="2"
               data-component="url" hidden>
    <br>

            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>code</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="code"
               data-endpoint="POSTapi-panel-events--event--approve"
               value="non"
               data-component="body" hidden>
    <br>
<p>Users code, scanned from QR for approving, that user been on event. Example:</p>
        </p>
        </form>

        <h1 id="panel-notifications">Panel Notifications</h1>

    

            <h2 id="panel-notifications-POSTapi-panel-notifications">Send notification for users</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-notifications">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/notifications" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"message\": \"Test notification message! Listen me!\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/notifications"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "message": "Test notification message! Listen me!"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-notifications">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;sending notification&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-notifications" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-notifications"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-notifications"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-notifications" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-notifications"></code></pre>
</span>
<form id="form-POSTapi-panel-notifications" data-method="POST"
      data-path="api/panel/notifications"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-notifications', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-notifications"
                    onclick="tryItOut('POSTapi-panel-notifications');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-notifications"
                    onclick="cancelTryOut('POSTapi-panel-notifications');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-notifications" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/notifications</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>message</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="message"
               data-endpoint="POSTapi-panel-notifications"
               value="Test notification message! Listen me!"
               data-component="body" hidden>
    <br>
<p>Message of notification.</p>
        </p>
        </form>

        <h1 id="panel-organization">Panel Organization</h1>

    

            <h2 id="panel-organization-GETapi-panel-me">Display information about current organization.</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-me">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/me" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/me"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-me">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 41,
        &quot;name&quot;: &quot;Prof.&quot;,
        &quot;description&quot;: &quot;Amet consequuntur voluptas libero dolor veniam earum. Sunt quia quia sint in deleniti nulla modi. Est vitae et nisi facilis. Aperiam quasi eaque et et et.&quot;,
        &quot;contacts&quot;: &quot;264 Myles Hills\nFeeneyside, NV 03300&quot;,
        &quot;avatar&quot;: {
            &quot;id&quot;: 45,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/45/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004525Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=adc82338376d55670b6ae10d98c363719fc10a1cf9c4db1c9371d02e0e507218&quot;
            }
        },
        &quot;users_count&quot;: 0,
        &quot;events_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-me" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-me"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-me"></code></pre>
</span>
<span id="execution-error-GETapi-panel-me" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-me"></code></pre>
</span>
<form id="form-GETapi-panel-me" data-method="GET"
      data-path="api/panel/me"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-me', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-me"
                    onclick="tryItOut('GETapi-panel-me');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-me"
                    onclick="cancelTryOut('GETapi-panel-me');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-me" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/me</code></b>
        </p>
                    </form>

            <h2 id="panel-organization-PATCHapi-panel-me">Update information about current organization.</h2>

<p>
</p>



<span id="example-requests-PATCHapi-panel-me">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request PATCH \
    "https://mch-backend-staging.server.bonch.dev/api/panel/me" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"name\": \"Pallam\",
    \"description\": \"Beautiful text about myself\",
    \"contacts\": \"Phone: +79110297197; Email: kargin.go@bonch.dev\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/me"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Pallam",
    "description": "Beautiful text about myself",
    "contacts": "Phone: +79110297197; Email: kargin.go@bonch.dev"
};

fetch(url, {
    method: "PATCH",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-PATCHapi-panel-me">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 42,
        &quot;name&quot;: &quot;Dr.&quot;,
        &quot;description&quot;: &quot;Vel soluta facilis pariatur ipsum velit quasi voluptatem. Perferendis officia qui sed sit. Perferendis nemo modi consequuntur laborum et. Laborum nam repellat inventore magnam.&quot;,
        &quot;contacts&quot;: &quot;62222 Bruen Drive\nFredyfort, NH 16534-0209&quot;,
        &quot;users_count&quot;: 0,
        &quot;events_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-PATCHapi-panel-me" hidden>
    <blockquote>Received response<span
                id="execution-response-status-PATCHapi-panel-me"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-PATCHapi-panel-me"></code></pre>
</span>
<span id="execution-error-PATCHapi-panel-me" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PATCHapi-panel-me"></code></pre>
</span>
<form id="form-PATCHapi-panel-me" data-method="PATCH"
      data-path="api/panel/me"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('PATCHapi-panel-me', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-PATCHapi-panel-me"
                    onclick="tryItOut('PATCHapi-panel-me');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-PATCHapi-panel-me"
                    onclick="cancelTryOut('PATCHapi-panel-me');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-PATCHapi-panel-me" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-purple">PATCH</small>
            <b><code>api/panel/me</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="name"
               data-endpoint="PATCHapi-panel-me"
               value="Pallam"
               data-component="body" hidden>
    <br>
<p>Username.</p>
        </p>
                <p>
            <b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="description"
               data-endpoint="PATCHapi-panel-me"
               value="Beautiful text about myself"
               data-component="body" hidden>
    <br>
<p>User story (organization self-description).</p>
        </p>
                <p>
            <b><code>contacts</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="contacts"
               data-endpoint="PATCHapi-panel-me"
               value="Phone: +79110297197; Email: kargin.go@bonch.dev"
               data-component="body" hidden>
    <br>
<p>Contact information.</p>
        </p>
        </form>

            <h2 id="panel-organization-POSTapi-panel-me-media">Update media for current organization.</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-me-media">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/me/media" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"file\": \"porro\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/me/media"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "file": "porro"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-me-media">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 43,
        &quot;name&quot;: &quot;Mr.&quot;,
        &quot;description&quot;: &quot;Labore ea ut amet. Et non id maiores inventore tempora illum. Velit sequi dolores possimus quia. Et repellendus aut quasi quo assumenda. Aspernatur non et nesciunt consequatur repudiandae impedit.&quot;,
        &quot;contacts&quot;: &quot;942 Adolph Cliffs Apt. 385\nBernierfort, AR 73768&quot;,
        &quot;users_count&quot;: 0,
        &quot;events_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-me-media" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-me-media"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-me-media"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-me-media" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-me-media"></code></pre>
</span>
<form id="form-POSTapi-panel-me-media" data-method="POST"
      data-path="api/panel/me/media"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-me-media', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-me-media"
                    onclick="tryItOut('POSTapi-panel-me-media');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-me-media"
                    onclick="cancelTryOut('POSTapi-panel-me-media');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-me-media" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/me/media</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>file</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="file"
               data-endpoint="POSTapi-panel-me-media"
               value="porro"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="panel-organization-GETapi-panel-me-ctoken">Generate Centrifugo Broadcast authentication token for current user</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-me-ctoken">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/me/ctoken" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/me/ctoken"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-me-ctoken">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Unauthenticated.&quot;,
    &quot;message&quot;: &quot;You must be authenticated, to perform this request.&quot;,
    &quot;error&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-me-ctoken" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-me-ctoken"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-me-ctoken"></code></pre>
</span>
<span id="execution-error-GETapi-panel-me-ctoken" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-me-ctoken"></code></pre>
</span>
<form id="form-GETapi-panel-me-ctoken" data-method="GET"
      data-path="api/panel/me/ctoken"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-me-ctoken', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-me-ctoken"
                    onclick="tryItOut('GETapi-panel-me-ctoken');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-me-ctoken"
                    onclick="cancelTryOut('GETapi-panel-me-ctoken');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-me-ctoken" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/me/ctoken</code></b>
        </p>
                    </form>

        <h1 id="panel-users">Panel Users</h1>

    

            <h2 id="panel-users-GETapi-panel-users-members">Show organization members</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-users-members">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/users/members" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/members"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-users-members">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 51,
            &quot;name&quot;: &quot;Oda Dickens&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 60,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/60/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=0f91f1801e557bcb7b038929b8cfb961f6332d47ff212bbd790b09c7de3e80d2&quot;
                }
            },
            &quot;description&quot;: &quot;Hic soluta est quod et qui hic. Libero facilis soluta accusamus sed eveniet quos. Alias amet fuga possimus cumque et. Et et voluptatum fugit at.&quot;,
            &quot;age&quot;: 21,
            &quot;score&quot;: 269,
            &quot;level&quot;: 4,
            &quot;need_points_for_new_level&quot;: 371,
            &quot;city&quot;: {
                &quot;id&quot;: 92,
                &quot;name&quot;: &quot;Damion Purdy&quot;
            }
        },
        {
            &quot;id&quot;: 52,
            &quot;name&quot;: &quot;Wiley Zemlak&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 61,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/61/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=7170283b5d30a22dead6d76fcb3491b31991aaa19633c1d2e9632dd7feb3cd33&quot;
                }
            },
            &quot;description&quot;: &quot;Voluptas modi aliquam et hic. Dignissimos delectus eum veritatis. Similique quo nulla facere quasi dolor. Optio necessitatibus voluptas neque dolores maxime.&quot;,
            &quot;age&quot;: 22,
            &quot;score&quot;: 52,
            &quot;level&quot;: 2,
            &quot;need_points_for_new_level&quot;: 108,
            &quot;city&quot;: {
                &quot;id&quot;: 93,
                &quot;name&quot;: &quot;Maxine Kautzer DVM&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-users-members" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-users-members"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-users-members"></code></pre>
</span>
<span id="execution-error-GETapi-panel-users-members" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-users-members"></code></pre>
</span>
<form id="form-GETapi-panel-users-members" data-method="GET"
      data-path="api/panel/users/members"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-users-members', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-users-members"
                    onclick="tryItOut('GETapi-panel-users-members');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-users-members"
                    onclick="cancelTryOut('GETapi-panel-users-members');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-users-members" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/users/members</code></b>
        </p>
                    </form>

            <h2 id="panel-users-GETapi-panel-users-subscribers">Show organization subscribers</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-users-subscribers">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/users/subscribers" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/subscribers"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-users-subscribers">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 53,
            &quot;name&quot;: &quot;Jess Gusikowski MD&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 62,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/62/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=17336a5fe09eb4113b714a74062fa2a592d26fa10c803d9301cf550480c8c414&quot;
                }
            },
            &quot;description&quot;: &quot;Possimus dolorem ad odit illum nihil ullam esse hic. Qui laboriosam sed illo praesentium animi est. Assumenda voluptate et quisquam suscipit.&quot;,
            &quot;age&quot;: 22,
            &quot;score&quot;: 823574,
            &quot;level&quot;: 16,
            &quot;need_points_for_new_level&quot;: 1797866,
            &quot;city&quot;: {
                &quot;id&quot;: 94,
                &quot;name&quot;: &quot;Kane Deckow&quot;
            }
        },
        {
            &quot;id&quot;: 54,
            &quot;name&quot;: &quot;Benjamin Rath V&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 63,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/63/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=92f6301a451aff1bafda8fa38d7dc8ed489ed1f7c4dfc60ff5c9956695508410&quot;
                }
            },
            &quot;description&quot;: &quot;Alias placeat architecto quia animi. Aut doloremque est dolores eos odio quod dicta.&quot;,
            &quot;age&quot;: 19,
            &quot;score&quot;: 2,
            &quot;level&quot;: 0,
            &quot;need_points_for_new_level&quot;: 38,
            &quot;city&quot;: {
                &quot;id&quot;: 95,
                &quot;name&quot;: &quot;Mekhi Botsford&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-users-subscribers" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-users-subscribers"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-users-subscribers"></code></pre>
</span>
<span id="execution-error-GETapi-panel-users-subscribers" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-users-subscribers"></code></pre>
</span>
<form id="form-GETapi-panel-users-subscribers" data-method="GET"
      data-path="api/panel/users/subscribers"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-users-subscribers', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-users-subscribers"
                    onclick="tryItOut('GETapi-panel-users-subscribers');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-users-subscribers"
                    onclick="cancelTryOut('GETapi-panel-users-subscribers');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-users-subscribers" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/users/subscribers</code></b>
        </p>
                    </form>

            <h2 id="panel-users-GETapi-panel-users-participants">Show participants of organizations events</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-users-participants">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/users/participants" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/participants"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-users-participants">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 55,
            &quot;name&quot;: &quot;Mrs. Kaitlin Welch I&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 64,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/64/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=31b4b41d9d4b1df2aca024f34ade0d3440331df8b24dff57bdcfc04d73d45f7a&quot;
                }
            },
            &quot;description&quot;: &quot;At natus quis optio recusandae id aut. Soluta molestiae omnis eveniet. Libero rerum impedit nobis quia dolores sint omnis. Nisi et suscipit omnis quidem unde.&quot;,
            &quot;age&quot;: 26,
            &quot;score&quot;: 80,
            &quot;level&quot;: 2,
            &quot;need_points_for_new_level&quot;: 80,
            &quot;city&quot;: {
                &quot;id&quot;: 96,
                &quot;name&quot;: &quot;Walker Hermann IV&quot;
            }
        },
        {
            &quot;id&quot;: 56,
            &quot;name&quot;: &quot;Emmett Gerhold&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 65,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/65/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=b006e36fb8ca1d1cd2617908be45c89a081f869bbe426cfc0c997a9bc499e4c4&quot;
                }
            },
            &quot;description&quot;: &quot;Sunt aut odit dolorem voluptas fugiat est. Maxime blanditiis nihil est expedita id illo omnis. Quas rerum qui aperiam error quasi voluptatem consequatur. Facilis aliquam a sed adipisci aperiam.&quot;,
            &quot;age&quot;: 20,
            &quot;score&quot;: 870491487,
            &quot;level&quot;: 26,
            &quot;need_points_for_new_level&quot;: 1813863073,
            &quot;city&quot;: {
                &quot;id&quot;: 97,
                &quot;name&quot;: &quot;Mr. Nikko McLaughlin II&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-users-participants" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-users-participants"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-users-participants"></code></pre>
</span>
<span id="execution-error-GETapi-panel-users-participants" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-users-participants"></code></pre>
</span>
<form id="form-GETapi-panel-users-participants" data-method="GET"
      data-path="api/panel/users/participants"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-users-participants', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-users-participants"
                    onclick="tryItOut('GETapi-panel-users-participants');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-users-participants"
                    onclick="cancelTryOut('GETapi-panel-users-participants');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-users-participants" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/users/participants</code></b>
        </p>
                    </form>

            <h2 id="panel-users-GETapi-panel-users-search">Return users from search.</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-users-search">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/users/search?search=test" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"search\": \"afzcvwzqmkht\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/search"
);

const params = {
    "search": "test",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "search": "afzcvwzqmkht"
};

fetch(url, {
    method: "GET",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-users-search">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 57,
            &quot;name&quot;: &quot;Mr. Paris Torphy III&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 66,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/66/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=c7b3bfc71b428e4c2d4c2fe8d680551da0ff01f7c4196e5b05b35e9baf789faa&quot;
                }
            },
            &quot;description&quot;: &quot;Id ab non in facere est. Laudantium omnis voluptate ab facere. Omnis nihil blanditiis sint qui eligendi sed laudantium optio. Corporis a facere ad consequatur magni aut aut.&quot;,
            &quot;age&quot;: 25,
            &quot;score&quot;: 606972,
            &quot;level&quot;: 15,
            &quot;need_points_for_new_level&quot;: 703748,
            &quot;city&quot;: {
                &quot;id&quot;: 98,
                &quot;name&quot;: &quot;Mr. Jeffery Raynor DDS&quot;
            }
        },
        {
            &quot;id&quot;: 58,
            &quot;name&quot;: &quot;Dr. Denis Hansen Sr.&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 67,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/67/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=9047b604793eb9c434e981568d099b6b8b338c7713e1a667c7f7b1b1ff233adb&quot;
                }
            },
            &quot;description&quot;: &quot;Voluptatem id qui quidem ut odit mollitia. Voluptas ipsum blanditiis maxime et. Dolorem sed et explicabo praesentium non culpa aut sunt.&quot;,
            &quot;age&quot;: 24,
            &quot;score&quot;: 113902,
            &quot;level&quot;: 13,
            &quot;need_points_for_new_level&quot;: 213778,
            &quot;city&quot;: {
                &quot;id&quot;: 99,
                &quot;name&quot;: &quot;Mrs. Eleanore Schimmel&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-users-search" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-users-search"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-users-search"></code></pre>
</span>
<span id="execution-error-GETapi-panel-users-search" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-users-search"></code></pre>
</span>
<form id="form-GETapi-panel-users-search" data-method="GET"
      data-path="api/panel/users/search"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-users-search', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-users-search"
                    onclick="tryItOut('GETapi-panel-users-search');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-users-search"
                    onclick="cancelTryOut('GETapi-panel-users-search');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-users-search" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/users/search</code></b>
        </p>
                        <h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
                    <p>
                <b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="search"
               data-endpoint="GETapi-panel-users-search"
               value="test"
               data-component="query" hidden>
    <br>
<p>Search string. Possibly ['id', 'name', 'description']</p>
            </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="search"
               data-endpoint="GETapi-panel-users-search"
               value="afzcvwzqmkht"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 50 characters.</p>
        </p>
        </form>

            <h2 id="panel-users-GETapi-panel-users--user-">Show information about user</h2>

<p>
</p>



<span id="example-requests-GETapi-panel-users--user-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/panel/users/2" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/2"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-panel-users--user-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 59,
        &quot;name&quot;: &quot;Mr. Clemens Lubowitz DVM&quot;,
        &quot;avatar&quot;: {
            &quot;id&quot;: 68,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/68/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004527Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=48850c664aba514efacb38e7807ec0dc89919ffd8cd357dee055d7c66c717303&quot;
            }
        },
        &quot;description&quot;: &quot;Aut eos rerum sit ut id nihil. Magnam minus et ipsa voluptas animi a et. Quae voluptatem non libero. Natus et est molestiae nihil est quam eum.&quot;,
        &quot;age&quot;: 29,
        &quot;score&quot;: 32733,
        &quot;level&quot;: 11,
        &quot;need_points_for_new_level&quot;: 49187,
        &quot;city&quot;: {
            &quot;id&quot;: 100,
            &quot;name&quot;: &quot;Prof. Elliott Cormier IV&quot;
        }
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-panel-users--user-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-panel-users--user-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-panel-users--user-"></code></pre>
</span>
<span id="execution-error-GETapi-panel-users--user-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-panel-users--user-"></code></pre>
</span>
<form id="form-GETapi-panel-users--user-" data-method="GET"
      data-path="api/panel/users/{user}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-panel-users--user-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-panel-users--user-"
                    onclick="tryItOut('GETapi-panel-users--user-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-panel-users--user-"
                    onclick="cancelTryOut('GETapi-panel-users--user-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-panel-users--user-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/panel/users/{user}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="user"
               data-endpoint="GETapi-panel-users--user-"
               value="2"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="panel-users-POSTapi-panel-users--user--invite">Invite user to organization</h2>

<p>
</p>



<span id="example-requests-POSTapi-panel-users--user--invite">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/5/invite" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/panel/users/5/invite"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-panel-users--user--invite">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;invite sent&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-panel-users--user--invite" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-panel-users--user--invite"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-panel-users--user--invite"></code></pre>
</span>
<span id="execution-error-POSTapi-panel-users--user--invite" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-panel-users--user--invite"></code></pre>
</span>
<form id="form-POSTapi-panel-users--user--invite" data-method="POST"
      data-path="api/panel/users/{user}/invite"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-panel-users--user--invite', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-panel-users--user--invite"
                    onclick="tryItOut('POSTapi-panel-users--user--invite');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-panel-users--user--invite"
                    onclick="cancelTryOut('POSTapi-panel-users--user--invite');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-panel-users--user--invite" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/panel/users/{user}/invite</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="user"
               data-endpoint="POSTapi-panel-users--user--invite"
               value="5"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

        <h1 id="tag">Tag</h1>

    

            <h2 id="tag-GETapi-tags">Return list of present tags</h2>

<p>
</p>



<span id="example-requests-GETapi-tags">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/tags" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/tags"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-tags">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 57
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;data&quot;: []
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-tags" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-tags"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-tags"></code></pre>
</span>
<span id="execution-error-GETapi-tags" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-tags"></code></pre>
</span>
<form id="form-GETapi-tags" data-method="GET"
      data-path="api/tags"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-tags', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-tags"
                    onclick="tryItOut('GETapi-tags');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-tags"
                    onclick="cancelTryOut('GETapi-tags');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-tags" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/tags</code></b>
        </p>
                    </form>

        <h1 id="user-auth">User Auth</h1>

    

            <h2 id="user-auth-POSTapi-auth-otp">Creates and send OTP to specified PhoneNumber.</h2>

<p>
</p>

<p>Type of OTP generates might be changed with backend Feature Flags
<br> In Dev Mode it always: <b>1234</b></p>

<span id="example-requests-POSTapi-auth-otp">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/auth/otp" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"phone\": \"+79110297197\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/auth/otp"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "phone": "+79110297197"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-auth-otp">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 59
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;called&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-auth-otp" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-auth-otp"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-otp"></code></pre>
</span>
<span id="execution-error-POSTapi-auth-otp" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-otp"></code></pre>
</span>
<form id="form-POSTapi-auth-otp" data-method="POST"
      data-path="api/auth/otp"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-otp', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-auth-otp"
                    onclick="tryItOut('POSTapi-auth-otp');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-auth-otp"
                    onclick="cancelTryOut('POSTapi-auth-otp');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-auth-otp" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/auth/otp</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="phone"
               data-endpoint="POSTapi-auth-otp"
               value="+79110297197"
               data-component="body" hidden>
    <br>
<p>Phone Number, that used to create OTP.</p>
        </p>
        </form>

            <h2 id="user-auth-POSTapi-auth-login">Login function</h2>

<p>
</p>



<span id="example-requests-POSTapi-auth-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/auth/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"phone\": \"+79110297197\",
    \"code\": \"1234\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/auth/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "phone": "+79110297197",
    "code": "1234"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-auth-login">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 58
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;You tried to login, using invalid credentials&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-auth-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-auth-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-login"></code></pre>
</span>
<span id="execution-error-POSTapi-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-login"></code></pre>
</span>
<form id="form-POSTapi-auth-login" data-method="POST"
      data-path="api/auth/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-auth-login"
                    onclick="tryItOut('POSTapi-auth-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-auth-login"
                    onclick="cancelTryOut('POSTapi-auth-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-auth-login" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/auth/login</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="phone"
               data-endpoint="POSTapi-auth-login"
               value="+79110297197"
               data-component="body" hidden>
    <br>
<p>Phone Number, that used to create OTP.</p>
        </p>
                <p>
            <b><code>code</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="code"
               data-endpoint="POSTapi-auth-login"
               value="1234"
               data-component="body" hidden>
    <br>
<p>Code from OTP.</p>
        </p>
        </form>

        <h1 id="user-chats">User Chats</h1>

    

            <h2 id="user-chats-GETapi-chats">Show current user chats</h2>

<p>
</p>



<span id="example-requests-GETapi-chats">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/chats" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/chats"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-chats">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 26,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 1,
                    &quot;iid&quot;: &quot;eos&quot;,
                    &quot;member_id&quot;: 30,
                    &quot;message&quot;: &quot;culpa&quot;,
                    &quot;code&quot;: &quot;quia&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: false,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 26,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:18.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:18.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 18,
                            &quot;name&quot;: &quot;Claudie Lockman&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Veniam qui est dicta non quam sit esse. Maiores necessitatibus pariatur similique et. Quasi excepturi dignissimos quis et voluptas et.&quot;,
                            &quot;age&quot;: 28,
                            &quot;score&quot;: 8402,
                            &quot;level&quot;: 9,
                            &quot;need_points_for_new_level&quot;: 12078,
                            &quot;city&quot;: {
                                &quot;id&quot;: 28,
                                &quot;name&quot;: &quot;Abagail Hegmann&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 26
                },
                {
                    &quot;id&quot;: 27,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:18.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:18.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 19,
                            &quot;name&quot;: &quot;Miss Trisha Collins V&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Aliquid ut et aliquam et et placeat. Neque consequatur qui consequatur consequatur nisi aut. Eligendi maiores perferendis saepe velit et sunt. Recusandae dolorem aut ratione aliquid.&quot;,
                            &quot;age&quot;: 26,
                            &quot;score&quot;: 23426996,
                            &quot;level&quot;: 21,
                            &quot;need_points_for_new_level&quot;: 60459084,
                            &quot;city&quot;: {
                                &quot;id&quot;: 29,
                                &quot;name&quot;: &quot;Ezequiel Cronin PhD&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 26
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 8,
                &quot;name&quot;: &quot;And it&#039;ll fetch.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 8,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:18.799365Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:18.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:18.000000Z&quot;
        },
        {
            &quot;id&quot;: 32,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 2,
                    &quot;iid&quot;: &quot;magnam&quot;,
                    &quot;member_id&quot;: 37,
                    &quot;message&quot;: &quot;laborum&quot;,
                    &quot;code&quot;: &quot;vel&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: true,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 33,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 21,
                            &quot;name&quot;: &quot;Abdullah Kirlin V&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Facilis et culpa sed consequuntur dolorum ut. Voluptates et nulla eius blanditiis consequuntur qui eum. Veritatis nihil aut et voluptates eligendi voluptate.&quot;,
                            &quot;age&quot;: 27,
                            &quot;score&quot;: 39775,
                            &quot;level&quot;: 11,
                            &quot;need_points_for_new_level&quot;: 42145,
                            &quot;city&quot;: {
                                &quot;id&quot;: 33,
                                &quot;name&quot;: &quot;Jevon Altenwerth&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 32
                },
                {
                    &quot;id&quot;: 34,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 22,
                            &quot;name&quot;: &quot;Meda Boyle DDS&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Veniam suscipit ex quas assumenda maiores quia magni expedita. Accusamus eum et facilis nihil explicabo harum. In sunt labore ad minima culpa et. Reiciendis ex assumenda iste ex ipsum sed.&quot;,
                            &quot;age&quot;: 21,
                            &quot;score&quot;: 84851,
                            &quot;level&quot;: 13,
                            &quot;need_points_for_new_level&quot;: 242829,
                            &quot;city&quot;: {
                                &quot;id&quot;: 34,
                                &quot;name&quot;: &quot;Briana Morissette&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 32
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 10,
                &quot;name&quot;: &quot;Alice. &#039;I&#039;M not a.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 10,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:19.049217Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-chats" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-chats"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-chats"></code></pre>
</span>
<span id="execution-error-GETapi-chats" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-chats"></code></pre>
</span>
<form id="form-GETapi-chats" data-method="GET"
      data-path="api/chats"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-chats', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-chats"
                    onclick="tryItOut('GETapi-chats');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-chats"
                    onclick="cancelTryOut('GETapi-chats');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-chats" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/chats</code></b>
        </p>
                    </form>

            <h2 id="user-chats-GETapi-chats-events">Show current user events chats</h2>

<p>
</p>



<span id="example-requests-GETapi-chats-events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/chats/events" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/chats/events"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-chats-events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 38,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 3,
                    &quot;iid&quot;: &quot;debitis&quot;,
                    &quot;member_id&quot;: 44,
                    &quot;message&quot;: &quot;at&quot;,
                    &quot;code&quot;: &quot;iure&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: false,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 40,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 24,
                            &quot;name&quot;: &quot;Juwan Graham&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Ut voluptate error rerum aut consequatur quia dolores. Quia velit iusto blanditiis qui autem dolorem qui. Veniam iure nostrum facere laudantium corrupti.&quot;,
                            &quot;age&quot;: 28,
                            &quot;score&quot;: 275347371,
                            &quot;level&quot;: 24,
                            &quot;need_points_for_new_level&quot;: 395741269,
                            &quot;city&quot;: {
                                &quot;id&quot;: 38,
                                &quot;name&quot;: &quot;Antonia Stracke Sr.&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 38
                },
                {
                    &quot;id&quot;: 41,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 25,
                            &quot;name&quot;: &quot;Dr. Annamae Kuhn MD&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Tempore repellendus odio magni ipsa. Et quam non aut ut voluptas sit officia. Qui possimus eius quas et reiciendis.&quot;,
                            &quot;age&quot;: 25,
                            &quot;score&quot;: 8,
                            &quot;level&quot;: 0,
                            &quot;need_points_for_new_level&quot;: 32,
                            &quot;city&quot;: {
                                &quot;id&quot;: 39,
                                &quot;name&quot;: &quot;Chelsea Rempel&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 38
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 12,
                &quot;name&quot;: &quot;English!&#039; said the.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 12,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:19.349331Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
        },
        {
            &quot;id&quot;: 44,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 4,
                    &quot;iid&quot;: &quot;iusto&quot;,
                    &quot;member_id&quot;: 51,
                    &quot;message&quot;: &quot;omnis&quot;,
                    &quot;code&quot;: &quot;et&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: false,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 47,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 27,
                            &quot;name&quot;: &quot;Felicita Daniel&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Dicta fugiat pariatur non minima sapiente. Et ut animi deleniti ex quia blanditiis voluptate. Fuga aperiam et voluptas. Consequatur et eius consequuntur consequatur quibusdam et.&quot;,
                            &quot;age&quot;: 25,
                            &quot;score&quot;: 95837,
                            &quot;level&quot;: 13,
                            &quot;need_points_for_new_level&quot;: 231843,
                            &quot;city&quot;: {
                                &quot;id&quot;: 43,
                                &quot;name&quot;: &quot;Miss Marge Wisoky Sr.&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 44
                },
                {
                    &quot;id&quot;: 48,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 28,
                            &quot;name&quot;: &quot;Roderick Gutkowski&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Id eius laudantium esse hic natus. Est corporis sit occaecati. Nihil et neque culpa sapiente et et quaerat ut. Minus magnam optio non velit maiores voluptatem.&quot;,
                            &quot;age&quot;: 25,
                            &quot;score&quot;: 870223789,
                            &quot;level&quot;: 26,
                            &quot;need_points_for_new_level&quot;: 1814130771,
                            &quot;city&quot;: {
                                &quot;id&quot;: 44,
                                &quot;name&quot;: &quot;Wanda Gulgowski&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 44
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 14,
                &quot;name&quot;: &quot;Hatter. He came in.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 14,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:19.572590Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-chats-events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-chats-events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-chats-events"></code></pre>
</span>
<span id="execution-error-GETapi-chats-events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-chats-events"></code></pre>
</span>
<form id="form-GETapi-chats-events" data-method="GET"
      data-path="api/chats/events"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-chats-events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-chats-events"
                    onclick="tryItOut('GETapi-chats-events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-chats-events"
                    onclick="cancelTryOut('GETapi-chats-events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-chats-events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/chats/events</code></b>
        </p>
                    </form>

            <h2 id="user-chats-GETapi-chats-organizations">Show current user events chats</h2>

<p>
</p>



<span id="example-requests-GETapi-chats-organizations">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/chats/organizations" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/chats/organizations"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-chats-organizations">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 50,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 5,
                    &quot;iid&quot;: &quot;quos&quot;,
                    &quot;member_id&quot;: 58,
                    &quot;message&quot;: &quot;illum&quot;,
                    &quot;code&quot;: &quot;quis&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: false,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 54,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 30,
                            &quot;name&quot;: &quot;Prof. Stacey Rice&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Perferendis et et qui voluptas quidem omnis facere quia. Et exercitationem enim repellendus facere. Vel labore ex iusto sed.&quot;,
                            &quot;age&quot;: 22,
                            &quot;score&quot;: 597778608,
                            &quot;level&quot;: 25,
                            &quot;need_points_for_new_level&quot;: 744398672,
                            &quot;city&quot;: {
                                &quot;id&quot;: 48,
                                &quot;name&quot;: &quot;Myrtie Wyman&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 50
                },
                {
                    &quot;id&quot;: 55,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 31,
                            &quot;name&quot;: &quot;Albina Brekke&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Sint ducimus animi qui ducimus quod eos. Totam exercitationem et dolore nulla. Voluptatem quisquam ducimus itaque repudiandae et laboriosam est. Laborum iste dolorem rem fugiat.&quot;,
                            &quot;age&quot;: 24,
                            &quot;score&quot;: 293068,
                            &quot;level&quot;: 14,
                            &quot;need_points_for_new_level&quot;: 362292,
                            &quot;city&quot;: {
                                &quot;id&quot;: 49,
                                &quot;name&quot;: &quot;Jennings Rau&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 50
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 16,
                &quot;name&quot;: &quot;I&#039;ll just see what.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 16,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:19.858237Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:19.000000Z&quot;
        },
        {
            &quot;id&quot;: 56,
            &quot;messages&quot;: [
                {
                    &quot;id&quot;: 6,
                    &quot;iid&quot;: &quot;quibusdam&quot;,
                    &quot;member_id&quot;: 65,
                    &quot;message&quot;: &quot;est&quot;,
                    &quot;code&quot;: &quot;exercitationem&quot;,
                    &quot;type&quot;: &quot;message&quot;,
                    &quot;silent&quot;: false,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;
                }
            ],
            &quot;members&quot;: [
                {
                    &quot;id&quot;: 61,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 33,
                            &quot;name&quot;: &quot;Tito Emard&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Animi et id nam facere numquam. Ex consectetur est facilis et beatae accusamus. Eos commodi tempore quaerat eum.&quot;,
                            &quot;age&quot;: 21,
                            &quot;score&quot;: 160668771,
                            &quot;level&quot;: 23,
                            &quot;need_points_for_new_level&quot;: 174875549,
                            &quot;city&quot;: {
                                &quot;id&quot;: 53,
                                &quot;name&quot;: &quot;Prof. Frederique Wehner&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 56
                },
                {
                    &quot;id&quot;: 62,
                    &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                    &quot;member&quot;: {
                        &quot;user&quot;: {
                            &quot;id&quot;: 34,
                            &quot;name&quot;: &quot;Dane Harber&quot;,
                            &quot;avatar&quot;: null,
                            &quot;description&quot;: &quot;Eum cumque fuga dolores qui qui pariatur facere. Voluptate velit id enim amet eligendi. Nulla veniam odio nemo vero itaque dolores libero.&quot;,
                            &quot;age&quot;: 23,
                            &quot;score&quot;: 8524362,
                            &quot;level&quot;: 19,
                            &quot;need_points_for_new_level&quot;: 12447158,
                            &quot;city&quot;: {
                                &quot;id&quot;: 54,
                                &quot;name&quot;: &quot;Mr. Leonel Krajcik&quot;
                            }
                        }
                    },
                    &quot;chat_id&quot;: 56
                }
            ],
            &quot;chatable&quot;: {
                &quot;id&quot;: 18,
                &quot;name&quot;: &quot;Alice, and her.&quot;,
                &quot;type&quot;: &quot;event&quot;,
                &quot;avatar&quot;: null
            },
            &quot;chatable_type&quot;: &quot;event&quot;,
            &quot;chatable_id&quot;: 18,
            &quot;is_closed&quot;: null,
            &quot;closed_at&quot;: null,
            &quot;deleted_at&quot;: &quot;2022-06-13T00:45:20.083969Z&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-chats-organizations" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-chats-organizations"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-chats-organizations"></code></pre>
</span>
<span id="execution-error-GETapi-chats-organizations" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-chats-organizations"></code></pre>
</span>
<form id="form-GETapi-chats-organizations" data-method="GET"
      data-path="api/chats/organizations"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-chats-organizations', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-chats-organizations"
                    onclick="tryItOut('GETapi-chats-organizations');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-chats-organizations"
                    onclick="cancelTryOut('GETapi-chats-organizations');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-chats-organizations" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/chats/organizations</code></b>
        </p>
                    </form>

            <h2 id="user-chats-GETapi-chats--chat-">Show chat resource</h2>

<p>
</p>



<span id="example-requests-GETapi-chats--chat-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/chats/1" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/chats/1"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-chats--chat-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 62,
        &quot;messages&quot;: [
            {
                &quot;id&quot;: 7,
                &quot;iid&quot;: &quot;tenetur&quot;,
                &quot;member_id&quot;: 72,
                &quot;message&quot;: &quot;voluptatem&quot;,
                &quot;code&quot;: &quot;numquam&quot;,
                &quot;type&quot;: &quot;message&quot;,
                &quot;silent&quot;: true,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;
            }
        ],
        &quot;members&quot;: [
            {
                &quot;id&quot;: 68,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                &quot;member&quot;: {
                    &quot;user&quot;: {
                        &quot;id&quot;: 36,
                        &quot;name&quot;: &quot;Ila Herman&quot;,
                        &quot;avatar&quot;: null,
                        &quot;description&quot;: &quot;Quos repellendus temporibus tempore magnam. Cumque ad beatae omnis tempora. Praesentium quos molestiae maxime velit nobis quia.&quot;,
                        &quot;age&quot;: 27,
                        &quot;score&quot;: 7004,
                        &quot;level&quot;: 9,
                        &quot;need_points_for_new_level&quot;: 13476,
                        &quot;city&quot;: {
                            &quot;id&quot;: 58,
                            &quot;name&quot;: &quot;Coby Harber MD&quot;
                        }
                    }
                },
                &quot;chat_id&quot;: 62
            },
            {
                &quot;id&quot;: 69,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
                &quot;member&quot;: {
                    &quot;user&quot;: {
                        &quot;id&quot;: 37,
                        &quot;name&quot;: &quot;Fermin Goldner&quot;,
                        &quot;avatar&quot;: null,
                        &quot;description&quot;: &quot;Tempora officiis quis perferendis. Minus sed distinctio sunt quidem officia voluptas. Accusantium ipsam est explicabo sit voluptas ad maiores. Eum amet sit et ex. Voluptas sequi in in.&quot;,
                        &quot;age&quot;: 26,
                        &quot;score&quot;: 695624,
                        &quot;level&quot;: 16,
                        &quot;need_points_for_new_level&quot;: 1925816,
                        &quot;city&quot;: {
                            &quot;id&quot;: 59,
                            &quot;name&quot;: &quot;Dr. Marcel Beahan&quot;
                        }
                    }
                },
                &quot;chat_id&quot;: 62
            }
        ],
        &quot;chatable&quot;: {
            &quot;id&quot;: 20,
            &quot;name&quot;: &quot;I? Ah, THAT&#039;S the.&quot;,
            &quot;type&quot;: &quot;event&quot;,
            &quot;avatar&quot;: null
        },
        &quot;chatable_type&quot;: &quot;event&quot;,
        &quot;chatable_id&quot;: 20,
        &quot;is_closed&quot;: null,
        &quot;closed_at&quot;: null,
        &quot;deleted_at&quot;: &quot;2022-06-13T00:45:20.403030Z&quot;,
        &quot;created_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;,
        &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.000000Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-chats--chat-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-chats--chat-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-chats--chat-"></code></pre>
</span>
<span id="execution-error-GETapi-chats--chat-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-chats--chat-"></code></pre>
</span>
<form id="form-GETapi-chats--chat-" data-method="GET"
      data-path="api/chats/{chat}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-chats--chat-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-chats--chat-"
                    onclick="tryItOut('GETapi-chats--chat-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-chats--chat-"
                    onclick="cancelTryOut('GETapi-chats--chat-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-chats--chat-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/chats/{chat}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>chat</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="chat"
               data-endpoint="GETapi-chats--chat-"
               value="1"
               data-component="url" hidden>
    <br>
<p>The ID of chat.</p>
            </p>
                    </form>

            <h2 id="user-chats-POSTapi-chats--chat--message">Send message to chat</h2>

<p>
</p>



<span id="example-requests-POSTapi-chats--chat--message">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/chats/48/message" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"message\": \"Test chat message! Listen me!\",
    \"iid\": \"EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/chats/48/message"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "message": "Test chat message! Listen me!",
    "iid": "EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-chats--chat--message">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 8,
        &quot;iid&quot;: &quot;nobis&quot;,
        &quot;member_id&quot;: 75,
        &quot;message&quot;: &quot;inventore&quot;,
        &quot;code&quot;: &quot;perspiciatis&quot;,
        &quot;type&quot;: &quot;message&quot;,
        &quot;silent&quot;: true,
        &quot;created_at&quot;: &quot;2022-06-13T00:45:20.671662Z&quot;,
        &quot;updated_at&quot;: &quot;2022-06-13T00:45:20.671663Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-chats--chat--message" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-chats--chat--message"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-chats--chat--message"></code></pre>
</span>
<span id="execution-error-POSTapi-chats--chat--message" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-chats--chat--message"></code></pre>
</span>
<form id="form-POSTapi-chats--chat--message" data-method="POST"
      data-path="api/chats/{chat}/message"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-chats--chat--message', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-chats--chat--message"
                    onclick="tryItOut('POSTapi-chats--chat--message');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-chats--chat--message"
                    onclick="cancelTryOut('POSTapi-chats--chat--message');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-chats--chat--message" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/chats/{chat}/message</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>chat</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="chat"
               data-endpoint="POSTapi-chats--chat--message"
               value="48"
               data-component="url" hidden>
    <br>
<p>The ID of chat, used for sending message.</p>
            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>message</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="message"
               data-endpoint="POSTapi-chats--chat--message"
               value="Test chat message! Listen me!"
               data-component="body" hidden>
    <br>
<p>Message of chat message.</p>
        </p>
                <p>
            <b><code>iid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="iid"
               data-endpoint="POSTapi-chats--chat--message"
               value="EDACE0E2-D7C7-4FA2-B89E-3F6B699C7A6E"
               data-component="body" hidden>
    <br>
<p>IID of chat message.</p>
        </p>
        </form>

        <h1 id="user-events">User Events</h1>

    

            <h2 id="user-events-GETapi-events">Show available events</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/events" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/events"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 5,
            &quot;title&quot;: &quot;When she got up.&quot;,
            &quot;description&quot;: &quot;Autem dignissimos consequatur ipsum recusandae veniam. Est nobis ad quis ad aut earum officia. Sit soluta eos quo eligendi.&quot;,
            &quot;place&quot;: &quot;58409 Weimann Corners\nNorth Dana, RI 92685-3893&quot;,
            &quot;start_at&quot;: &quot;2022-06-18T00:09:17.570236Z&quot;,
            &quot;min_age&quot;: 12,
            &quot;is_offline&quot;: true,
            &quot;tags&quot;: [
                {
                    &quot;id&quot;: 18,
                    &quot;name&quot;: &quot;tag J&quot;,
                    &quot;group_name&quot;: &quot;group F&quot;
                }
            ],
            &quot;avatar&quot;: {
                &quot;id&quot;: 14,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/14/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004517Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=fafd6269dec5b7ae2d7bbc545e4b01633ba42d626c25a2dd67fef76de027dffe&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 17,
                &quot;name&quot;: &quot;Prof. Eusebio Haag&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 11,
                &quot;name&quot;: &quot;Dr.&quot;,
                &quot;description&quot;: &quot;Assumenda nulla architecto quas quia. Id est qui est error aspernatur ducimus assumenda. Et dolorem quo aut repellat officia molestiae.&quot;,
                &quot;contacts&quot;: &quot;59405 Helene Rapid Suite 680\nNorth Raegan, NE 24182-7896&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 13,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/13/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004517Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=d1ce81dbf4cf27fa15ee3d7b9e097c4888ae486234510aedb673c48000b72465&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 1
        },
        {
            &quot;id&quot;: 6,
            &quot;title&quot;: &quot;Cat. &#039;--so long as.&quot;,
            &quot;description&quot;: &quot;Quibusdam aliquam necessitatibus sapiente nam. Illum asperiores veritatis sunt accusantium temporibus animi. Velit modi soluta labore et beatae ea perspiciatis.&quot;,
            &quot;place&quot;: &quot;6047 Reynolds Flats\nLethamouth, AL 08945&quot;,
            &quot;start_at&quot;: &quot;2022-06-13T06:54:17.723645Z&quot;,
            &quot;min_age&quot;: 0,
            &quot;is_offline&quot;: false,
            &quot;tags&quot;: [
                {
                    &quot;id&quot;: 19,
                    &quot;name&quot;: &quot;tag C&quot;,
                    &quot;group_name&quot;: &quot;group G&quot;
                }
            ],
            &quot;avatar&quot;: {
                &quot;id&quot;: 16,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/16/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004517Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=4f143f0e902a85ca2375e6097f06739debd8c748b834ed491b715fc11e9c5d17&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 19,
                &quot;name&quot;: &quot;Bettye Tromp&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 12,
                &quot;name&quot;: &quot;Dr.&quot;,
                &quot;description&quot;: &quot;Qui illo voluptate commodi consequatur neque quaerat. Corporis et praesentium totam perferendis vel. Odio ipsam quae ullam libero beatae. Magnam blanditiis aut cumque voluptatem ut eius.&quot;,
                &quot;contacts&quot;: &quot;98627 Barton Point\nNew Ashleeton, CT 27617-5054&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 15,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/15/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004517Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=cdfbf5331abc6e5ef43648dd4dbd4da15a073fdc1d854ddc7b8519922f71ba43&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 1
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-events"></code></pre>
</span>
<span id="execution-error-GETapi-events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-events"></code></pre>
</span>
<form id="form-GETapi-events" data-method="GET"
      data-path="api/events"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-events"
                    onclick="tryItOut('GETapi-events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-events"
                    onclick="cancelTryOut('GETapi-events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/events</code></b>
        </p>
                <p>
            <label id="auth-GETapi-events" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-events"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-events-GETapi-events--event-">Show information about event</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-events--event-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/events/6" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/events/6"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-events--event-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 7,
        &quot;title&quot;: &quot;Even the Duchess.&quot;,
        &quot;description&quot;: &quot;Magni veniam assumenda aut. Esse qui enim officiis aspernatur. Consectetur voluptatem maiores autem pariatur fugit fugit. Delectus optio doloribus numquam aut et vitae.&quot;,
        &quot;place&quot;: &quot;6272 Delmer Viaduct Apt. 470\nSouth Vivianneton, NH 24107&quot;,
        &quot;start_at&quot;: &quot;2022-06-19T04:46:17.890044Z&quot;,
        &quot;min_age&quot;: 12,
        &quot;is_offline&quot;: true,
        &quot;tags&quot;: [
            {
                &quot;id&quot;: 20,
                &quot;name&quot;: &quot;tag H&quot;,
                &quot;group_name&quot;: &quot;group C&quot;
            }
        ],
        &quot;avatar&quot;: {
            &quot;id&quot;: 18,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/18/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=1a5265a892fafaaf46aaec0ebd807d2c6006e29821bec912b265cdbc5fdb1171&quot;
            }
        },
        &quot;city&quot;: {
            &quot;id&quot;: 21,
            &quot;name&quot;: &quot;Cydney Waters&quot;
        },
        &quot;organization&quot;: {
            &quot;id&quot;: 13,
            &quot;name&quot;: &quot;Ms.&quot;,
            &quot;description&quot;: &quot;Enim qui a nostrum eveniet et. Totam et ea dolore nemo cumque quibusdam maiores aperiam. Ut vero non suscipit est quisquam.&quot;,
            &quot;contacts&quot;: &quot;964 Ursula Junction Apt. 500\nRaymundomouth, UT 01622-4947&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 17,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/17/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=c1ea90e23b843126c4c2fe844b268a2ee9e636240b3fddc61067ac1b62573220&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        &quot;roles&quot;: [
            {
                &quot;id&quot;: 1,
                &quot;name&quot;: &quot;Kyle Hessel&quot;,
                &quot;description&quot;: &quot;Placeat sequi cumque magnam ducimus nemo. Nihil saepe et dolorem ea qui. Minima et dicta sint quidem aperiam sapiente voluptas. Est tempora nihil ut mollitia in.&quot;
            }
        ],
        &quot;participants_count&quot;: 1
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-events--event-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-events--event-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-events--event-"></code></pre>
</span>
<span id="execution-error-GETapi-events--event-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-events--event-"></code></pre>
</span>
<form id="form-GETapi-events--event-" data-method="GET"
      data-path="api/events/{event}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-events--event-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-events--event-"
                    onclick="tryItOut('GETapi-events--event-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-events--event-"
                    onclick="cancelTryOut('GETapi-events--event-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-events--event-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/events/{event}</code></b>
        </p>
                <p>
            <label id="auth-GETapi-events--event-" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-events--event-"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="GETapi-events--event-"
               value="6"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-events-GETapi-events--event--participants">Show participants of selected event</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-events--event--participants">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/events/16/participants" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/events/16/participants"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-events--event--participants">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 14,
            &quot;name&quot;: &quot;Ms. Sister Ziemann DDS&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 19,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/19/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=875a2dcf2f21dac37932a8ffcbab742d845ba87a8b26d93e28834ed6b91cf78a&quot;
                }
            },
            &quot;description&quot;: &quot;Quibusdam ratione illo sapiente nostrum earum. Sapiente dolore nemo tempore illo aut totam consequatur. Ratione ut ut quia cupiditate voluptatum magni.&quot;,
            &quot;age&quot;: 19,
            &quot;score&quot;: 572093,
            &quot;level&quot;: 15,
            &quot;need_points_for_new_level&quot;: 738627,
            &quot;city&quot;: {
                &quot;id&quot;: 23,
                &quot;name&quot;: &quot;Mr. Darion Osinski Jr.&quot;
            }
        },
        {
            &quot;id&quot;: 15,
            &quot;name&quot;: &quot;Annabelle Bins&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 20,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/20/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=a969903ba4ac1d97fef0ddf0f790cfa17bdf1c8536696afddb5632d86e0c1df5&quot;
                }
            },
            &quot;description&quot;: &quot;Modi voluptatem et pariatur. Qui tempore facere quos tenetur dicta reprehenderit minima qui. Dolores numquam et dolores quos libero.&quot;,
            &quot;age&quot;: 20,
            &quot;score&quot;: 37724844,
            &quot;level&quot;: 21,
            &quot;need_points_for_new_level&quot;: 46161236,
            &quot;city&quot;: {
                &quot;id&quot;: 24,
                &quot;name&quot;: &quot;Dr. Glennie Witting&quot;
            }
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-events--event--participants" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-events--event--participants"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-events--event--participants"></code></pre>
</span>
<span id="execution-error-GETapi-events--event--participants" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-events--event--participants"></code></pre>
</span>
<form id="form-GETapi-events--event--participants" data-method="GET"
      data-path="api/events/{event}/participants"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-events--event--participants', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-events--event--participants"
                    onclick="tryItOut('GETapi-events--event--participants');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-events--event--participants"
                    onclick="cancelTryOut('GETapi-events--event--participants');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-events--event--participants" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/events/{event}/participants</code></b>
        </p>
                <p>
            <label id="auth-GETapi-events--event--participants" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-events--event--participants"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="GETapi-events--event--participants"
               value="16"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-events-POSTapi-events--event--participate">Send participate request for join to event.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-POSTapi-events--event--participate">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/events/1/participate" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"roles\": [
        1,
        2,
        3
    ]
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/events/1/participate"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "roles": [
        1,
        2,
        3
    ]
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-events--event--participate">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Unauthenticated.&quot;,
    &quot;message&quot;: &quot;You must be authenticated, to perform this request.&quot;,
    &quot;error&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-events--event--participate" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-events--event--participate"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-events--event--participate"></code></pre>
</span>
<span id="execution-error-POSTapi-events--event--participate" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-events--event--participate"></code></pre>
</span>
<form id="form-POSTapi-events--event--participate" data-method="POST"
      data-path="api/events/{event}/participate"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-events--event--participate', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-events--event--participate"
                    onclick="tryItOut('POSTapi-events--event--participate');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-events--event--participate"
                    onclick="cancelTryOut('POSTapi-events--event--participate');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-events--event--participate" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/events/{event}/participate</code></b>
        </p>
                <p>
            <label id="auth-POSTapi-events--event--participate" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="POSTapi-events--event--participate"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>event</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="event"
               data-endpoint="POSTapi-events--event--participate"
               value="1"
               data-component="url" hidden>
    <br>

            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>roles</code></b>&nbsp;&nbsp;<small>integer[]</small>     <i>optional</i> &nbsp;
                <input type="number"
               name="roles[0]"
               data-endpoint="POSTapi-events--event--participate"
               data-component="body" hidden>
        <input type="number"
               name="roles[1]"
               data-endpoint="POSTapi-events--event--participate"
               data-component="body" hidden>
    <br>
<p>Nullable Id of roles, used for participate.</p>
        </p>
        </form>

        <h1 id="user-me">User Me</h1>

    

            <h2 id="user-me-GETapi-me">Display information about current user.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Delilah Purdy IV&quot;,
        &quot;avatar&quot;: {
            &quot;id&quot;: 1,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/1/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004515Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=7b247c41a2a6755ff01202d071fea4eb7943bd980a3242c58743fecca09b3514&quot;
            }
        },
        &quot;description&quot;: &quot;Quisquam quos pariatur iste fugiat iure minima reprehenderit. Eius quod est quasi nemo placeat sed doloribus repudiandae.&quot;,
        &quot;age&quot;: 29,
        &quot;score&quot;: 404,
        &quot;level&quot;: 5,
        &quot;need_points_for_new_level&quot;: 876,
        &quot;city&quot;: {
            &quot;id&quot;: 3,
            &quot;name&quot;: &quot;Merritt Wisozk&quot;
        }
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me"></code></pre>
</span>
<span id="execution-error-GETapi-me" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me"></code></pre>
</span>
<form id="form-GETapi-me" data-method="GET"
      data-path="api/me"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me"
                    onclick="tryItOut('GETapi-me');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me"
                    onclick="cancelTryOut('GETapi-me');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-PATCHapi-me">Update information about current user.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>After <b>FIRST</b> update, user WILL BE registered.</p>

<span id="example-requests-PATCHapi-me">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request PATCH \
    "https://mch-backend-staging.server.bonch.dev/api/me" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"name\": \"Pallam\",
    \"birthday\": \"1997-04-22\",
    \"description\": \"Beautiful text about myself\",
    \"city_id\": 1,
    \"language\": \"ru_RU\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Pallam",
    "birthday": "1997-04-22",
    "description": "Beautiful text about myself",
    "city_id": 1,
    "language": "ru_RU"
};

fetch(url, {
    method: "PATCH",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-PATCHapi-me">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 2,
        &quot;name&quot;: &quot;Marcos Douglas&quot;,
        &quot;avatar&quot;: {
            &quot;id&quot;: 2,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/2/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004515Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=99a776436fa09576e7277e2e2ae712123f1fce673143d420bf082a4b89f2dbc1&quot;
            }
        },
        &quot;description&quot;: &quot;Commodi excepturi excepturi et non ipsam. Distinctio consequatur odit corrupti repellendus. Quam harum vel quis et quia amet eaque.&quot;,
        &quot;age&quot;: 27,
        &quot;score&quot;: 79293,
        &quot;level&quot;: 12,
        &quot;need_points_for_new_level&quot;: 84547,
        &quot;city&quot;: {
            &quot;id&quot;: 4,
            &quot;name&quot;: &quot;Mattie Kessler&quot;
        }
    }
}</code>
 </pre>
    </span>
<span id="execution-results-PATCHapi-me" hidden>
    <blockquote>Received response<span
                id="execution-response-status-PATCHapi-me"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-PATCHapi-me"></code></pre>
</span>
<span id="execution-error-PATCHapi-me" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PATCHapi-me"></code></pre>
</span>
<form id="form-PATCHapi-me" data-method="PATCH"
      data-path="api/me"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('PATCHapi-me', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-PATCHapi-me"
                    onclick="tryItOut('PATCHapi-me');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-PATCHapi-me"
                    onclick="cancelTryOut('PATCHapi-me');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-PATCHapi-me" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-purple">PATCH</small>
            <b><code>api/me</code></b>
        </p>
                <p>
            <label id="auth-PATCHapi-me" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="PATCHapi-me"
                                                                data-component="header"></label>
        </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="name"
               data-endpoint="PATCHapi-me"
               value="Pallam"
               data-component="body" hidden>
    <br>
<p>Username.</p>
        </p>
                <p>
            <b><code>birthday</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="birthday"
               data-endpoint="PATCHapi-me"
               value="1997-04-22"
               data-component="body" hidden>
    <br>
<p>User birthday. Must be earlier than total 18 age.</p>
        </p>
                <p>
            <b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="description"
               data-endpoint="PATCHapi-me"
               value="Beautiful text about myself"
               data-component="body" hidden>
    <br>
<p>User story (user self-description).</p>
        </p>
                <p>
            <b><code>city_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
                <input type="number"
               name="city_id"
               data-endpoint="PATCHapi-me"
               value="1"
               data-component="body" hidden>
    <br>
<p>User city.</p>
        </p>
                <p>
            <b><code>language</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
                <input type="text"
               name="language"
               data-endpoint="PATCHapi-me"
               value="ru_RU"
               data-component="body" hidden>
    <br>
<p>User language setting. Available languages: ru_RU, en_US.</p>
        </p>
        </form>

            <h2 id="user-me-GETapi-me-organizations">Display organizations of current user (when user is member)</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-organizations">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/organizations" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/organizations"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-organizations">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;name&quot;: &quot;Mr.&quot;,
            &quot;description&quot;: &quot;Odio sint quibusdam quas. Saepe harum placeat officia repellat nihil veritatis. Aliquid asperiores amet qui sunt adipisci tempora.&quot;,
            &quot;contacts&quot;: &quot;246 Antonetta Ferry Apt. 008\nMullermouth, UT 10964&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 3,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/3/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004515Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=b26e5648ce2dc35a4955f776bea91a80e734e0ba076d0dc8850c36477f3d8674&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        {
            &quot;id&quot;: 2,
            &quot;name&quot;: &quot;Dr.&quot;,
            &quot;description&quot;: &quot;Neque magni aut cum neque consequatur earum velit. Vel odio et est explicabo voluptas minima ipsum.&quot;,
            &quot;contacts&quot;: &quot;474 Glover Springs Suite 611\nEast Athenashire, MT 13355-7816&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 4,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/4/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004515Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=6bf64e7119955527472ea38cedf8c6f4c1ff7052b109123339e91deecf5db436&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-organizations" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-organizations"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-organizations"></code></pre>
</span>
<span id="execution-error-GETapi-me-organizations" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-organizations"></code></pre>
</span>
<form id="form-GETapi-me-organizations" data-method="GET"
      data-path="api/me/organizations"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-organizations', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-organizations"
                    onclick="tryItOut('GETapi-me-organizations');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-organizations"
                    onclick="cancelTryOut('GETapi-me-organizations');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-organizations" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/organizations</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-organizations" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-organizations"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-GETapi-me-events">Display events of current user (when user is participant)</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/events" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/events"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;title&quot;: &quot;Mock Turtle in a.&quot;,
            &quot;description&quot;: &quot;Ut rerum expedita ut dicta nam. Quibusdam nobis optio voluptatibus eveniet nihil incidunt laboriosam. Quis quam non fugiat sit aliquam ipsa.&quot;,
            &quot;place&quot;: &quot;3456 Trevor Passage\nNorth Jayme, MI 86474-9168&quot;,
            &quot;start_at&quot;: &quot;2022-06-18T15:02:15.808728Z&quot;,
            &quot;min_age&quot;: 8,
            &quot;is_offline&quot;: false,
            &quot;tags&quot;: [],
            &quot;avatar&quot;: {
                &quot;id&quot;: 6,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/6/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004516Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=e748442c8f19430c5dc674e483118f37cdb52589c8620516a18c92dea64cb579&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 5,
                &quot;name&quot;: &quot;Eliseo Christiansen&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Mr.&quot;,
                &quot;description&quot;: &quot;Nihil alias non perspiciatis eos voluptatum impedit. Sit nisi dolor officia. Harum omnis ut totam odio odio illum. Et quisquam velit libero nesciunt sequi. Porro non saepe facere.&quot;,
                &quot;contacts&quot;: &quot;48219 Abbott Tunnel Suite 317\nMargarettview, OK 60169&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 5,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/5/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004516Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=5bdc36a8e0ab10382fda219c3786b4d7ecd9f112fdd9e4a49b260fe7f51eba2a&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 1
        },
        {
            &quot;id&quot;: 2,
            &quot;title&quot;: &quot;Quick, now!&#039; And.&quot;,
            &quot;description&quot;: &quot;Dolor ea aut incidunt. Et animi sit ullam enim consequatur eos. Commodi necessitatibus ut repellendus nesciunt provident maxime omnis a. Nemo ullam iste architecto porro molestiae.&quot;,
            &quot;place&quot;: &quot;8927 Konopelski Prairie\nBernhardbury, TX 39786-4717&quot;,
            &quot;start_at&quot;: &quot;2022-06-15T20:08:15.933916Z&quot;,
            &quot;min_age&quot;: 24,
            &quot;is_offline&quot;: true,
            &quot;tags&quot;: [],
            &quot;avatar&quot;: {
                &quot;id&quot;: 8,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/8/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004516Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=44e71404d66683f0d8d09f2ad5f48415d5bd65c771eb90d253d9db1ec96e59d5&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 7,
                &quot;name&quot;: &quot;Mr. Wilfred Yundt DDS&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 4,
                &quot;name&quot;: &quot;Miss&quot;,
                &quot;description&quot;: &quot;Animi sed qui enim fugiat sed. Magnam placeat quia exercitationem nihil ut doloremque officia quis. Id ut ex fugiat voluptatem aut et.&quot;,
                &quot;contacts&quot;: &quot;7011 Boehm Neck\nAmbershire, OH 57085-8088&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 7,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/7/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004516Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=a0afa4f7204096a6593d3b7151d4e29f1cae788d0d6e0156539093508c275a60&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 1
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-events"></code></pre>
</span>
<span id="execution-error-GETapi-me-events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-events"></code></pre>
</span>
<form id="form-GETapi-me-events" data-method="GET"
      data-path="api/me/events"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-events"
                    onclick="tryItOut('GETapi-me-events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-events"
                    onclick="cancelTryOut('GETapi-me-events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/events</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-events" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-events"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-GETapi-me-subscribes">Display subscribes of current user</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-subscribes">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/subscribes" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/subscribes"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-subscribes">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 5,
            &quot;name&quot;: &quot;Ms.&quot;,
            &quot;description&quot;: &quot;Aut recusandae iure voluptates est unde inventore veniam. Harum dolores qui error maiores quis consequatur. Officia non dolor laborum dolor ut architecto facere. Nihil dolor qui dolor consequuntur.&quot;,
            &quot;contacts&quot;: &quot;4545 Hirthe Hollow Apt. 202\nLake Romaville, VA 46794-3872&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 9,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/9/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004516Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=17deeb3055671d45f609f31d2433160437a54b17ca10eda8fc98fa8cbde3cf90&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        {
            &quot;id&quot;: 6,
            &quot;name&quot;: &quot;Prof.&quot;,
            &quot;description&quot;: &quot;Et qui quia consequatur. Voluptas placeat maxime et. Aut omnis rerum corrupti illum ut.&quot;,
            &quot;contacts&quot;: &quot;528 Kamryn Union Suite 029\nWest Frances, NE 90584-0298&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 10,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/10/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004516Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=d716ea508fdac2ee77f73c7ec20793388f3d9b363ff97fb9c8bc15292b6b5210&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-subscribes" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-subscribes"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-subscribes"></code></pre>
</span>
<span id="execution-error-GETapi-me-subscribes" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-subscribes"></code></pre>
</span>
<form id="form-GETapi-me-subscribes" data-method="GET"
      data-path="api/me/subscribes"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-subscribes', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-subscribes"
                    onclick="tryItOut('GETapi-me-subscribes');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-subscribes"
                    onclick="cancelTryOut('GETapi-me-subscribes');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-subscribes" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/subscribes</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-subscribes" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-subscribes"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-GETapi-me-ctoken">Generate Centrifugo Broadcast authentication token for current user</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-ctoken">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/ctoken" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/ctoken"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-ctoken">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Unauthenticated.&quot;,
    &quot;message&quot;: &quot;You must be authenticated, to perform this request.&quot;,
    &quot;error&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-ctoken" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-ctoken"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-ctoken"></code></pre>
</span>
<span id="execution-error-GETapi-me-ctoken" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-ctoken"></code></pre>
</span>
<form id="form-GETapi-me-ctoken" data-method="GET"
      data-path="api/me/ctoken"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-ctoken', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-ctoken"
                    onclick="tryItOut('GETapi-me-ctoken');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-ctoken"
                    onclick="cancelTryOut('GETapi-me-ctoken');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-ctoken" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/ctoken</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-ctoken" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-ctoken"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-GETapi-me-notifications">Show users notifications</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-notifications">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/notifications" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/notifications"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-notifications">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;user_id&quot;: 5,
            &quot;type&quot;: &quot;friend_participate_event&quot;,
            &quot;data&quot;: &quot;{\&quot;sender_type\&quot;: \&quot;user\&quot;, \&quot;sender_id\&quot;: 2, \&quot;event_id\&quot;: 1}&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;
        },
        {
            &quot;id&quot;: 2,
            &quot;user_id&quot;: 6,
            &quot;type&quot;: &quot;friendship_request&quot;,
            &quot;data&quot;: &quot;{\&quot;sender_type\&quot;: \&quot;user\&quot;, \&quot;sender_id\&quot;: 2}&quot;,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-notifications" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-notifications"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-notifications"></code></pre>
</span>
<span id="execution-error-GETapi-me-notifications" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-notifications"></code></pre>
</span>
<form id="form-GETapi-me-notifications" data-method="GET"
      data-path="api/me/notifications"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-notifications', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-notifications"
                    onclick="tryItOut('GETapi-me-notifications');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-notifications"
                    onclick="cancelTryOut('GETapi-me-notifications');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-notifications" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/notifications</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-notifications" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-notifications"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-GETapi-me-applications">Show information about user event applications</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-applications">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/applications" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/applications"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-applications">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
            &quot;event&quot;: {
                &quot;id&quot;: 3,
                &quot;title&quot;: &quot;I&#039;m mad?&#039; said.&quot;,
                &quot;description&quot;: &quot;Omnis ut voluptas eos alias quisquam. Officiis numquam optio aut consequatur autem officiis aspernatur hic. Dolor fuga officia et dolor.&quot;,
                &quot;place&quot;: &quot;8433 Roy Rue\nDanielbury, WY 18507&quot;,
                &quot;start_at&quot;: &quot;2022-06-15 21:53:16&quot;,
                &quot;min_age&quot;: 25,
                &quot;is_offline&quot;: false,
                &quot;tags&quot;: [],
                &quot;avatar&quot;: null,
                &quot;city&quot;: {
                    &quot;id&quot;: 12,
                    &quot;name&quot;: &quot;Zetta Gulgowski&quot;
                },
                &quot;participants_count&quot;: 0
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 8,
                &quot;name&quot;: &quot;Prof.&quot;,
                &quot;description&quot;: &quot;Tempore autem sint perferendis dolores. Error eos sit aspernatur eum. Pariatur laboriosam aut voluptatem quae sit.&quot;,
                &quot;contacts&quot;: &quot;39709 Brendon Forest\nLake Aurelie, NV 78946-1973&quot;,
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;template&quot;: {
                &quot;id&quot;: 8,
                &quot;general_title&quot;: null,
                &quot;organization_title&quot;: null,
                &quot;sign&quot;: null,
                &quot;seal&quot;: null,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;
            }
        },
        {
            &quot;id&quot;: 2,
            &quot;created_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
            &quot;event&quot;: {
                &quot;id&quot;: 4,
                &quot;title&quot;: &quot;Alice remarked.&quot;,
                &quot;description&quot;: &quot;Perferendis illo qui culpa et voluptas repellendus et. Hic dolorum voluptatem in eos.&quot;,
                &quot;place&quot;: &quot;59525 Dietrich Turnpike\nChanceberg, NY 53535-1198&quot;,
                &quot;start_at&quot;: &quot;2022-06-13 15:09:16&quot;,
                &quot;min_age&quot;: 5,
                &quot;is_offline&quot;: false,
                &quot;tags&quot;: [],
                &quot;avatar&quot;: null,
                &quot;city&quot;: {
                    &quot;id&quot;: 14,
                    &quot;name&quot;: &quot;Violette Gutkowski&quot;
                },
                &quot;participants_count&quot;: 0
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 10,
                &quot;name&quot;: &quot;Miss&quot;,
                &quot;description&quot;: &quot;Voluptas magni voluptatem explicabo enim in ratione delectus. Sunt soluta placeat quae ut.&quot;,
                &quot;contacts&quot;: &quot;8068 Jolie Inlet Apt. 309\nNorth Shayleetown, WI 79546-8248&quot;,
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;template&quot;: {
                &quot;id&quot;: 11,
                &quot;general_title&quot;: null,
                &quot;organization_title&quot;: null,
                &quot;sign&quot;: null,
                &quot;seal&quot;: null,
                &quot;created_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-06-13T00:45:16.000000Z&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-applications" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-applications"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-applications"></code></pre>
</span>
<span id="execution-error-GETapi-me-applications" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-applications"></code></pre>
</span>
<form id="form-GETapi-me-applications" data-method="GET"
      data-path="api/me/applications"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-applications', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-applications"
                    onclick="tryItOut('GETapi-me-applications');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-applications"
                    onclick="cancelTryOut('GETapi-me-applications');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-applications" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/applications</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-applications" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-applications"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="user-me-GETapi-me-possible-friends">Show possible friends (max 10, cause it slow)</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-me-possible-friends">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/me/possible-friends" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/me/possible-friends"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-me-possible-friends">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 9,
            &quot;name&quot;: &quot;Gregorio Emmerich&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 11,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/11/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004517Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=feff45b7b44b94734460caabfbc2f4f9db068114aba9da5d2e612f5b7469bd2c&quot;
                }
            },
            &quot;description&quot;: &quot;Totam earum repudiandae assumenda amet est fugiat. Similique nostrum qui fugit enim. Non perferendis mollitia dolorum minus fugit nostrum tenetur.&quot;,
            &quot;age&quot;: 25,
            &quot;score&quot;: 80544,
            &quot;level&quot;: 12,
            &quot;need_points_for_new_level&quot;: 83296,
            &quot;city&quot;: {
                &quot;id&quot;: 15,
                &quot;name&quot;: &quot;Candelario Bruen&quot;
            }
        },
        {
            &quot;id&quot;: 10,
            &quot;name&quot;: &quot;Mrs. Blanche Borer&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 12,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/12/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004517Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=88dff0bc053531414d69845389a53a40183755f7379de798e0b96e2c4288749e&quot;
                }
            },
            &quot;description&quot;: &quot;Eaque perspiciatis optio ab provident. Ea a minus vel id eius. Aut rerum veritatis explicabo praesentium.&quot;,
            &quot;age&quot;: 26,
            &quot;score&quot;: 369846618,
            &quot;level&quot;: 25,
            &quot;need_points_for_new_level&quot;: 972330662,
            &quot;city&quot;: {
                &quot;id&quot;: 16,
                &quot;name&quot;: &quot;Mr. Sigmund Cormier II&quot;
            }
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-me-possible-friends" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-me-possible-friends"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-me-possible-friends"></code></pre>
</span>
<span id="execution-error-GETapi-me-possible-friends" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-me-possible-friends"></code></pre>
</span>
<form id="form-GETapi-me-possible-friends" data-method="GET"
      data-path="api/me/possible-friends"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-me-possible-friends', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-me-possible-friends"
                    onclick="tryItOut('GETapi-me-possible-friends');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-me-possible-friends"
                    onclick="cancelTryOut('GETapi-me-possible-friends');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-me-possible-friends" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/me/possible-friends</code></b>
        </p>
                <p>
            <label id="auth-GETapi-me-possible-friends" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-me-possible-friends"
                                                                data-component="header"></label>
        </p>
                </form>

        <h1 id="user-media">User Media</h1>

    

            <h2 id="user-media-POSTapi-media">Store media file</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p><br> Media resources enabled for download ~5 minutes after request
<br> All Media resources, by default, has -1 (primary) ordering.
<br> Max uploaded media of user = 5. If more, will return error.</p>

<span id="example-requests-POSTapi-media">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/media" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"file\": \"optio\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/media"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "file": "optio"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-media">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Unauthenticated.&quot;,
    &quot;message&quot;: &quot;You must be authenticated, to perform this request.&quot;,
    &quot;error&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-media" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-media"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-media"></code></pre>
</span>
<span id="execution-error-POSTapi-media" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-media"></code></pre>
</span>
<form id="form-POSTapi-media" data-method="POST"
      data-path="api/media"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-media', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-media"
                    onclick="tryItOut('POSTapi-media');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-media"
                    onclick="cancelTryOut('POSTapi-media');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-media" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/media</code></b>
        </p>
                <p>
            <label id="auth-POSTapi-media" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="POSTapi-media"
                                                                data-component="header"></label>
        </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>file</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="file"
               data-endpoint="POSTapi-media"
               value="optio"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="user-media-DELETEapi-media--media-">Deleting media, uploaded by user</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-DELETEapi-media--media-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "https://mch-backend-staging.server.bonch.dev/api/media/1" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/media/1"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-media--media-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;title&quot;: &quot;Изображение удалено&quot;,
    &quot;message&quot;: &quot;Изображение успешно удалено&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-DELETEapi-media--media-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-media--media-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-media--media-"></code></pre>
</span>
<span id="execution-error-DELETEapi-media--media-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-media--media-"></code></pre>
</span>
<form id="form-DELETEapi-media--media-" data-method="DELETE"
      data-path="api/media/{media}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-media--media-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-media--media-"
                    onclick="tryItOut('DELETEapi-media--media-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-media--media-"
                    onclick="cancelTryOut('DELETEapi-media--media-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-media--media-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/media/{media}</code></b>
        </p>
                <p>
            <label id="auth-DELETEapi-media--media-" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="DELETEapi-media--media-"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>media</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="media"
               data-endpoint="DELETEapi-media--media-"
               value="1"
               data-component="url" hidden>
    <br>
<p>The ID of media, used for deletion.</p>
            </p>
                    </form>

        <h1 id="user-organizations">User Organizations</h1>

    

            <h2 id="user-organizations-GETapi-organization">Show available organizations</h2>

<p>
</p>



<span id="example-requests-GETapi-organization">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/organization" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-organization">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 14,
            &quot;name&quot;: &quot;Dr.&quot;,
            &quot;description&quot;: &quot;Est cum enim explicabo facere magni. Hic vitae impedit est eum consectetur magni. Hic impedit esse enim ipsam.&quot;,
            &quot;contacts&quot;: &quot;2426 Carmine Islands Apt. 651\nFannyborough, MO 71663&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 21,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/21/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=0f1b4a9121284e6a3ebc40c5e3c922492d8cb0270a0bfb23f51afd47d4d2d00f&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        {
            &quot;id&quot;: 15,
            &quot;name&quot;: &quot;Mrs.&quot;,
            &quot;description&quot;: &quot;Quaerat commodi veniam ipsa officiis aut nam sed. Adipisci vel quibusdam recusandae at voluptas aut autem corporis. Rerum quaerat sint odit. Provident ut magnam delectus fugit voluptas quam impedit.&quot;,
            &quot;contacts&quot;: &quot;386 Herman Camp\nWest Olen, WI 20926&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 22,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/22/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=d4805f971c5087999b6a1e9e6d545fb390e995b2928fb50e39ff45487f8d8d4a&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-organization" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-organization"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-organization"></code></pre>
</span>
<span id="execution-error-GETapi-organization" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-organization"></code></pre>
</span>
<form id="form-GETapi-organization" data-method="GET"
      data-path="api/organization"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-organization', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-organization"
                    onclick="tryItOut('GETapi-organization');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-organization"
                    onclick="cancelTryOut('GETapi-organization');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-organization" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/organization</code></b>
        </p>
                    </form>

            <h2 id="user-organizations-GETapi-organization--organization-">Show information about organization</h2>

<p>
</p>



<span id="example-requests-GETapi-organization--organization-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/organization/19" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/19"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-organization--organization-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 16,
        &quot;name&quot;: &quot;Dr.&quot;,
        &quot;description&quot;: &quot;Quo quaerat et cupiditate quaerat sed. In magnam sed doloremque natus labore mollitia minima impedit. A quidem et dicta ex earum.&quot;,
        &quot;contacts&quot;: &quot;6320 Feil Rest Suite 539\nHarrisside, KY 60088-2562&quot;,
        &quot;avatar&quot;: {
            &quot;id&quot;: 23,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/23/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=c362d4e9a875945526b9366b27c1286f444c1483e34d40aaa8d4bf46703521bb&quot;
            }
        },
        &quot;users_count&quot;: 0,
        &quot;events_count&quot;: 0
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-organization--organization-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-organization--organization-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-organization--organization-"></code></pre>
</span>
<span id="execution-error-GETapi-organization--organization-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-organization--organization-"></code></pre>
</span>
<form id="form-GETapi-organization--organization-" data-method="GET"
      data-path="api/organization/{organization}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-organization--organization-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-organization--organization-"
                    onclick="tryItOut('GETapi-organization--organization-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-organization--organization-"
                    onclick="cancelTryOut('GETapi-organization--organization-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-organization--organization-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/organization/{organization}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="GETapi-organization--organization-"
               value="19"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-organizations-GETapi-organization--organization--members">Show members in organization</h2>

<p>
</p>



<span id="example-requests-GETapi-organization--organization--members">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/organization/12/members" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/12/members"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-organization--organization--members">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 16,
            &quot;name&quot;: &quot;Imelda Sipes&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 24,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/24/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=a033f2f7551b5dfa0e8aa87abe550b31f3d18389c41d17ec19fd0c6fc6306189&quot;
                }
            },
            &quot;description&quot;: &quot;Velit aliquam voluptatum quisquam numquam et. Dolorum a quis officia. Consequuntur nostrum et autem laboriosam esse necessitatibus. Nesciunt earum quas officiis officiis et sed est quos.&quot;,
            &quot;age&quot;: 22,
            &quot;score&quot;: 1808,
            &quot;level&quot;: 7,
            &quot;need_points_for_new_level&quot;: 3312,
            &quot;city&quot;: {
                &quot;id&quot;: 25,
                &quot;name&quot;: &quot;Walton Pouros&quot;
            }
        },
        {
            &quot;id&quot;: 17,
            &quot;name&quot;: &quot;Nikita Morissette&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 25,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/25/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004518Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=2681b92b70117f6e94c80f5d8d5c668f5b0d2e36b825d7e5eab4171d49623622&quot;
                }
            },
            &quot;description&quot;: &quot;Id ipsum aut magni ducimus molestias. Et et eligendi et ea est provident doloribus. Accusantium qui sit qui voluptatum. Voluptatibus rem eos quia repellat quaerat est.&quot;,
            &quot;age&quot;: 27,
            &quot;score&quot;: 27076380,
            &quot;level&quot;: 21,
            &quot;need_points_for_new_level&quot;: 56809700,
            &quot;city&quot;: {
                &quot;id&quot;: 26,
                &quot;name&quot;: &quot;Eva Ernser&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-organization--organization--members" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-organization--organization--members"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-organization--organization--members"></code></pre>
</span>
<span id="execution-error-GETapi-organization--organization--members" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-organization--organization--members"></code></pre>
</span>
<form id="form-GETapi-organization--organization--members" data-method="GET"
      data-path="api/organization/{organization}/members"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-organization--organization--members', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-organization--organization--members"
                    onclick="tryItOut('GETapi-organization--organization--members');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-organization--organization--members"
                    onclick="cancelTryOut('GETapi-organization--organization--members');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-organization--organization--members" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/organization/{organization}/members</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="GETapi-organization--organization--members"
               value="12"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-organizations-POSTapi-organization--organization--subscribe">Subscribe for notifications from organizations</h2>

<p>
</p>



<span id="example-requests-POSTapi-organization--organization--subscribe">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/organization/4/subscribe" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/4/subscribe"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-organization--organization--subscribe">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;subscribed successfully&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-organization--organization--subscribe" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-organization--organization--subscribe"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-organization--organization--subscribe"></code></pre>
</span>
<span id="execution-error-POSTapi-organization--organization--subscribe" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-organization--organization--subscribe"></code></pre>
</span>
<form id="form-POSTapi-organization--organization--subscribe" data-method="POST"
      data-path="api/organization/{organization}/subscribe"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-organization--organization--subscribe', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-organization--organization--subscribe"
                    onclick="tryItOut('POSTapi-organization--organization--subscribe');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-organization--organization--subscribe"
                    onclick="cancelTryOut('POSTapi-organization--organization--subscribe');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-organization--organization--subscribe" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/organization/{organization}/subscribe</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="POSTapi-organization--organization--subscribe"
               value="4"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-organizations-POSTapi-organization--organization--unsubscribe">Unsubscribe for notifications from organizations</h2>

<p>
</p>



<span id="example-requests-POSTapi-organization--organization--unsubscribe">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/organization/12/unsubscribe" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/12/unsubscribe"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-organization--organization--unsubscribe">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;unsubscribed successfully&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-organization--organization--unsubscribe" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-organization--organization--unsubscribe"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-organization--organization--unsubscribe"></code></pre>
</span>
<span id="execution-error-POSTapi-organization--organization--unsubscribe" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-organization--organization--unsubscribe"></code></pre>
</span>
<form id="form-POSTapi-organization--organization--unsubscribe" data-method="POST"
      data-path="api/organization/{organization}/unsubscribe"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-organization--organization--unsubscribe', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-organization--organization--unsubscribe"
                    onclick="tryItOut('POSTapi-organization--organization--unsubscribe');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-organization--organization--unsubscribe"
                    onclick="cancelTryOut('POSTapi-organization--organization--unsubscribe');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-organization--organization--unsubscribe" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/organization/{organization}/unsubscribe</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="POSTapi-organization--organization--unsubscribe"
               value="12"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-organizations-POSTapi-organization--organization--join">Join onto organizations</h2>

<p>
</p>



<span id="example-requests-POSTapi-organization--organization--join">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/organization/7/join" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/7/join"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-organization--organization--join">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;joined successfully&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-organization--organization--join" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-organization--organization--join"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-organization--organization--join"></code></pre>
</span>
<span id="execution-error-POSTapi-organization--organization--join" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-organization--organization--join"></code></pre>
</span>
<form id="form-POSTapi-organization--organization--join" data-method="POST"
      data-path="api/organization/{organization}/join"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-organization--organization--join', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-organization--organization--join"
                    onclick="tryItOut('POSTapi-organization--organization--join');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-organization--organization--join"
                    onclick="cancelTryOut('POSTapi-organization--organization--join');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-organization--organization--join" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/organization/{organization}/join</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="POSTapi-organization--organization--join"
               value="7"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-organizations-POSTapi-organization--organization--leave">Leave from organizations</h2>

<p>
</p>



<span id="example-requests-POSTapi-organization--organization--leave">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/organization/7/leave" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/7/leave"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-organization--organization--leave">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;leaved successfully&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-organization--organization--leave" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-organization--organization--leave"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-organization--organization--leave"></code></pre>
</span>
<span id="execution-error-POSTapi-organization--organization--leave" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-organization--organization--leave"></code></pre>
</span>
<form id="form-POSTapi-organization--organization--leave" data-method="POST"
      data-path="api/organization/{organization}/leave"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-organization--organization--leave', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-organization--organization--leave"
                    onclick="tryItOut('POSTapi-organization--organization--leave');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-organization--organization--leave"
                    onclick="cancelTryOut('POSTapi-organization--organization--leave');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-organization--organization--leave" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/organization/{organization}/leave</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="POSTapi-organization--organization--leave"
               value="7"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-organizations-GETapi-organization-search">Return organization from search.</h2>

<p>
</p>

<p>..</p>

<span id="example-requests-GETapi-organization-search">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/organization/search?search=test" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"search\": \"mnjrhdhpircyygiifcxusckznqexfbtevqog\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/search"
);

const params = {
    "search": "test",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "search": "mnjrhdhpircyygiifcxusckznqexfbtevqog"
};

fetch(url, {
    method: "GET",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-organization-search">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 37,
            &quot;name&quot;: &quot;Mrs.&quot;,
            &quot;description&quot;: &quot;Voluptas quasi aperiam rerum voluptate reprehenderit odit quisquam. Earum cupiditate qui delectus doloribus. Enim vero et omnis quia.&quot;,
            &quot;contacts&quot;: &quot;43640 Wuckert Cape\nWest Efrainfurt, FL 65830&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 39,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/39/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=e7044fe1becb7f843cadb88355bc816d4e04f3ac08ac008fc8cd63c66fb449d2&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        {
            &quot;id&quot;: 38,
            &quot;name&quot;: &quot;Prof.&quot;,
            &quot;description&quot;: &quot;Nobis quos rerum impedit dolorem ad architecto nostrum. Atque nobis rerum consequatur dolorum mollitia quo labore. Unde excepturi dolor eos beatae est aperiam sed dolorem.&quot;,
            &quot;contacts&quot;: &quot;98888 Keeling Tunnel Suite 752\nO&#039;Connertown, WI 54050-3171&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 40,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/40/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=b81333be8949d3a5f97069c1753875e56cab8b5ead607f209bf2ecc47cb79f62&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-organization-search" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-organization-search"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-organization-search"></code></pre>
</span>
<span id="execution-error-GETapi-organization-search" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-organization-search"></code></pre>
</span>
<form id="form-GETapi-organization-search" data-method="GET"
      data-path="api/organization/search"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-organization-search', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-organization-search"
                    onclick="tryItOut('GETapi-organization-search');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-organization-search"
                    onclick="cancelTryOut('GETapi-organization-search');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-organization-search" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/organization/search</code></b>
        </p>
                        <h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
                    <p>
                <b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="search"
               data-endpoint="GETapi-organization-search"
               value="test"
               data-component="query" hidden>
    <br>
<p>Search string. Possibly ['id', 'name', 'description']</p>
            </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="search"
               data-endpoint="GETapi-organization-search"
               value="mnjrhdhpircyygiifcxusckznqexfbtevqog"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 50 characters.</p>
        </p>
        </form>

            <h2 id="user-organizations-GETapi-organization--organization--events">Show events created by organization</h2>

<p>
</p>



<span id="example-requests-GETapi-organization--organization--events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/organization/15/events" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/organization/15/events"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-organization--organization--events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 28,
            &quot;title&quot;: &quot;Alice, as she.&quot;,
            &quot;description&quot;: &quot;Dicta quia qui a expedita fuga eum officiis. Corporis minus nisi facere blanditiis eos nulla tenetur. Vel quisquam illum quae possimus eos.&quot;,
            &quot;place&quot;: &quot;39422 Keaton Mountains Suite 942\nLake Anabelle, SD 55501&quot;,
            &quot;start_at&quot;: &quot;2022-06-19T12:41:21.539689Z&quot;,
            &quot;min_age&quot;: 1,
            &quot;is_offline&quot;: false,
            &quot;tags&quot;: [],
            &quot;avatar&quot;: {
                &quot;id&quot;: 42,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/42/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=78c8c50e58151a98d5e9b9a06180957198ceaa6cd84a4593a884d2905c819530&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 78,
                &quot;name&quot;: &quot;Joanny Schmeler&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 39,
                &quot;name&quot;: &quot;Prof.&quot;,
                &quot;description&quot;: &quot;Aspernatur deleniti cupiditate nobis impedit qui. Et quo molestias impedit delectus quis non. Unde voluptatum et rerum tempora. Voluptatibus ullam voluptas voluptatum id.&quot;,
                &quot;contacts&quot;: &quot;35767 Karli Plains Apt. 463\nCasperton, WY 39912&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 41,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/41/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=6aa2660445de6f64d661d87b952f26dd816ff5f87c2f110052f08e4a5094616a&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 0
        },
        {
            &quot;id&quot;: 29,
            &quot;title&quot;: &quot;I believe.&#039; &#039;Boots.&quot;,
            &quot;description&quot;: &quot;Ut dolor non sit perspiciatis ut ut. Quia soluta deserunt veritatis in amet nihil. Dolorem rerum natus ipsam corrupti eum ratione. Tempora repudiandae rerum eveniet distinctio qui molestias corrupti.&quot;,
            &quot;place&quot;: &quot;984 Hamill Ville Suite 661\nSouth Daija, AR 22074&quot;,
            &quot;start_at&quot;: &quot;2022-06-15T08:15:21.616827Z&quot;,
            &quot;min_age&quot;: 0,
            &quot;is_offline&quot;: true,
            &quot;tags&quot;: [],
            &quot;avatar&quot;: {
                &quot;id&quot;: 44,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/44/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=2550612adcce3f95fa905440858cbaebd095418494f287f00128d4f95834cd35&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 79,
                &quot;name&quot;: &quot;Ms. Constance Glover&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 40,
                &quot;name&quot;: &quot;Dr.&quot;,
                &quot;description&quot;: &quot;Eveniet voluptatum quo nulla laudantium vel est. Cupiditate voluptas molestiae perferendis neque rerum. Nostrum architecto reprehenderit assumenda nulla beatae ut quis.&quot;,
                &quot;contacts&quot;: &quot;49813 McKenzie Lights\nEarnestinestad, SC 97523&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 43,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/43/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=568ead970ae7b2abf2b22140bfd970a8cce3e95af59fe7abb1d160156cef36a6&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-organization--organization--events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-organization--organization--events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-organization--organization--events"></code></pre>
</span>
<span id="execution-error-GETapi-organization--organization--events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-organization--organization--events"></code></pre>
</span>
<form id="form-GETapi-organization--organization--events" data-method="GET"
      data-path="api/organization/{organization}/events"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-organization--organization--events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-organization--organization--events"
                    onclick="tryItOut('GETapi-organization--organization--events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-organization--organization--events"
                    onclick="cancelTryOut('GETapi-organization--organization--events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-organization--organization--events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/organization/{organization}/events</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>organization</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="organization"
               data-endpoint="GETapi-organization--organization--events"
               value="15"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

        <h1 id="user-ratings">User Ratings</h1>

    

            <h2 id="user-ratings-GETapi-ratings-users">Show users ratings</h2>

<p>
</p>



<span id="example-requests-GETapi-ratings-users">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/ratings/users" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/ratings/users"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-ratings-users">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 45,
            &quot;name&quot;: &quot;Mr. Domenico Bednar PhD&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 35,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/35/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=c205e8f89c291cc28259d7e9d5d11a0a34f56f12f90856a1191d78ebb0915c73&quot;
                }
            },
            &quot;description&quot;: &quot;Veritatis quas totam ducimus sint porro ut sequi quo. Eum cupiditate est deleniti quos aut. Unde consequuntur dolor quis autem maiores. Dolore nihil ea et temporibus.&quot;,
            &quot;age&quot;: 29,
            &quot;score&quot;: 848,
            &quot;level&quot;: 6,
            &quot;need_points_for_new_level&quot;: 1712,
            &quot;city&quot;: {
                &quot;id&quot;: 72,
                &quot;name&quot;: &quot;Claire Torphy DDS&quot;
            }
        },
        {
            &quot;id&quot;: 46,
            &quot;name&quot;: &quot;Holden Haag&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 36,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/36/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=2aefaa0a1e896017bc2812d873b5587e0ce8088412fde6156999b555c190efcc&quot;
                }
            },
            &quot;description&quot;: &quot;Quidem facere aut sed. Sequi aperiam voluptas hic saepe dolorum aperiam aut. Magnam vel accusantium ipsam aut natus commodi inventore. Et ipsa ut magni et voluptatum.&quot;,
            &quot;age&quot;: 29,
            &quot;score&quot;: 2,
            &quot;level&quot;: 0,
            &quot;need_points_for_new_level&quot;: 38,
            &quot;city&quot;: {
                &quot;id&quot;: 73,
                &quot;name&quot;: &quot;Margarita Weimann&quot;
            }
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-ratings-users" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-ratings-users"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-ratings-users"></code></pre>
</span>
<span id="execution-error-GETapi-ratings-users" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-ratings-users"></code></pre>
</span>
<form id="form-GETapi-ratings-users" data-method="GET"
      data-path="api/ratings/users"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-ratings-users', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-ratings-users"
                    onclick="tryItOut('GETapi-ratings-users');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-ratings-users"
                    onclick="cancelTryOut('GETapi-ratings-users');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-ratings-users" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/ratings/users</code></b>
        </p>
                    </form>

            <h2 id="user-ratings-GETapi-ratings-organizations">Show organizations ratings</h2>

<p>
</p>



<span id="example-requests-GETapi-ratings-organizations">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/ratings/organizations" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/ratings/organizations"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-ratings-organizations">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 35,
            &quot;name&quot;: &quot;Dr.&quot;,
            &quot;description&quot;: &quot;Est dignissimos illo rerum non ut eaque. Rerum eos nobis et in quisquam. Mollitia tempora eveniet consequatur eum. Natus velit magnam ullam aut modi.&quot;,
            &quot;contacts&quot;: &quot;6271 Gottlieb Glen\nEast Delphine, WI 88834-5187&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 37,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/37/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=980474b032f45a950cb09e1c76d0bbd52e0fb92edee10cf975743420e5039bb4&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        },
        {
            &quot;id&quot;: 36,
            &quot;name&quot;: &quot;Miss&quot;,
            &quot;description&quot;: &quot;Architecto consequuntur iusto magni beatae quis accusamus qui. Ducimus sit molestiae et aut. Maxime eum ut pariatur molestiae et iusto.&quot;,
            &quot;contacts&quot;: &quot;3389 Reva Stream\nKeonport, ME 21331-7357&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 38,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/38/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=567dbe4c12991133e2ea81c9e0881c4a0a84c021b671afdfc8b112f97893407e&quot;
                }
            },
            &quot;users_count&quot;: 0,
            &quot;events_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-ratings-organizations" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-ratings-organizations"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-ratings-organizations"></code></pre>
</span>
<span id="execution-error-GETapi-ratings-organizations" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-ratings-organizations"></code></pre>
</span>
<form id="form-GETapi-ratings-organizations" data-method="GET"
      data-path="api/ratings/organizations"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-ratings-organizations', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-ratings-organizations"
                    onclick="tryItOut('GETapi-ratings-organizations');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-ratings-organizations"
                    onclick="cancelTryOut('GETapi-ratings-organizations');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-ratings-organizations" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/ratings/organizations</code></b>
        </p>
                    </form>

        <h1 id="user-users">User Users</h1>

    

            <h2 id="user-users-GETapi-users">Return informations about users in system</h2>

<p>
</p>



<span id="example-requests-GETapi-users">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/users" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/users"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 40,
            &quot;name&quot;: &quot;Miss Carolyne Wuckert I&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 26,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/26/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004520Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=9dc0174f8261b244655a01004781785da23aad0eb76bf8943f76708c68233e6f&quot;
                }
            },
            &quot;description&quot;: &quot;Suscipit deserunt nulla veritatis et cumque asperiores. Commodi similique beatae eligendi quia. Necessitatibus autem dignissimos expedita omnis. Temporibus error suscipit quisquam assumenda eius in.&quot;,
            &quot;age&quot;: 22,
            &quot;score&quot;: 852871814,
            &quot;level&quot;: 26,
            &quot;need_points_for_new_level&quot;: 1831482746,
            &quot;city&quot;: {
                &quot;id&quot;: 65,
                &quot;name&quot;: &quot;Kariane Upton&quot;
            }
        },
        {
            &quot;id&quot;: 41,
            &quot;name&quot;: &quot;Reymundo Donnelly&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 27,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/27/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004520Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=f49a51acc3a9fc18c2b69508274f22c721a5a21ddaddc51b3eb23b3bd403e2ae&quot;
                }
            },
            &quot;description&quot;: &quot;Quae in ex nobis dolorem in nemo quisquam. Provident dolore et ut. Enim veniam possimus quia. Et eos est sed. Ut quisquam et et cumque. Qui excepturi eius recusandae. Consequatur illo alias et neque.&quot;,
            &quot;age&quot;: 20,
            &quot;score&quot;: 5906650,
            &quot;level&quot;: 19,
            &quot;need_points_for_new_level&quot;: 15064870,
            &quot;city&quot;: {
                &quot;id&quot;: 66,
                &quot;name&quot;: &quot;Seth Batz&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users"></code></pre>
</span>
<span id="execution-error-GETapi-users" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users"></code></pre>
</span>
<form id="form-GETapi-users" data-method="GET"
      data-path="api/users"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users"
                    onclick="tryItOut('GETapi-users');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users"
                    onclick="cancelTryOut('GETapi-users');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users</code></b>
        </p>
                    </form>

            <h2 id="user-users-GETapi-users-search">Return users from search.</h2>

<p>
</p>



<span id="example-requests-GETapi-users-search">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/users/search?search=test" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"search\": \"manfurivnzyisuisxwhzsblivxjpjhzhumyszkdt\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/users/search"
);

const params = {
    "search": "test",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "search": "manfurivnzyisuisxwhzsblivxjpjhzhumyszkdt"
};

fetch(url, {
    method: "GET",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users-search">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 42,
            &quot;name&quot;: &quot;Sigurd Kovacek&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 28,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/28/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004520Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=f442f8a234620e51b8d0b1e36fe29bf8df4c9e672c9ac2044ebb8a6df50a76b5&quot;
                }
            },
            &quot;description&quot;: &quot;Eaque eaque labore totam distinctio. Aut aliquid ipsam aliquam aut totam quaerat at. Quasi error ex iure consectetur qui dolorem. Earum quisquam velit molestias mollitia.&quot;,
            &quot;age&quot;: 27,
            &quot;score&quot;: 190,
            &quot;level&quot;: 4,
            &quot;need_points_for_new_level&quot;: 450,
            &quot;city&quot;: {
                &quot;id&quot;: 67,
                &quot;name&quot;: &quot;Dr. Mohammed Torphy&quot;
            }
        },
        {
            &quot;id&quot;: 43,
            &quot;name&quot;: &quot;Jasper Roberts&quot;,
            &quot;avatar&quot;: {
                &quot;id&quot;: 29,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/29/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004520Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=ad742e9991417af22047226e786530b1cc12cd7dfa2b0a67629d953addb3f106&quot;
                }
            },
            &quot;description&quot;: &quot;Corrupti tenetur rerum quibusdam facere eos non. Omnis ea assumenda sed voluptatem. Asperiores aut aut consequatur iure expedita dolores eligendi quia. Explicabo cupiditate ut qui quae.&quot;,
            &quot;age&quot;: 24,
            &quot;score&quot;: 1,
            &quot;level&quot;: 0,
            &quot;need_points_for_new_level&quot;: 39,
            &quot;city&quot;: {
                &quot;id&quot;: 68,
                &quot;name&quot;: &quot;Prof. Jan Schoen MD&quot;
            }
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users-search" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users-search"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users-search"></code></pre>
</span>
<span id="execution-error-GETapi-users-search" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users-search"></code></pre>
</span>
<form id="form-GETapi-users-search" data-method="GET"
      data-path="api/users/search"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users-search', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users-search"
                    onclick="tryItOut('GETapi-users-search');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users-search"
                    onclick="cancelTryOut('GETapi-users-search');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users-search" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/search</code></b>
        </p>
                        <h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
                    <p>
                <b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="search"
               data-endpoint="GETapi-users-search"
               value="test"
               data-component="query" hidden>
    <br>
<p>Search string. Possibly ['id', 'name', 'description']</p>
            </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>search</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="search"
               data-endpoint="GETapi-users-search"
               value="manfurivnzyisuisxwhzsblivxjpjhzhumyszkdt"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 50 characters.</p>
        </p>
        </form>

            <h2 id="user-users-GETapi-users--user-">Show information about user</h2>

<p>
</p>



<span id="example-requests-GETapi-users--user-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/users/18" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/users/18"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users--user-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 44,
        &quot;name&quot;: &quot;Gladys Volkman&quot;,
        &quot;avatar&quot;: {
            &quot;id&quot;: 30,
            &quot;url&quot;: {
                &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/30/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004520Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=aee13e28243a5865c77a56786cf78e77d5e207af0896f5c76fb5beae1234aa80&quot;
            }
        },
        &quot;description&quot;: &quot;Non placeat cum itaque qui. Nihil harum tempora sunt. Neque autem neque rerum quas recusandae aliquam.&quot;,
        &quot;age&quot;: 29,
        &quot;score&quot;: 44,
        &quot;level&quot;: 2,
        &quot;need_points_for_new_level&quot;: 116,
        &quot;city&quot;: {
            &quot;id&quot;: 69,
            &quot;name&quot;: &quot;Dr. Lexus Kozey III&quot;
        }
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users--user-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users--user-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users--user-"></code></pre>
</span>
<span id="execution-error-GETapi-users--user-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users--user-"></code></pre>
</span>
<form id="form-GETapi-users--user-" data-method="GET"
      data-path="api/users/{user}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users--user-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users--user-"
                    onclick="tryItOut('GETapi-users--user-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users--user-"
                    onclick="cancelTryOut('GETapi-users--user-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users--user-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/{user}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="user"
               data-endpoint="GETapi-users--user-"
               value="18"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-users-GETapi-users--user--events">Show events participated by user</h2>

<p>
</p>



<span id="example-requests-GETapi-users--user--events">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "https://mch-backend-staging.server.bonch.dev/api/users/8/events" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/users/8/events"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users--user--events">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 24,
            &quot;title&quot;: &quot;So she swallowed.&quot;,
            &quot;description&quot;: &quot;Vitae rerum et tempore non. Aspernatur dolores aliquam ipsa vel quidem voluptas.&quot;,
            &quot;place&quot;: &quot;9315 Watsica Tunnel Suite 591\nLake Reilly, NH 89046&quot;,
            &quot;start_at&quot;: &quot;2022-06-15T02:34:21.001372Z&quot;,
            &quot;min_age&quot;: 15,
            &quot;is_offline&quot;: true,
            &quot;tags&quot;: [],
            &quot;avatar&quot;: {
                &quot;id&quot;: 32,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/32/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=b1f1fb6a629ab5b2f123589a6758b344decd5842e94ab9336764e2eb477ede36&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 70,
                &quot;name&quot;: &quot;Prof. Myrtle Bednar&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 33,
                &quot;name&quot;: &quot;Dr.&quot;,
                &quot;description&quot;: &quot;Accusantium magni voluptas autem est quasi ut ut. Ea sed ipsam aut consequatur perferendis amet.&quot;,
                &quot;contacts&quot;: &quot;556 Pink Stream Suite 870\nSouth Gilesmouth, OH 09217-2370&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 31,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/31/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=a5bc2fdc581a9037afb38db6ba4fa335c6d2a3fd820d96f9b88c05ca5fe9653a&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 0
        },
        {
            &quot;id&quot;: 25,
            &quot;title&quot;: &quot;Heads below!&#039; (a.&quot;,
            &quot;description&quot;: &quot;Totam hic voluptas consequuntur explicabo voluptates nesciunt temporibus optio. Voluptatem excepturi repellendus placeat qui autem et veniam.&quot;,
            &quot;place&quot;: &quot;225 Bailey Groves\nBreitenbergton, IL 81184&quot;,
            &quot;start_at&quot;: &quot;2022-06-18T08:07:21.081689Z&quot;,
            &quot;min_age&quot;: 28,
            &quot;is_offline&quot;: true,
            &quot;tags&quot;: [],
            &quot;avatar&quot;: {
                &quot;id&quot;: 34,
                &quot;url&quot;: {
                    &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/34/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=e8ecb1ff5ce65ee2b6fe60d2a4120883f4e767c38044c52108e11c795a3582f7&quot;
                }
            },
            &quot;city&quot;: {
                &quot;id&quot;: 71,
                &quot;name&quot;: &quot;Prof. Lavinia Wintheiser&quot;
            },
            &quot;organization&quot;: {
                &quot;id&quot;: 34,
                &quot;name&quot;: &quot;Dr.&quot;,
                &quot;description&quot;: &quot;Assumenda impedit recusandae tempora quibusdam adipisci accusantium totam. Assumenda numquam ipsam repellendus ad nam dignissimos et. Voluptatibus facilis eos modi sit vero assumenda quod nulla.&quot;,
                &quot;contacts&quot;: &quot;442 Georgianna Pike Suite 997\nWest Naomimouth, NV 94344-9784&quot;,
                &quot;avatar&quot;: {
                    &quot;id&quot;: 33,
                    &quot;url&quot;: {
                        &quot;full_size&quot;: &quot;https://s3.selcdn.ru/mch_staging/33/media.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=157816_MCH_STAGING%2F20220613%2Fru-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20220613T004521Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=5400&amp;X-Amz-Signature=5560547c63fdcd9c0ebcb7315560af4134ec61778be186eee115dc8975a7985c&quot;
                    }
                },
                &quot;users_count&quot;: 0,
                &quot;events_count&quot;: 0
            },
            &quot;participants_count&quot;: 0
        }
    ],
    &quot;links&quot;: {
        &quot;first&quot;: &quot;/?page=1&quot;,
        &quot;last&quot;: &quot;/?page=1&quot;,
        &quot;prev&quot;: null,
        &quot;next&quot;: null
    },
    &quot;meta&quot;: {
        &quot;current_page&quot;: 1,
        &quot;from&quot;: 1,
        &quot;last_page&quot;: 1,
        &quot;links&quot;: [
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;&amp;laquo; Previous&quot;,
                &quot;active&quot;: false
            },
            {
                &quot;url&quot;: &quot;/?page=1&quot;,
                &quot;label&quot;: &quot;1&quot;,
                &quot;active&quot;: true
            },
            {
                &quot;url&quot;: null,
                &quot;label&quot;: &quot;Next &amp;raquo;&quot;,
                &quot;active&quot;: false
            }
        ],
        &quot;path&quot;: &quot;/&quot;,
        &quot;per_page&quot;: 10,
        &quot;to&quot;: 2,
        &quot;total&quot;: 2
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users--user--events" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users--user--events"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users--user--events"></code></pre>
</span>
<span id="execution-error-GETapi-users--user--events" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users--user--events"></code></pre>
</span>
<form id="form-GETapi-users--user--events" data-method="GET"
      data-path="api/users/{user}/events"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users--user--events', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users--user--events"
                    onclick="tryItOut('GETapi-users--user--events');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users--user--events"
                    onclick="cancelTryOut('GETapi-users--user--events');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users--user--events" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/{user}/events</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="user"
               data-endpoint="GETapi-users--user--events"
               value="8"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-users-POSTapi-users--user--friends-add">Add user to friendlist</h2>

<p>
</p>



<span id="example-requests-POSTapi-users--user--friends-add">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/users/18/friends/add" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/users/18/friends/add"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users--user--friends-add">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;successfully sent friendship request&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-users--user--friends-add" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users--user--friends-add"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users--user--friends-add"></code></pre>
</span>
<span id="execution-error-POSTapi-users--user--friends-add" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users--user--friends-add"></code></pre>
</span>
<form id="form-POSTapi-users--user--friends-add" data-method="POST"
      data-path="api/users/{user}/friends/add"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users--user--friends-add', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users--user--friends-add"
                    onclick="tryItOut('POSTapi-users--user--friends-add');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users--user--friends-add"
                    onclick="cancelTryOut('POSTapi-users--user--friends-add');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users--user--friends-add" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/{user}/friends/add</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="user"
               data-endpoint="POSTapi-users--user--friends-add"
               value="18"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="user-users-POSTapi-users--user--friends-accept">Accept friend request</h2>

<p>
</p>



<span id="example-requests-POSTapi-users--user--friends-accept">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "https://mch-backend-staging.server.bonch.dev/api/users/1/friends/accept" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "https://mch-backend-staging.server.bonch.dev/api/users/1/friends/accept"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users--user--friends-accept">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;successfully accepted friendship request&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-users--user--friends-accept" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users--user--friends-accept"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users--user--friends-accept"></code></pre>
</span>
<span id="execution-error-POSTapi-users--user--friends-accept" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users--user--friends-accept"></code></pre>
</span>
<form id="form-POSTapi-users--user--friends-accept" data-method="POST"
      data-path="api/users/{user}/friends/accept"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users--user--friends-accept', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users--user--friends-accept"
                    onclick="tryItOut('POSTapi-users--user--friends-accept');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users--user--friends-accept"
                    onclick="cancelTryOut('POSTapi-users--user--friends-accept');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users--user--friends-accept" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/{user}/friends/accept</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>user</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="user"
               data-endpoint="POSTapi-users--user--friends-accept"
               value="1"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

    

        
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                                        <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                                        <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                            </div>
            </div>
</div>
</body>
</html>
